package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

final class RpcTest {

  @Test
  void accountPlugin_deductCoinBalance() {
    String symbol = "ETH";

    byte[] rpc = AccountPluginRpc.deductCoinBalance(symbol, Unsigned256.TEN);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.DEDUCT_COIN_BALANCE);
    assertThat(stream.readString()).isEqualTo(symbol);
    assertThat(Unsigned256.read(stream)).isEqualTo(Unsigned256.TEN);
  }

  @Test
  void accountPlugin_addTokenBalance() {
    long amount = 10;

    byte[] rpc = AccountPluginRpc.addTokenBalance(amount);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.ADD_MPC_TOKEN_BALANCE);
    assertThat(stream.readLong()).isEqualTo(amount);
  }

  @Test
  void accountPlugin_associate() {
    long amount = 10;
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000004000000000000000000000000000000000001");

    byte[] rpc = AccountPluginRpc.associateTokens(amount, contractAddress);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.ASSOCIATE_TOKENS_TO_CONTRACT);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(contractAddress);
  }

  @Test
  @DisplayName("Associating with an expiration timestamp, creates expected rpc.")
  void accountPlugin_associateWithExpiration() {
    long amount = 10;
    long expirationTimestamp = 100L;
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000004000000000000000000000000000000000001");

    byte[] rpc =
        AccountPluginRpc.associateTokensWithExpiration(
            amount, contractAddress, expirationTimestamp);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.ASSOCIATE_TO_CONTRACT_WITH_EXPIRATION);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(contractAddress);
    assertThat(stream.readLong()).isEqualTo(expirationTimestamp);
  }

  @Test
  void accountPlugin_disassociate() {
    long amount = 10;
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000004000000000000000000000000000000000001");

    byte[] rpc = AccountPluginRpc.disassociateTokens(amount, contractAddress);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.DISASSOCIATE_TOKENS_FROM_CONTRACT);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(contractAddress);
  }

  @Test
  void accountPlugin_burnStakedTokens() {
    long amount = 10;
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000004000000000000000000000000000000000001");

    byte[] rpc = AccountPluginRpc.burnStakedTokens(amount, contractAddress);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.BURN_MPC_TOKENS);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(contractAddress);
  }

  @Test
  void associateCallback() {
    long amount = 10;
    BlockchainAddress account =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");

    byte[] rpc =
        SafeDataOutputStream.serialize(
            LargeOracleContract.Callbacks.associateCallback(account, amount));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(LargeOracleContract.Callbacks.ASSOCIATE_TOKENS_CALLBACK);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(account);
    assertThat(stream.readLong()).isEqualTo(amount);
  }

  @Test
  @DisplayName("Callback for associating with an expiration timestamp, creates expected rpc.")
  void associateWithExpirationCallback() {
    long amount = 10;
    long expirationTimestamp = 100L;
    BlockchainAddress account =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");

    byte[] rpc =
        SafeDataOutputStream.serialize(
            LargeOracleContract.Callbacks.associateWithExpirationCallback(
                account, amount, expirationTimestamp));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(LargeOracleContract.Callbacks.ASSOCIATE_TOKENS_WITH_EXPIRATION_CALLBACK);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(account);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(stream.readLong()).isEqualTo(expirationTimestamp);
  }

  @Test
  @DisplayName("Setting expiration timestamp for association, creates expected rpc.")
  void accountPlugin_setExpirationForAssociation() {
    long expirationTimestamp = 100L;
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000004000000000000000000000000000000000001");

    byte[] rpc = AccountPluginRpc.setExpirationForAssociation(contractAddress, expirationTimestamp);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.SET_EXPIRATION_FOR_ASSOCIATION);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(contractAddress);
    stream.readBoolean();
    assertThat(stream.readLong()).isEqualTo(expirationTimestamp);
  }

  @Test
  @DisplayName("Callback for setting expiration timestamp for associating, creates expected rpc.")
  void setExpirationForAssociationCallback() {
    long expirationTimestamp = 100L;
    BlockchainAddress account =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");

    byte[] rpc =
        SafeDataOutputStream.serialize(
            LargeOracleContract.Callbacks.setExpirationForAssociation(
                account, expirationTimestamp));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(LargeOracleContract.Callbacks.SET_EXPIRATION_FOR_ASSOCIATION_CALLBACK);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(account);
    stream.readBoolean();
    assertThat(stream.readLong()).isEqualTo(expirationTimestamp);
  }

  @Test
  void disassociateCallback() {
    long amount = 10;
    BlockchainAddress account =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");

    byte[] rpc =
        SafeDataOutputStream.serialize(
            LargeOracleContract.Callbacks.disassociateCallback(account, amount));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(LargeOracleContract.Callbacks.DISASSOCIATE_TOKENS_CALLBACK);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(account);
    assertThat(stream.readLong()).isEqualTo(amount);
  }

  @Test
  void sellByocCallback() {
    BlockchainAddress seller =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    String symbol = "ETH";
    long mpcTokenToBuy = 10;

    byte[] rpc =
        SafeDataOutputStream.serialize(
            LargeOracleContract.Callbacks.sellByocTwinsCallback(
                seller, symbol, Unsigned256.TEN, mpcTokenToBuy));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(LargeOracleContract.Callbacks.SELL_BYOC_TWINS_CALLBACK);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(seller);
    assertThat(stream.readString()).isEqualTo(symbol);
    assertThat(Unsigned256.read(stream)).isEqualTo(Unsigned256.TEN);
    assertThat(stream.readLong()).isEqualTo(mpcTokenToBuy);
  }

  @Test
  void bpoTriggerCommittee() {
    byte[] dataStreamSerializable =
        SafeDataOutputStream.serialize(BpOrchestrationRpc.triggerNewCommittee());
    assertThat(dataStreamSerializable).isEqualTo(new byte[] {13});
  }

  @Test
  void smallOracleRpc_updateRequest() {
    int invocationByte = 12;
    BlockchainAddress account1 =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BlockchainAddress account2 =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");
    List<BlockchainAddress> accounts = List.of(account1, account2);
    BlockchainPublicKey key1 = new KeyPair(BigInteger.valueOf(1)).getPublic();
    BlockchainPublicKey key2 = new KeyPair(BigInteger.valueOf(2)).getPublic();
    List<BlockchainPublicKey> keys = List.of(key1, key2);

    byte[] rpc =
        SafeDataOutputStream.serialize(
            SmallOracleRpc.updateRequest(invocationByte, accounts, keys));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(invocationByte);
    assertThat(BlockchainAddress.LIST_SERIALIZER.readDynamic(stream)).isEqualTo(accounts);
    assertThat(BlockchainPublicKey.LIST_SERIALIZER.readDynamic(stream)).isEqualTo(keys);
  }

  @Test
  void smallOracleRpc_addCounterClaim() {
    int invocationByte = 12;
    int nrOfBytes = 32;
    byte[] passThrough = new byte[nrOfBytes];

    byte[] rpc =
        SafeDataOutputStream.serialize(SmallOracleRpc.addCounterClaim(invocationByte, passThrough));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(invocationByte);
    assertThat(stream.readBytes(nrOfBytes)).isEqualTo(passThrough);
  }

  @Test
  void smallOracleRpc_sendPollResult() {
    int invocationByte = 12;
    long oracleId = 0;
    long disputeId = 10;
    int votingResult = 15;

    byte[] rpc =
        SafeDataOutputStream.serialize(
            SmallOracleRpc.sendPollResult(invocationByte, oracleId, disputeId, votingResult));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(invocationByte);
    assertThat(stream.readLong()).isEqualTo(oracleId);
    assertThat(stream.readLong()).isEqualTo(disputeId);
    assertThat(stream.readInt()).isEqualTo(votingResult);
  }
}
