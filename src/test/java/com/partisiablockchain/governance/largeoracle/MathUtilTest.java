package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class MathUtilTest {
  @Test
  public void testBigIntegerDivision() {
    Unsigned256 bigFive = Unsigned256.create(5);
    Assertions.assertThat(MathUtil.ceilDiv(bigFive, Unsigned256.create(2)))
        .isEqualTo(Unsigned256.create(3));
    Assertions.assertThat(MathUtil.ceilDiv(Unsigned256.ZERO, Unsigned256.create(2)))
        .isEqualTo(Unsigned256.ZERO);
  }
}
