package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** test. */
public final class OracleDisputeIdTest {

  @Test
  public void equals() {
    EqualsVerifier.forClass(OracleDisputeId.class).usingGetClass().verify();
  }

  @Test
  void compareTo() {
    BlockchainAddress address =
        BlockchainAddress.fromString("000000000000000000000000000000000000000000");
    OracleDisputeId first = new OracleDisputeId(address, 0, 0);
    OracleDisputeId second = new OracleDisputeId(address, 1, 0);
    OracleDisputeId third = new OracleDisputeId(address, 1, 1);

    Assertions.assertThat(first.compareTo(second)).isLessThan(0);
    Assertions.assertThat(first.compareTo(third)).isLessThan(0);
    Assertions.assertThat(second.compareTo(first)).isGreaterThan(0);
    Assertions.assertThat(second.compareTo(third)).isLessThan(0);
    Assertions.assertThat(third.compareTo(second)).isGreaterThan(0);
  }
}
