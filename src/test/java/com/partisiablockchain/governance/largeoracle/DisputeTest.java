package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import org.junit.jupiter.api.Test;

/** Test. */
public final class DisputeTest {

  private final BlockchainAddress challenger =
      BlockchainAddress.fromString("000000000000000000000000000000000000000000");
  private static final int resultInvocation = 0;
  private static final int counterClaim = 0;
  private static final int voteCount = 8;

  @Test
  public void countingMultipleVotes() {
    Dispute dispute = Dispute.create(challenger, resultInvocation, counterClaim, voteCount);
    assertThat(dispute.countVotes())
        .isEqualTo(-1); // Default value is -1 until votes have been cast
    dispute = dispute.addVote(0, 0);
    assertThat(dispute.countVotes()).isEqualTo(0);
    dispute = dispute.addVote(2, 1);
    // Result is still 0, since the assumption that a majority vote exists is broken
    assertThat(dispute.countVotes()).isEqualTo(0);
    dispute = dispute.addVote(4, 1);
    assertThat(dispute.countVotes()).isEqualTo(1);
    dispute = dispute.addVote(3, 0);
    // Since the assumption is broken and the last candidate to hold majority was 0 result is 0
    assertThat(dispute.countVotes()).isEqualTo(0);
    dispute = dispute.addVote(7, 0);
    assertThat(dispute.enoughVotesCast()).isFalse();
    dispute = dispute.addVote(1, 0);
    assertThat(dispute.enoughVotesCast()).isTrue();
    assertThat(dispute.countVotes()).isEqualTo(0);
  }
}
