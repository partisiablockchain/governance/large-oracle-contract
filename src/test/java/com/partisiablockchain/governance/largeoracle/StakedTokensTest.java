package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/** Test. */
public final class StakedTokensTest {
  private final BlockchainAddress oracle =
      BlockchainAddress.fromString("040000000000000000000000000000000000001337");
  private final OracleDisputeId disputeId = new OracleDisputeId(oracle, 1, 0L);
  private static final long tokens = 500L;

  private StakedTokens stakedTokens;

  /** Setup default stakes tokens with 500 associated tokens. */
  @BeforeEach
  public void setUp() {
    stakedTokens = StakedTokens.create().associateTokens(tokens, null);
  }

  @Test
  @DisplayName("Migrating from a state with the exact same fields, copies values to the new state.")
  public void createFromCurrentStateAccessor() {
    stakedTokens =
        stakedTokens
            .lockToDispute(disputeId, 200, 0L)
            .lockToOracle(oracle, 30, 0L)
            .releaseFromOracle(oracle, 0)
            .lockToOracle(oracle, 40, 0L)
            .setReserved(1);
    StateAccessor accessor = StateAccessor.create(stakedTokens);
    StakedTokens reconstructed = StakedTokens.createFromStateAccessor(accessor);
    StateSerializableEquality.assertStatesEqual(stakedTokens, reconstructed);
  }

  @Test
  @DisplayName(
      "Migrating from a state with a new field, populates the new field with a default value.")
  public void createFromOldStateAccessor() {
    OldStakedTokens oldStakedTokens =
        new OldStakedTokens(500, 100, AvlTree.create(), AvlTree.create(), AvlTree.create());
    StateAccessor accessor = StateAccessor.create(oldStakedTokens);
    StakedTokens reconstructed = StakedTokens.createFromStateAccessor(accessor);
    assertThat(reconstructed.getExpirationTimestamp())
        .isEqualTo(StakedTokens.create().getExpirationTimestamp());
  }

  @Test
  void disassociateTokensNotSufficientFreeTokens() {
    assertThatThrownBy(() -> stakedTokens.disassociateTokens(tokens + 1))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Not enough free tokens to disassociate");
  }

  @Test
  public void canHaveZeroStakedTokens() {
    stakedTokens = stakedTokens.disassociateTokens(tokens);
    assertThat(stakedTokens.getAllocatableTokens()).isEqualTo(0L);
  }

  @Test
  public void increasingCurrentFreeTokensShouldNotAffectReservedTokens() {
    stakedTokens = stakedTokens.associateTokens(1, null);
    assertThat(stakedTokens.getAllocatableTokens()).isEqualTo(tokens + 1);
  }

  @Test
  public void possibleToIncreaseAndDecreaseFreeTokensBy0() {
    stakedTokens = stakedTokens.associateTokens(0, null);
    assertThat(stakedTokens.getAllocatableTokens()).isEqualTo(tokens);
    stakedTokens = stakedTokens.disassociateTokens(0);
    assertThat(stakedTokens.getAllocatableTokens()).isEqualTo(tokens);
  }

  @Test
  public void insufficientFreeTokensToLockToDispute() {
    assertThatThrownBy(
            () -> Objects.requireNonNull(stakedTokens.lockToDispute(disputeId, tokens + 1, 0L)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Not enough allocatable tokens to lock to dispute");
  }

  @Test
  public void insufficientFreeTokensToLockToOracle() {
    assertThatThrownBy(
            () -> Objects.requireNonNull(stakedTokens.lockToOracle(oracle, tokens + 1, 0L)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Not enough allocatable tokens to lock to oracle");
  }

  @Test
  public void insufficientTokensToBurnFromOracle() {
    assertThatThrownBy(
            () -> Objects.requireNonNull(stakedTokens.burnFromOracle(oracle, tokens + 1)))
        .isInstanceOf(IllegalStateException.class)
        .hasMessage("Insufficient amount of staked tokens to burn");
  }

  @Test
  public void burnFromOracle() {
    stakedTokens =
        stakedTokens
            .lockToDispute(disputeId, 100, 0L)
            .lockToOracle(oracle, 20, 0L)
            .releaseFromOracle(oracle, 0)
            .lockToOracle(oracle, 30, 0L);

    assertThat(stakedTokens.getAllocatableTokens()).isEqualTo(350L);
    assertThat(stakedTokens.getLockedToDispute(disputeId)).isEqualTo(100L);
    assertThat(stakedTokens.getLockedToOracle(oracle)).isEqualTo(30L);
    assertThat(stakedTokens.getPendingForOracle(oracle)).isEqualTo(20L);

    stakedTokens = stakedTokens.burnFromOracle(oracle, 100);

    assertThat(stakedTokens.getAllocatableTokens()).isEqualTo(300L);
    assertThat(stakedTokens.getLockedToDispute(disputeId)).isEqualTo(100L);
    assertThat(stakedTokens.getLockedToOracle(oracle)).isEqualTo(0L);
    assertThat(stakedTokens.getPendingForOracle(oracle)).isEqualTo(0L);

    // burning currently happens from pending tokens before locked
    stakedTokens =
        stakedTokens
            .lockToOracle(oracle, 50, 0L)
            .releaseFromOracle(oracle, 0)
            .lockToOracle(oracle, 50, 0L)
            .releaseFromOracle(oracle, 1)
            .lockToOracle(oracle, 50, 0L)
            .burnFromOracle(oracle, 30);
    assertThat(stakedTokens.getTimestamps(oracle).size()).isEqualTo(2);
    assertThat(stakedTokens.getPendingForOracle(oracle)).isEqualTo(70);
    assertThat(stakedTokens.getLockedToOracle(oracle)).isEqualTo(50);

    stakedTokens = stakedTokens.burnFromOracle(oracle, 20);
    assertThat(stakedTokens.getPendingForOracle(oracle)).isEqualTo(50);
    assertThat(stakedTokens.getTimestamps(oracle).size()).isEqualTo(1);
  }

  /**
   * When burning more tokens than locked to an oracle we should also burn free tokens that has been
   * reserved.
   */
  @Test
  void burnReservedTokens() {
    stakedTokens = stakedTokens.lockToOracle(oracle, 1, 0L);

    long reserveAmount = 1;
    stakedTokens = stakedTokens.setReserved(reserveAmount);

    stakedTokens = stakedTokens.burnFromOracle(oracle, tokens);

    assertThat(stakedTokens.getFreeTokens()).isEqualTo(0);
    assertThat(stakedTokens.getLockedToOracle(oracle)).isEqualTo(0);
  }

  @Test
  @DisplayName("Trying to lock expired tokens to an oracle, throws exception.")
  void cannotLockTokensToOracleThatHasExpired() {
    long expirationTimestamp = 100L;
    stakedTokens = stakedTokens.associateTokens(10, expirationTimestamp);
    long blockProductionTime = expirationTimestamp + 1;

    assertThatThrownBy(() -> stakedTokens.lockToOracle(oracle, 1, blockProductionTime))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Tokens associated to contract have expired, and cannot be locked to oracle");
  }

  @Test
  @DisplayName("Trying to lock expired tokens to a dispute, throws exception.")
  void cannotLockTokensToDisputeThatHasExpired() {
    long expirationTimestamp = 100L;
    stakedTokens = stakedTokens.associateTokens(10, expirationTimestamp);
    long blockProductionTime = expirationTimestamp + 1;

    assertThatThrownBy(() -> stakedTokens.lockToDispute(disputeId, 1, blockProductionTime))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Tokens associated to contract have expired, and cannot be locked to dispute");
  }

  @Test
  void clearOlderThan() {
    long now = 1695628625L;
    stakedTokens = stakedTokens.lockToOracle(oracle, 42, 0L).releaseFromOracle(oracle, now);
    long past = now - 1;

    stakedTokens = stakedTokens.clearPendingForOracleOlderThan(oracle, past);
    assertThat(stakedTokens.getPendingForOracle(oracle)).isEqualTo(42);

    stakedTokens = stakedTokens.clearPendingForOracleOlderThan(oracle, now);
    assertThat(stakedTokens.getPendingForOracle(oracle)).isEqualTo(42);

    long future = now + 1;
    stakedTokens = stakedTokens.clearPendingForOracleOlderThan(oracle, future);
    assertThat(stakedTokens.getAllocatableTokens()).isEqualTo(500);
    assertThat(stakedTokens.getPendingForOracle(oracle)).isEqualTo(0);
  }

  @Immutable
  private record OldStakedTokens(
      long freeTokens,
      long reservedTokens,
      AvlTree<BlockchainAddress, Long> lockedToOracle,
      AvlTree<BlockchainAddress, PendingTokensTimestamps> pendingTokens,
      AvlTree<OracleDisputeId, Long> lockedToDispute)
      implements StateSerializable {}
}
