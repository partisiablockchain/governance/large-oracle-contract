package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractEventInteraction;
import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/** LargeOracleContract. */
public final class LargeOracleContractTest {

  private final SysContractSerialization<LargeOracleContractState> serialization =
      new SysContractSerialization<>(
          LargeOracleContractInvoker.class, LargeOracleContractState.class);

  private SysContractContextTest context;

  private final BlockchainPublicKey oracle0 = new KeyPair(BigInteger.valueOf(111)).getPublic();
  private final BlockchainAddress address0 =
      BlockchainAddress.fromString("000000000000000000000000000000000000000011");
  private final BlockchainPublicKey oracle1 = new KeyPair(BigInteger.valueOf(222)).getPublic();
  private final BlockchainAddress address1 =
      BlockchainAddress.fromString("000000000000000000000000000000000000000022");
  private final BlockchainPublicKey oracle2 = new KeyPair(BigInteger.valueOf(333)).getPublic();
  private final BlockchainAddress address2 =
      BlockchainAddress.fromString("000000000000000000000000000000000000000033");
  private final BlockchainPublicKey oracle3 = new KeyPair(BigInteger.valueOf(444)).getPublic();
  private final BlockchainAddress address3 =
      BlockchainAddress.fromString("000000000000000000000000000000000000000044");

  private final List<BlockchainPublicKey> oracleKeys = List.of(oracle0, oracle1, oracle2);
  private final List<BlockchainAddress> oracleAddresses = List.of(address0, address1, address2);
  private final List<OracleMember> oracleMembers =
      LargeOracleContract.createOracleMembers(oracleAddresses, oracleKeys);
  private final List<BlockchainAddress> shuffledOracleAddress =
      List.of(address0, address2, address1);
  private final List<BlockchainPublicKey> shuffledOracleKeys = List.of(oracle0, oracle2, oracle1);
  private static final long oracleId = 1;

  private final KeyPair oraclePrivateKey = new KeyPair(BigInteger.TWO);
  private final BlockchainPublicKey oraclePublicKey = oraclePrivateKey.getPublic();

  private final BlockchainAddress bpOrchestrationContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000042");
  private final BlockchainAddress depositContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000001");
  private final BlockchainAddress withdrawalContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000002");
  private final BlockchainAddress governanceUpdates =
      BlockchainAddress.fromString("040000000000000000000000000000000000000003");

  private final BlockchainAddress challenger =
      BlockchainAddress.fromString("000000000000000000000000000000000000000003");

  private final BlockchainAddress priceOracleContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000069");
  private static final long disputeTokenCost = 25_000L;
  private static final long disputeId1 = 1337L;
  private static final long disputeId2 = 80085L;

  private static final int smallOracleResultInvocation = 3;
  private static final int smallOracleCounterClaimInvocation = 4;
  private static final Hash hash = Hash.create(s -> s.writeInt(42));
  private static final int hashCodeOfHash = -2089190303;

  private static final String ETH = "ETH";

  private LargeOracleContractState state;

  /** Setup. */
  @BeforeEach
  public void setUp() {
    context = new SysContractContextTest(1, 100, address0);
    state =
        serialization.create(
            context,
            rpc -> {
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(rpc, oracleKeys);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, oracleAddresses);
              oraclePublicKey.write(rpc);
              bpOrchestrationContract.write(rpc);
              governanceUpdates.write(rpc);
            });
  }

  @Test
  public void create() {
    assertThat(state.getOracleMembers()).containsAll(oracleMembers);
    assertThat(state.getActiveDisputes().size()).isEqualTo(0);
  }

  private void assertCreateDispute(LargeOracleContractState state) {
    OracleDisputeId oracleDisputeId = new OracleDisputeId(withdrawalContract, oracleId, disputeId1);
    Dispute dispute = state.getDispute(oracleDisputeId);
    assertThat(dispute.getChallenger()).isEqualTo(challenger);
    assertThat(dispute.getContractResultInvocation()).isEqualTo(smallOracleResultInvocation);
    assertThat(dispute.getContractCounterClaimInvocation())
        .isEqualTo(smallOracleCounterClaimInvocation);
    assertThat(dispute.getVotes()).containsOnlyNulls();

    StakedTokens accountTokens = state.getTokensStakedBy(challenger);
    assertThat(accountTokens.getAllocatableTokens()).isEqualTo(0L);
    assertThat(accountTokens.getLockedToDispute(oracleDisputeId)).isEqualTo(disputeTokenCost);
  }

  @Test
  public void createDispute() {
    state = state.stakeTokens(challenger, disputeTokenCost, null);
    from(withdrawalContract);
    state = invokeCreateDisputePoll();
    assertCreateDispute(state);
  }

  @Test
  public void createDisputeWithThrowErrorInvocation() {
    state = state.stakeTokens(challenger, disputeTokenCost, null);
    from(withdrawalContract);
    state = invokeCreateDisputePollWithThrowError();
    assertCreateDispute(state);
  }

  @Test
  public void onlySmallOracleContractsCanStartDisputePoll() {
    from(challenger);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.CREATE_DISPUTE_POLL);
                      challenger.write(rpc);
                      rpc.writeLong(0);
                      rpc.writeLong(oracleId);
                      rpc.writeLong(disputeId1);
                      rpc.writeByte(smallOracleResultInvocation);
                      rpc.writeByte(smallOracleCounterClaimInvocation);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Disputes can only be created by one of the small oracle contracts");
  }

  @Test
  public void onlySmallOracleContractsCanStartDisputePollWithThrowError() {
    from(challenger);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(
                          LargeOracleContract.Invocations.CREATE_DISPUTE_POLL_WITH_ERROR_THROW);
                      challenger.write(rpc);
                      rpc.writeLong(disputeTokenCost);
                      rpc.writeLong(oracleId);
                      rpc.writeLong(disputeId1);
                      rpc.writeByte(smallOracleResultInvocation);
                      rpc.writeByte(smallOracleCounterClaimInvocation);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Disputes can only be created by one of the small oracle contracts");
  }

  @Test
  public void notifyCallingSmallOracleIfChallengerHasInsufficientStakedTokens() {
    state = state.stakeTokens(challenger, disputeTokenCost - 1, null);

    from(withdrawalContract);
    state = invokeCreateDisputePoll();
    ContractEventInteraction withdrawalInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertThat(withdrawalInteraction.contract).isEqualTo(withdrawalContract);
    byte[] actualRpc = SafeDataOutputStream.serialize(withdrawalInteraction.rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            SmallOracleRpc.sendPollResult(
                smallOracleResultInvocation,
                oracleId,
                disputeId1,
                LargeOracleContract.INSUFFICIENT_TOKENS));
    assertThat(actualRpc).isEqualTo(expectedRpc);
    assertThat(state).isNotNull();
  }

  @Test
  @DisplayName(
      "Trying to start a dispute with expired tokens from a small oracle, does not start dispute.")
  public void notifyCallingSmallOracleIfChallengerHasExpiredTokens() {
    long expirationTimestamp = 100L;
    setBlockProductionTime(expirationTimestamp + 1);
    state = state.stakeTokens(challenger, disputeTokenCost, expirationTimestamp);

    from(withdrawalContract);
    state = invokeCreateDisputePoll();
    ContractEventInteraction withdrawalInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertThat(withdrawalInteraction.contract).isEqualTo(withdrawalContract);
    byte[] actualRpc = SafeDataOutputStream.serialize(withdrawalInteraction.rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            SmallOracleRpc.sendPollResult(
                smallOracleResultInvocation,
                oracleId,
                disputeId1,
                LargeOracleContract.INSUFFICIENT_TOKENS));
    assertThat(actualRpc).isEqualTo(expectedRpc);
    assertThat(state).isNotNull();
  }

  @Test
  public void errorIsThrownIfChallengerHasInsufficientStakedTokens() {
    state = state.stakeTokens(challenger, disputeTokenCost - 1, null);
    from(withdrawalContract);
    assertThatThrownBy(this::invokeCreateDisputePollWithThrowError)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("{} has insufficient staked tokens to initiate a dispute poll");
  }

  @Test
  @DisplayName(
      "Trying to create a dispute with an expired association as a challenger, throws exception")
  public void challengerHasExpiredTokens() {
    long expirationTimestamp = 100L;
    state = state.stakeTokens(challenger, disputeTokenCost, expirationTimestamp);

    from(withdrawalContract);
    setBlockProductionTime(expirationTimestamp + 1);
    assertThatThrownBy(this::invokeCreateDisputePollWithThrowError)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Tokens associated to contract have expired, and cannot be locked to oracle");
  }

  @Test
  public void voteOnDispute() {
    Dispute dispute = generateDispute();
    OracleDisputeId oracleDisputeId = new OracleDisputeId(withdrawalContract, oracleId, disputeId1);
    state = state.setDispute(oracleDisputeId, dispute);

    from(address0);
    state = invokeVote(0, withdrawalContract, disputeId1);

    dispute = state.getDispute(oracleDisputeId);
    assertThat(dispute.getVotes().get(0)).isEqualTo(0);
  }

  @Test
  public void multipleDisputes() {
    Dispute dispute = generateDispute();
    OracleDisputeId oracleDisputeId1 =
        new OracleDisputeId(withdrawalContract, oracleId, disputeId1);
    OracleDisputeId oracleDisputeId2 =
        new OracleDisputeId(withdrawalContract, oracleId, disputeId2);
    OracleDisputeId oracleDisputeId3 = new OracleDisputeId(depositContract, oracleId, disputeId1);
    OracleDisputeId oracleDisputeId4 = new OracleDisputeId(depositContract, oracleId, disputeId2);
    state =
        state
            .setDispute(oracleDisputeId1, dispute)
            .setDispute(oracleDisputeId2, dispute)
            .setDispute(oracleDisputeId3, dispute)
            .setDispute(oracleDisputeId4, dispute);

    from(address0);
    state = invokeVote(0, withdrawalContract, disputeId1);
    state = invokeVote(1, withdrawalContract, disputeId2);
    state = invokeVote(2, depositContract, disputeId1);
    state = invokeVote(3, depositContract, disputeId2);

    dispute = state.getDispute(oracleDisputeId1);
    assertThat(dispute.getVotes().get(0)).isEqualTo(0);
    dispute = state.getDispute(oracleDisputeId2);
    assertThat(dispute.getVotes().get(0)).isEqualTo(1);
    dispute = state.getDispute(oracleDisputeId3);
    assertThat(dispute.getVotes().get(0)).isEqualTo(2);
    dispute = state.getDispute(oracleDisputeId4);
    assertThat(dispute.getVotes().get(0)).isEqualTo(3);
  }

  @Test
  public void twoThirdsOfTheVoteTriggerResultBeingSent() {
    state = state.stakeTokens(challenger, disputeTokenCost, null);
    from(depositContract);
    state = invokeCreateDisputePoll();

    from(address1);
    state = invokeVote(0, depositContract, disputeId1);
    from(address2);
    state = invokeVote(0, depositContract, disputeId1);
    ContractEventInteraction contractEventInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertThat(contractEventInteraction.contract).isEqualTo(depositContract);

    byte[] actualRpc = SafeDataOutputStream.serialize(contractEventInteraction.rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            SmallOracleRpc.sendPollResult(smallOracleResultInvocation, oracleId, disputeId1, 0));
    assertThat(actualRpc).isEqualTo(expectedRpc);

    OracleDisputeId oracleDisputeId = new OracleDisputeId(depositContract, oracleId, disputeId1);
    assertThat(state.getDispute(oracleDisputeId)).isNull();
    StakedTokens accountTokens = state.getTokensStakedBy(challenger);
    assertThat(accountTokens.getAllocatableTokens()).isEqualTo(disputeTokenCost);
    assertThat(accountTokens.getLockedToDispute(oracleDisputeId)).isEqualTo(0L);
  }

  @Test
  public void challengersTokensAreBurnedIfNoFraudWasDetected() {
    state = state.stakeTokens(challenger, disputeTokenCost, null);
    from(withdrawalContract);
    state = invokeCreateDisputePoll();

    from(address2);
    state = invokeVote(-1, withdrawalContract, disputeId1);
    from(address0);
    state = invokeVote(-1, withdrawalContract, disputeId1);
    ContractEventInteraction contractEventInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertThat(contractEventInteraction.contract).isEqualTo(withdrawalContract);

    OracleDisputeId oracleDisputeId = new OracleDisputeId(withdrawalContract, oracleId, disputeId1);
    assertThat(state.getDispute(oracleDisputeId)).isNull();
    StakedTokens accountTokens = state.getTokensStakedBy(challenger);
    assertThat(accountTokens.getAllocatableTokens()).isEqualTo(0L);
    assertThat(accountTokens.getLockedToDispute(oracleDisputeId)).isEqualTo(0L);

    verifyBurnTokensUpdateSent(List.of(challenger));
  }

  @Test
  public void challengersTokensAreNotBurnedIfAmountIsZero() {
    state = state.stakeTokens(challenger, 5000, null);
    from(priceOracleContract);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.CREATE_DISPUTE_POLL);
              challenger.write(rpc);
              rpc.writeLong(0);
              rpc.writeLong(oracleId);
              rpc.writeLong(disputeId1);
              rpc.writeByte(smallOracleResultInvocation);
              rpc.writeByte(smallOracleCounterClaimInvocation);
            });

    from(address2);
    state = invokeVote(-1, priceOracleContract, disputeId1);
    from(address0);
    state = invokeVote(-1, priceOracleContract, disputeId1);
    ContractEventInteraction priceOracleInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertThat(priceOracleInteraction.contract).isEqualTo(priceOracleContract);

    OracleDisputeId oracleDisputeId =
        new OracleDisputeId(priceOracleContract, oracleId, disputeId1);
    assertThat(state.getDispute(oracleDisputeId)).isNull();
    StakedTokens accountTokens = state.getTokensStakedBy(challenger);
    assertThat(accountTokens.getAllocatableTokens()).isEqualTo(5000L);
    assertThat(accountTokens.getLockedToDispute(oracleDisputeId)).isEqualTo(0L);
    verifyBurnTokensUpdateSent(List.of());
  }

  @Test
  public void onlyOracleMembersCanVote() {
    from(challenger);
    assertThatThrownBy(() -> invokeVote(0, withdrawalContract, disputeId1))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only members of the large oracle can vote on disputes");
  }

  @Test
  public void cannotVoteWhenNoDisputeInProgress() {
    from(address0);
    assertThatThrownBy(() -> invokeVote(0, withdrawalContract, disputeId1))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No dispute with this id to vote on");
  }

  @Test
  public void addCounterClaim() {
    Dispute dispute = generateDispute();
    OracleDisputeId oracleDisputeId = new OracleDisputeId(withdrawalContract, oracleId, disputeId1);
    state = state.setDispute(oracleDisputeId, dispute);
    assertThat(state.getDispute(oracleDisputeId).hasAddedCounterClaim(address1)).isFalse();

    Random rng = new Random();
    byte[] counterClaim = new byte[32];
    rng.nextBytes(counterClaim);

    from(address1);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.ADD_DISPUTE_COUNTER_CLAIM);
              withdrawalContract.write(rpc);
              rpc.writeLong(oracleId);
              rpc.writeLong(disputeId1);
              rpc.writeDynamicBytes(counterClaim);
            });

    ContractEventInteraction contractEventInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertThat(contractEventInteraction.contract).isEqualTo(withdrawalContract);
    assertThat(state.getDispute(oracleDisputeId).hasAddedCounterClaim(address1)).isTrue();
    byte[] actualRpc = SafeDataOutputStream.serialize(contractEventInteraction.rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            SmallOracleRpc.addCounterClaim(smallOracleCounterClaimInvocation, counterClaim));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  @Test
  public void onlyOracleMembersCanAddCounterClaim() {
    from(challenger);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.ADD_DISPUTE_COUNTER_CLAIM);
                      oracle0.createAddress().write(rpc);
                      rpc.writeLong(1);
                      rpc.writeLong(1);
                      rpc.writeDynamicBytes(new byte[0]);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only members of the large oracle can add counter-claims to disputes");
  }

  @Test
  public void cannotAddCounterClaimIfDisputeDoesNotExists() {
    from(address2);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.ADD_DISPUTE_COUNTER_CLAIM);
                      oracle0.createAddress().write(rpc);
                      rpc.writeLong(oracleId);
                      rpc.writeLong(1);
                      rpc.writeDynamicBytes(new byte[0]);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No dispute with this id in progress");
  }

  @Test
  public void onlyOneCounterClaimPerOracleMember() {
    Dispute dispute = generateDispute();
    OracleDisputeId oracleDisputeId = new OracleDisputeId(withdrawalContract, oracleId, disputeId1);
    state = state.setDispute(oracleDisputeId, dispute.recordCounterClaimChallenger(address1));

    from(address1);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.ADD_DISPUTE_COUNTER_CLAIM);
                      withdrawalContract.write(rpc);
                      rpc.writeLong(oracleId);
                      rpc.writeLong(disputeId1);
                      rpc.writeDynamicBytes(new byte[0]);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only one counter-claim per oracle member allowed");
  }

  @Test
  public void burnStakedTokens() {
    state =
        state
            .stakeTokens(address0, 250_000L, null)
            .stakeTokens(address1, 150_000L, null)
            .stakeTokens(address2, 500_000L, null);

    from(withdrawalContract);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.BURN_STAKED_TOKENS);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, oracleAddresses);
              rpc.writeLong(disputeTokenCost);
            });

    assertThat(state.getTokensStakedBy(address0))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(250_000L - disputeTokenCost);
    assertThat(state.getTokensStakedBy(address1))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(150_000L - disputeTokenCost);
    assertThat(state.getTokensStakedBy(address2))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(500_000L - disputeTokenCost);

    verifyBurnTokensUpdateSent(oracleAddresses);

    from(depositContract);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.BURN_STAKED_TOKENS);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, oracleAddresses);
              rpc.writeLong(disputeTokenCost);
            });

    assertThat(state.getTokensStakedBy(address0))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(250_000L - 2 * disputeTokenCost);
    assertThat(state.getTokensStakedBy(address1))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(150_000L - 2 * disputeTokenCost);
    assertThat(state.getTokensStakedBy(address2))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(500_000L - 2 * disputeTokenCost);

    verifyBurnTokensUpdateSent(oracleAddresses);
  }

  @Test
  public void onlyOneOfTheSmallOracleContractsCanBurnTokens() {
    from(challenger);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.BURN_STAKED_TOKENS);
                      BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, oracleAddresses);
                      rpc.writeLong(disputeTokenCost);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Tokens can only be burned by one of the small oracle contracts");
  }

  @Test
  public void updateExchangeRate() {
    from(governanceUpdates);

    LargeOracleContractState updatedState = invokeChangeExchangeRate(40, 1);
    assertThat(updatedState.convertToMpcTokens(new Fraction(200, 1))).isEqualTo(5);

    updatedState = invokeChangeExchangeRate(3, 5);
    assertThat(updatedState.convertToMpcTokens(new Fraction(20, 1))).isEqualTo(34);
  }

  @Test
  public void updateExchangeNotFromGovernanceUpdates() {
    from(withdrawalContract);
    assertThatThrownBy(() -> invokeChangeExchangeRate(2, 1))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Exchange rate can only be updated by governance updates");
  }

  private LargeOracleContractState invokeChangeExchangeRate(int numerator, int denominator) {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(LargeOracleContract.Invocations.CHANGE_EXCHANGE_RATE);
          rpc.writeInt(numerator);
          rpc.writeInt(denominator);
        });
  }

  @Test
  public void recalibrateByocTwins() {
    from(withdrawalContract);

    OuterCoinTest conversionRates = new OuterCoinTest();
    context.setGlobalAccountPluginState(conversionRates);

    String coin1 = "coin";
    long coin1Amount = 10;
    state = state.setMpcToUsdExchangeRate(new Fraction(3, 1));
    for (BlockchainAddress oracle : oracleAddresses) {
      state = state.stakeTokens(oracle, 100, null);
    }
    state = invokeRecalibrateByoc(coin1, Unsigned256.create(coin1Amount));
    assertThat(state.getByocTwinToMpcTradeDeal(coin1).getByocTwins())
        .isEqualTo(Unsigned256.create(coin1Amount));

    long mpcTokens =
        state.convertToMpcTokens(
            new Fraction(
                coin1Amount * conversionRates.coins.coins.get(0).conversionRate.getNumerator(),
                LargeOracleContract.GAS_TO_USD
                    * conversionRates.coins.coins.get(0).conversionRate.getDenominator()));
    assertThat(state.getByocTwinToMpcTradeDeal(coin1).getMpcTokens()).isEqualTo(mpcTokens);

    String coin2 = "dogecoin";
    long coin2Amount = 5;
    state = invokeRecalibrateByoc(coin1, Unsigned256.create(coin1Amount));
    state = invokeRecalibrateByoc(coin2, Unsigned256.create(coin2Amount));
    assertThat(state.getByocTwinToMpcTradeDeal(coin1).getByocTwins())
        .isEqualTo(Unsigned256.create(coin1Amount * 2));
    assertThat(state.getByocTwinToMpcTradeDeal(coin1).getMpcTokens()).isEqualTo(mpcTokens * 2);

    mpcTokens =
        state.convertToMpcTokens(
            new Fraction(
                coin2Amount * conversionRates.coins.coins.get(1).conversionRate.getNumerator(),
                LargeOracleContract.GAS_TO_USD
                    * conversionRates.coins.coins.get(1).conversionRate.getDenominator()));
    assertThat(state.getByocTwinToMpcTradeDeal(coin2).getByocTwins())
        .isEqualTo(Unsigned256.create(coin2Amount));
    assertThat(state.getByocTwinToMpcTradeDeal(coin2).getMpcTokens()).isEqualTo(mpcTokens);
  }

  @Test
  public void recalibrateOnlySmallOracleCanRecalibrate() {
    from(challenger);

    OuterCoinTest conversionRates = new OuterCoinTest();
    context.setGlobalAccountPluginState(conversionRates);

    String coin1 = "coin";
    long coin1Amount = 10;
    assertThatThrownBy(() -> invokeRecalibrateByoc(coin1, Unsigned256.create(coin1Amount)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only a small oracle can recalibrate the BYOC twins");
  }

  @Test
  public void recalibrateCoinDoesntExist() {
    from(withdrawalContract);

    OuterCoinTest conversionRates = new OuterCoinTest();
    context.setGlobalAccountPluginState(conversionRates);

    String coin1 = "othercoin";
    long coin1Amount = 10;
    assertThatThrownBy(() -> invokeRecalibrateByoc(coin1, Unsigned256.create(coin1Amount)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("The symbol 'othercoin' was not found");
  }

  @Test
  public void recalibrateByocTwinsConfiscation() {
    from(depositContract);
    OuterCoinTest conversionRates = new OuterCoinTest();
    context.setGlobalAccountPluginState(conversionRates);

    state = state.setMpcToUsdExchangeRate(new Fraction(3, 2));
    String coin = "coin";
    long coinAmount = 1000;
    for (BlockchainAddress oracle : oracleAddresses) {
      state = state.stakeTokens(oracle, coinAmount, null);
    }
    state = invokeRecalibrateByoc(coin, Unsigned256.create(coinAmount));

    long mpcTokens =
        state.convertToMpcTokens(
            new Fraction(
                coinAmount * conversionRates.coins.coins.get(0).conversionRate.getNumerator(),
                LargeOracleContract.GAS_TO_USD
                    * conversionRates.coins.coins.get(0).conversionRate.getDenominator()));
    long equalShares = mpcTokens / oracleAddresses.size();
    long rest = mpcTokens % oracleAddresses.size();

    List<LocalPluginStateUpdate> updateLocalAccountPluginStates =
        context.getUpdateLocalAccountPluginStates();
    assertThat(updateLocalAccountPluginStates.size()).isEqualTo(oracleAddresses.size());
    for (int i = 0; i < updateLocalAccountPluginStates.size(); i++) {
      long burnedAmount = equalShares + (i < rest ? 1 : 0);
      byte[] actualRpc = updateLocalAccountPluginStates.get(i).getRpc();
      byte[] expectedRpc =
          AccountPluginRpc.burnStakedTokens(burnedAmount, context.getContractAddress());
      assertThat(actualRpc).isEqualTo(expectedRpc);
      LocalPluginStateUpdate update = updateLocalAccountPluginStates.get(i);
      assertThat(update.getContext()).isEqualTo(oracleAddresses.get(i));
      assertThat(state.getTokensStakedBy(oracleAddresses.get(i)).getAllocatableTokens())
          .isEqualTo(coinAmount - burnedAmount);
    }
  }

  @Test
  void convertByocTwinsToMpcTokens() {
    OuterCoinTest conversionRates = new OuterCoinTest();
    context.setGlobalAccountPluginState(conversionRates);
    // 1.0000 MPC is 40 cents
    state = state.setMpcToUsdExchangeRate(new Fraction(40, 1_0000));

    // 1 ETH == 4000 USD == 400_000_000 GAS
    Unsigned256 oneEth = Unsigned256.create(1_000_000_000_000_000_000L);
    assertThat(LargeOracleContract.convertByocToGas(context, ETH, oneEth))
        .isEqualTo(Unsigned256.create(400_000_000L));

    // 1 MPC = 0.4 USD
    assertThat(LargeOracleContract.convertByocTwinsToMpcTokens(state, context, ETH, oneEth))
        .isEqualTo(10_000_0000);
  }

  @Test
  public void sellByocTwinsCorrectCallback() {
    from(withdrawalContract);
    String byocTwinSymbol = "coin";

    state = state.addByocTwinToMpcDeal(byocTwinSymbol, Unsigned256.create(10), 5);

    LargeOracleContractState updatedState = invokeSellByocTwins(byocTwinSymbol, Unsigned256.TEN, 5);
    assertCorrectCallback(byocTwinSymbol, Unsigned256.TEN, 5);
    assertCorrectWithdraw(byocTwinSymbol, Unsigned256.TEN, 0);
    assertThat(updatedState.getByocTwinToMpcTradeDeal(byocTwinSymbol).getByocTwins())
        .isEqualTo(Unsigned256.ZERO);
    assertThat(updatedState.getByocTwinToMpcTradeDeal(byocTwinSymbol).getMpcTokens()).isEqualTo(0);

    updatedState = invokeSellByocTwins(byocTwinSymbol, Unsigned256.create(4), 2);
    assertCorrectCallback(byocTwinSymbol, Unsigned256.create(4), 2);
    assertCorrectWithdraw(byocTwinSymbol, Unsigned256.create(4), 1);
    assertThat(updatedState.getByocTwinToMpcTradeDeal(byocTwinSymbol).getByocTwins())
        .isEqualTo(Unsigned256.create(6));
    assertThat(updatedState.getByocTwinToMpcTradeDeal(byocTwinSymbol).getMpcTokens()).isEqualTo(3);

    updatedState = invokeSellByocTwins(byocTwinSymbol, Unsigned256.create(5), 1);
    assertCorrectCallback(byocTwinSymbol, Unsigned256.create(4), 2);
    assertCorrectWithdraw(byocTwinSymbol, Unsigned256.create(4), 2);
    assertThat(updatedState.getByocTwinToMpcTradeDeal(byocTwinSymbol).getByocTwins())
        .isEqualTo(Unsigned256.create(6));
    assertThat(updatedState.getByocTwinToMpcTradeDeal(byocTwinSymbol).getMpcTokens()).isEqualTo(3);

    updatedState = invokeSellByocTwins(byocTwinSymbol, Unsigned256.create(11), 0);
    assertCorrectCallback(byocTwinSymbol, Unsigned256.create(10), 5);
    assertCorrectWithdraw(byocTwinSymbol, Unsigned256.create(10), 3);
    assertThat(updatedState.getByocTwinToMpcTradeDeal(byocTwinSymbol).getByocTwins())
        .isEqualTo(Unsigned256.ZERO);
    assertThat(updatedState.getByocTwinToMpcTradeDeal(byocTwinSymbol).getMpcTokens()).isEqualTo(0);
  }

  private void assertCorrectCallback(
      String byocTwinSymbol, Unsigned256 byocAmount, long mpcTokens) {
    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            LargeOracleContract.Callbacks.sellByocTwinsCallback(
                withdrawalContract, byocTwinSymbol, byocAmount, mpcTokens));
    assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  private void assertCorrectWithdraw(
      String byocTwinSymbol, Unsigned256 byocAmount, int interactionNumber) {
    byte[] actualCallbackRpc =
        context.getUpdateLocalAccountPluginStates().get(interactionNumber).getRpc();
    byte[] expectedCallbackRpc = AccountPluginRpc.deductCoinBalance(byocTwinSymbol, byocAmount);
    assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  public void sellByocTwinsWrongAmounts() {
    from(withdrawalContract);
    String byocTwinSymbol = "coin";
    context.setGlobalAccountPluginState(new OuterCoinTest());

    state = state.addByocTwinToMpcDeal(byocTwinSymbol, Unsigned256.create(10), 5);

    assertThatCode(() -> state = invokeSellByocTwins(byocTwinSymbol, Unsigned256.create(0), 0))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount to sell must be positive");

    assertThatCode(() -> state = invokeSellByocTwins("other coin", Unsigned256.create(5), 0))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No MPC tokens for sale for this BYOC twin");

    assertThatCode(() -> state = invokeSellByocTwins(byocTwinSymbol, Unsigned256.create(1), 0))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Not enough BYOC twins to buy an MPC token");

    assertThatCode(() -> state = invokeSellByocTwins(byocTwinSymbol, Unsigned256.create(3), 2))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("The trade will give less than the expected amount of MPC tokens");
  }

  @Test
  public void onSellByocTwinsCallback() {
    from(withdrawalContract);
    String byocTwinSymbol = "coin";
    context.setGlobalAccountPluginState(new OuterCoinTest());

    state = state.addByocTwinToMpcDeal(byocTwinSymbol, Unsigned256.create(10), 5);

    CallbackContext callbackContext = getCallbackContext(true);
    state = getByocTwinsCallback(callbackContext, byocTwinSymbol, Unsigned256.create(8), 4);
    assertThat(state.getByocTwinToMpcTradeDeal(byocTwinSymbol).getByocTwins())
        .isEqualTo(Unsigned256.TEN);
    assertThat(state.getByocTwinToMpcTradeDeal(byocTwinSymbol).getMpcTokens()).isEqualTo(5);

    byte[] actualRpc = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] expectedRpc = AccountPluginRpc.addTokenBalance(4);
    assertThat(actualRpc).isEqualTo(expectedRpc);

    state = getByocTwinsCallback(callbackContext, byocTwinSymbol, Unsigned256.create(2), 1);
    assertThat(state.getByocTwinToMpcTradeDeal(byocTwinSymbol).getByocTwins())
        .isEqualTo(Unsigned256.TEN);
    assertThat(state.getByocTwinToMpcTradeDeal(byocTwinSymbol).getMpcTokens()).isEqualTo(5);

    state =
        getByocTwinsCallback(getCallbackContext(false), byocTwinSymbol, Unsigned256.create(8), 4);
    assertThat(state.getByocTwinToMpcTradeDeal(byocTwinSymbol).getByocTwins())
        .isEqualTo(Unsigned256.create(18));
    assertThat(state.getByocTwinToMpcTradeDeal(byocTwinSymbol).getMpcTokens()).isEqualTo(9);
  }

  private LargeOracleContractState getByocTwinsCallback(
      CallbackContext callbackContext,
      String byocTwinSymbol,
      Unsigned256 byocTwinAmount,
      long mpcTokens) {
    return serialization.callback(
        context,
        callbackContext,
        state,
        LargeOracleContract.Callbacks.sellByocTwinsCallback(
            withdrawalContract, byocTwinSymbol, byocTwinAmount, mpcTokens));
  }

  private LargeOracleContractState invokeSellByocTwins(
      String byocTwinSymbol, Unsigned256 byocAmount, long minAmountOfMpcToBuy) {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(LargeOracleContract.Invocations.SELL_BYOC_TWINS);
          rpc.writeString(byocTwinSymbol);
          byocAmount.write(rpc);
          rpc.writeLong(minAmountOfMpcToBuy);
        });
  }

  private LargeOracleContractState invokeRecalibrateByoc(
      String byocTwinSymbol, Unsigned256 byocAmount) {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(LargeOracleContract.Invocations.RECALIBRATE_BYOC_TWINS);
          rpc.writeString(byocTwinSymbol);
          byocAmount.write(rpc);
          BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, oracleAddresses);
        });
  }

  private LargeOracleContractState invokeVote(
      int vote, BlockchainAddress smallOracle, long disputeId) {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(LargeOracleContract.Invocations.VOTE_ON_DISPUTE);
          smallOracle.write(rpc);
          rpc.writeLong(oracleId);
          rpc.writeLong(disputeId);
          rpc.writeInt(vote);
        });
  }

  private Dispute generateDispute() {
    return Dispute.create(
        challenger,
        smallOracleResultInvocation,
        smallOracleCounterClaimInvocation,
        state.getOracleMembers().size());
  }

  private LargeOracleContractState invokeCreateDisputePoll() {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(LargeOracleContract.Invocations.CREATE_DISPUTE_POLL);
          challenger.write(rpc);
          rpc.writeLong(disputeTokenCost);
          rpc.writeLong(oracleId);
          rpc.writeLong(disputeId1);
          rpc.writeByte(smallOracleResultInvocation);
          rpc.writeByte(smallOracleCounterClaimInvocation);
        });
  }

  private LargeOracleContractState invokeCreateDisputePollWithThrowError() {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(LargeOracleContract.Invocations.CREATE_DISPUTE_POLL_WITH_ERROR_THROW);
          challenger.write(rpc);
          rpc.writeLong(disputeTokenCost);
          rpc.writeLong(oracleId);
          rpc.writeLong(disputeId1);
          rpc.writeByte(smallOracleResultInvocation);
          rpc.writeByte(smallOracleCounterClaimInvocation);
        });
  }

  private void verifyBurnTokensUpdateSent(List<BlockchainAddress> accounts) {
    List<LocalPluginStateUpdate> updateLocalAccountPluginStates =
        context.getUpdateLocalAccountPluginStates();
    assertThat(accounts.size()).isEqualTo(updateLocalAccountPluginStates.size());

    for (int i = 0; i < accounts.size(); i++) {
      byte[] actualRpc = updateLocalAccountPluginStates.get(i).getRpc();
      byte[] expectedRpc =
          AccountPluginRpc.burnStakedTokens(disputeTokenCost, context.getContractAddress());
      assertThat(actualRpc).isEqualTo(expectedRpc);
      BlockchainAddress account = accounts.get(i);
      LocalPluginStateUpdate localPluginStateUpdate = updateLocalAccountPluginStates.get(i);
      assertThat(localPluginStateUpdate.getContext()).isEqualTo(account);
    }
  }

  private void from(BlockchainAddress from) {
    context =
        new SysContractContextTest(context.getBlockProductionTime(), context.getBlockTime(), from);
  }

  private void setBlockProductionTime(long blockProductionTime) {
    context =
        new SysContractContextTest(blockProductionTime, context.getBlockTime(), context.getFrom());
  }

  @Test
  public void replaceLargeOracle() {
    List<BlockchainPublicKey> newOracle =
        List.of(
            new KeyPair(BigInteger.valueOf(5)).getPublic(),
            new KeyPair(BigInteger.valueOf(6)).getPublic(),
            new KeyPair(BigInteger.valueOf(7)).getPublic());
    List<BlockchainAddress> newOracleAddress =
        List.of(
            BlockchainAddress.fromString("000000000000000000000000000000000000000055"),
            BlockchainAddress.fromString("000000000000000000000000000000000000000066"),
            BlockchainAddress.fromString("000000000000000000000000000000000000000077"));

    BlockchainPublicKey newPublicKey = new KeyPair(BigInteger.TEN).getPublic();

    from(bpOrchestrationContract);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REPLACE_LARGE_ORACLE);
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(rpc, newOracle);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, newOracleAddress);
              newPublicKey.write(rpc);
            });

    assertThat(state.getOracleKey()).isEqualTo(newPublicKey);
    assertThat(state.getOracleMembers())
        .isEqualTo(LargeOracleContract.createOracleMembers(newOracleAddress, newOracle));
  }

  @Test
  public void onlyBpOrchestratorCanReplaceLargeOracle() {
    List<BlockchainPublicKey> newOracle =
        List.of(
            new KeyPair(BigInteger.valueOf(1)).getPublic(),
            new KeyPair(BigInteger.valueOf(2)).getPublic(),
            new KeyPair(BigInteger.valueOf(3)).getPublic());
    List<BlockchainAddress> newOracleAddress =
        List.of(
            BlockchainAddress.fromString("000000000000000000000000000000000000000011"),
            BlockchainAddress.fromString("000000000000000000000000000000000000000022"),
            BlockchainAddress.fromString("000000000000000000000000000000000000000033"));

    BlockchainPublicKey newPublicKey = new KeyPair(BigInteger.TEN).getPublic();
    from(withdrawalContract);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.REPLACE_LARGE_ORACLE);
                      BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(rpc, newOracle);
                      BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, newOracleAddress);
                      newPublicKey.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only orchestration contract can replace the large oracle");
  }

  @Test
  public void resetDisputeOnNewOracle() {
    Dispute dispute = generateDispute();
    dispute = dispute.addVote(0, 1);
    OracleDisputeId disputeId = new OracleDisputeId(withdrawalContract, oracleId, disputeId1);
    state = state.setDispute(disputeId, dispute);
    assertThat(state.getActiveDisputes().getValue(disputeId).countVotes()).isEqualTo(1);
    from(bpOrchestrationContract);
    final KeyPair oracleKey4 = new KeyPair(BigInteger.valueOf(444));
    final KeyPair oracleKey5 = new KeyPair(BigInteger.valueOf(555));
    final BlockchainAddress address4 =
        BlockchainAddress.fromString("000000000000000000000000000000000000000044");
    final BlockchainAddress address5 =
        BlockchainAddress.fromString("000000000000000000000000000000000000000055");
    ArrayList<BlockchainPublicKey> newOracleKeys = new ArrayList<>(oracleKeys);
    newOracleKeys.add(oracleKey4.getPublic());
    newOracleKeys.add(oracleKey5.getPublic());
    ArrayList<BlockchainAddress> newOracleAddresses = new ArrayList<>(oracleAddresses);
    newOracleAddresses.add(address4);
    newOracleAddresses.add(address5);
    BlockchainPublicKey newPubKey = new KeyPair(BigInteger.ONE).getPublic();
    LargeOracleContractState newState =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REPLACE_LARGE_ORACLE);
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(rpc, newOracleKeys);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, newOracleAddresses);
              newPubKey.write(rpc);
            });

    AvlTree<OracleDisputeId, Dispute> activeDisputesNewOracle = newState.getActiveDisputes();
    assertThat(activeDisputesNewOracle.size()).isEqualTo(1);
    assertThat(activeDisputesNewOracle.getValue(disputeId).countVotes()).isEqualTo(-1);
    assertThat(activeDisputesNewOracle.getValue(disputeId).getVotes().size())
        .isEqualTo(newOracleAddresses.size());
  }

  @Test
  @DisplayName(
      "Oracle members with expired associations cannot be chosen as part of the new small oracle,"
          + " when a new small oracle is requested.")
  public void requestNewOracleWithExpiredAssociations() {
    final List<BlockchainPublicKey> oracleKeys = List.of(oracle0, oracle1, oracle2, oracle3);
    final List<BlockchainAddress> oracleAddresses = List.of(address0, address1, address2, address3);
    state =
        serialization.create(
            context,
            rpc -> {
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(rpc, oracleKeys);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, oracleAddresses);
              oraclePublicKey.write(rpc);
              bpOrchestrationContract.write(rpc);
              governanceUpdates.write(rpc);
            });

    from(withdrawalContract);
    byte[] additionalData = Hash.create(s -> s.writeInt(12)).getBytes();
    long requiredStake = 10;
    int randomCallback = 100;

    long earlyExpiration = 100L;
    long lateExpiration = 200L;
    state =
        state
            .stakeTokens(address3, requiredStake, earlyExpiration)
            .stakeTokens(address1, requiredStake, lateExpiration)
            .stakeTokens(address2, requiredStake, null)
            .stakeTokens(address0, requiredStake, null);

    setBlockProductionTime(lateExpiration);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requiredStake);
              rpc.writeByte(randomCallback);
              rpc.writeDynamicBytes(additionalData);
            });
    ContractEventInteraction withdrawalInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertThat(withdrawalInteraction.contract).isEqualTo(withdrawalContract);
    assertThat(state.getPendingMessages()).hasSize(1);

    List<BlockchainPublicKey> expectedOracle = List.of(oracle2, oracle1, oracle0);
    String encodedPublicKeys = getEncodedPublicKeys(expectedOracle);
    Hash expectedHash =
        Hash.create(
            s -> {
              s.write(additionalData);
              s.write(Hex.decode(encodedPublicKeys));
            });
    assertThat(state.getPendingMessages().get(0))
        .isEqualTo(new SigningRequest(0, expectedHash, context.getCurrentTransactionHash()));

    byte[] actualRpc = SafeDataOutputStream.serialize(withdrawalInteraction.rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            SmallOracleRpc.updateRequest(
                randomCallback, List.of(address2, address1, address0), expectedOracle));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  @Test
  public void requestNewOracle() {
    from(withdrawalContract);
    byte[] additionalData = Hash.create(s -> s.writeInt(12)).getBytes();
    int requireStake = 10;
    // made up value.
    int callback = 100;

    state =
        state
            .stakeTokens(address0, 10, null)
            .stakeTokens(address1, 10, null)
            .stakeTokens(address2, 10, null);

    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requireStake);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(additionalData);
            });
    ContractEventInteraction withdrawalInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertThat(withdrawalInteraction.contract).isEqualTo(withdrawalContract);

    String encodedPublicKeys = getEncodedPublicKeys(shuffledOracleKeys);

    assertThat(state.getPendingMessages()).hasSize(1);
    Hash expected =
        Hash.create(
            s -> {
              s.write(additionalData);
              s.write(Hex.decode(encodedPublicKeys));
            });
    assertThat(state.getPendingMessages().get(0))
        .isEqualTo(
            new SigningRequest(
                0,
                expected,
                Hash.create(
                    (stream) -> {
                      stream.writeLong(context.getBlockProductionTime());
                    })));
    assertUpdateCallback(withdrawalInteraction.rpc, callback);
  }

  @Test
  void requestSignature() {
    from(governanceUpdates);

    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_SIGNATURE);
              hash.write(rpc);
            });

    assertThat(state.getPendingMessages()).hasSize(1);
    SigningRequest signingRequest = state.getPendingMessages().get(0);
    assertThat(signingRequest.getMessageHash()).isEqualTo(hash);
  }

  @Test
  void requestSignatureNotAllowedFromNonGovernance() {
    from(address0);

    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.REQUEST_SIGNATURE);
                      hash.write(rpc);
                    }))
        .hasMessage("Only governance update can request a signature");
  }

  @Test
  void largeOracleShouldNotReleasePendingTokensOnRetry() {
    from(withdrawalContract);
    byte[] additionalData = Hash.create(s -> s.writeInt(333)).getBytes();
    int requireStake = 10;
    // Made up value
    int callback = 42;

    state =
        state
            .stakeTokens(address0, 5, null)
            .stakeTokens(address1, 15, null)
            .stakeTokens(address2, 15, null);
    state =
        state
            .lockTokensToOracle(address1, withdrawalContract, 5, context.getBlockProductionTime())
            .lockTokensToOracle(address2, withdrawalContract, 5, context.getBlockProductionTime());

    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requireStake);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(additionalData);
            });
    assertThat(state.getTokensStakedBy(address1))
        .extracting(stakedTokens -> stakedTokens.getPendingForOracle(withdrawalContract))
        .isEqualTo(5L);
    assertThat(state.getTokensStakedBy(address2))
        .extracting(stakedTokens -> stakedTokens.getPendingForOracle(withdrawalContract))
        .isEqualTo(5L);

    state = state.stakeTokens(address0, 10, null);
    state =
        state.lockTokensToOracle(address0, withdrawalContract, 5, context.getBlockProductionTime());

    state =
        serialization.callback(
            context,
            getCallbackContext(true),
            state,
            LargeOracleContract.Callbacks.associateCallback(address0, 5));

    assertThat(state.getTokensStakedBy(address1))
        .extracting(stakedTokens -> stakedTokens.getPendingForOracle(withdrawalContract))
        .isEqualTo(5L);
    assertThat(state.getTokensStakedBy(address2))
        .extracting(stakedTokens -> stakedTokens.getPendingForOracle(withdrawalContract))
        .isEqualTo(5L);
  }

  @Test
  public void requestNewOracleDoesNotAddMessageToBeSignedIfNoAdditionalData() {
    from(depositContract);
    byte[] empty = new byte[0];
    int requiredStake = 10;
    int callback = 42;

    state =
        state
            .stakeTokens(address0, 10, null)
            .stakeTokens(address1, 10, null)
            .stakeTokens(address2, 10, null);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requiredStake);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(empty);
            });
    ContractEventInteraction depositInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertThat(depositInteraction.contract).isEqualTo(depositContract);
    assertThat(state.getPendingMessages()).hasSize(0);

    byte[] actualRpc = SafeDataOutputStream.serialize(depositInteraction.rpc);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            SmallOracleRpc.updateRequest(callback, shuffledOracleAddress, shuffledOracleKeys));
    assertThat(actualRpc).isEqualTo(expectedRpc);
    assertThat(state.getTokensStakedBy(address0))
        .extracting(tokens -> tokens.getLockedToOracle(depositContract))
        .isEqualTo((long) requiredStake);
    assertThat(state.getTokensStakedBy(address1))
        .extracting(tokens -> tokens.getLockedToOracle(depositContract))
        .isEqualTo((long) requiredStake);
    assertThat(state.getTokensStakedBy(address2))
        .extracting(tokens -> tokens.getLockedToOracle(depositContract))
        .isEqualTo((long) requiredStake);
  }

  @Test
  public void onlyApprovedContractsCanRequestOracleChanges() {
    from(address0);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
                      rpc.writeLong(10);
                      rpc.writeByte(22);
                      rpc.writeDynamicBytes(new byte[0]);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only a small oracle can request a small oracle change");
  }

  @Test
  public void insufficientMembersWithEnoughStakeForOracleChange() {
    from(withdrawalContract);
    byte[] additionalData = Hash.create(s -> s.writeInt(333)).getBytes();
    int requireStake = 10;
    int callback = 42;
    state = state.stakeTokens(address0, 10, null).stakeTokens(address1, 5, null);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requireStake);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(additionalData);
            });

    OracleUpdateRequest updateRequest = state.getPendingUpdateRequest();
    assertThat(updateRequest.getContractAddress()).isEqualTo(withdrawalContract);
    assertThat(updateRequest.getCallback()).isEqualTo(callback);
    assertThat(updateRequest.getRequiredStake()).isEqualTo(requireStake);
    assertThat(updateRequest.getAdditionalData()).isEqualTo(additionalData);

    state =
        serialization.callback(
            context,
            getCallbackContext(true),
            state,
            LargeOracleContract.Callbacks.associateCallback(address2, 10));

    // Assert still not enough stakes
    assertThat(state.getPendingUpdateRequest()).isNotNull();

    from(depositContract);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requireStake * 2L);
              rpc.writeByte(callback * 2);
              rpc.writeDynamicBytes(new byte[0]);
            });

    state =
        serialization.callback(
            context,
            getCallbackContext(true),
            state,
            LargeOracleContract.Callbacks.associateCallback(address1, requireStake * 10L));
    ContractEventInteraction firstWithdrawalInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertThat(firstWithdrawalInteraction.contract).isEqualTo(withdrawalContract);
    assertUpdateCallback(firstWithdrawalInteraction.rpc, callback);

    from(withdrawalContract);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requireStake);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(additionalData);
            });

    state =
        serialization.callback(
            context,
            getCallbackContext(true),
            state,
            LargeOracleContract.Callbacks.associateCallback(address0, requireStake * 10L));

    state =
        serialization.callback(
            context,
            getCallbackContext(true),
            state,
            LargeOracleContract.Callbacks.associateCallback(address2, requireStake * 10L));

    assertThat(state.getPendingUpdateRequest()).isNull();
    assertThat(firstWithdrawalInteraction.contract).isEqualTo(withdrawalContract);
    ContractEventInteraction depositInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertUpdateCallback(depositInteraction.rpc, callback * 2);
    ContractEventInteraction secondWithdrawalInteraction =
        (ContractEventInteraction) context.getInteractions().get(1);
    assertUpdateCallback(secondWithdrawalInteraction.rpc, callback);
  }

  @Test
  public void pendingTokensReleasedAfter14Days() {
    from(withdrawalContract);
    byte[] additionalData = Hash.create(s -> s.writeInt(333)).getBytes();
    int requireStake = 10;
    int callback = 42;
    state =
        state
            .stakeTokens(address0, 2L * requireStake, null)
            .stakeTokens(address1, 2L * requireStake, null)
            .stakeTokens(address2, requireStake, null);
    // We have two oracle with enough tokens to be in oracles twice and one with enough stakes for a
    // single oracle. The following call to request_new_small_oracle will therefore be fulfilled.
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requireStake);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(additionalData);
            });

    // The next two requests will be added to pending as address2 does not have enough tokens.
    from(depositContract);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requireStake);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(new byte[0]);
            });

    from(withdrawalContract);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requireStake);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(additionalData);
            });

    // Give address2 the required amount to fulfill another oracle. Now the first oracle is
    // released, meaning they now have 10 pending, 10 locked, and 0 free tokens. Previously a loop
    // would check for free + pending tokens meaning the condition would return true as all oracles
    // have enough pending tokens to fulfill the next oracle request, but when calling
    // tryFulfillingUpdateRequests it checked only free tokens. Therefore, it would not update the
    // state, and loop forever.
    state =
        serialization.callback(
            context,
            getCallbackContext(true),
            state,
            LargeOracleContract.Callbacks.associateCallback(address2, requireStake));

    assertThat(state.getPendingUpdateRequest()).isNotNull();

    // After 14 days the pending tokens should be released, and we can fulfill the final oracle
    // request.
    context =
        new SysContractContextTest(
            context.getBlockProductionTime() + Duration.ofDays(14).toMillis() + 1,
            100,
            withdrawalContract);

    state =
        serialization.callback(
            context,
            getCallbackContext(true),
            state,
            LargeOracleContract.Callbacks.associateCallback(address2, 0));

    assertThat(state.getPendingUpdateRequest()).isNull();
  }

  private void assertUpdateCallback(DataStreamSerializable data, int callback) {
    byte[] actualRpc = SafeDataOutputStream.serialize(data);
    byte[] expectedRpc =
        SafeDataOutputStream.serialize(
            SmallOracleRpc.updateRequest(callback, shuffledOracleAddress, shuffledOracleKeys));
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  @Test
  public void oraclesStakesUnlockedAfter14Days() {
    from(depositContract);
    byte[] empty = new byte[0];
    int requiredStake = 10;
    int callback = 42;

    state =
        state
            .stakeTokens(address0, 30, null)
            .stakeTokens(address1, 30, null)
            .stakeTokens(address2, 30, null);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requiredStake);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(empty);
            });
    assertThat(state.getTokensStakedBy(address0))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(20L);
    assertThat(state.getTokensStakedBy(address1))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(20L);
    assertThat(state.getTokensStakedBy(address2))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(20L);
    ContractEventInteraction depositInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertThat(depositInteraction.contract).isEqualTo(depositContract);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(5);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(empty);
            });
    assertThat(state.getTokensStakedBy(address0))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(15L);
    assertThat(state.getTokensStakedBy(address1))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(15L);
    assertThat(state.getTokensStakedBy(address2))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(15L);
    context =
        new SysContractContextTest(
            Duration.ofDays(14).toMillis() + context.getBlockTime(),
            context.getBlockTime(),
            context.getFrom());
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(15);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(empty);
            });
    // Assert tokens locked in first epoch is free now
    assertThat(state.getTokensStakedBy(address0))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(10L);
    assertThat(state.getTokensStakedBy(address1))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(10L);
    assertThat(state.getTokensStakedBy(address2))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(10L);

    from(withdrawalContract);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requiredStake);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(empty);
            });
    // Starting a new epoch for the withdraw contract does not reset tokens for the deposit contract
    assertThat(state.getTokensStakedBy(address0))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(0L);
    assertThat(state.getTokensStakedBy(address1))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(0L);
    assertThat(state.getTokensStakedBy(address2))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(0L);
  }

  @Test
  public void addSignature() {
    from(address0);
    Hash randomHash = Hash.create(s -> s.writeInt(987));
    Hash message = Hash.create(s -> s.writeInt(123));
    state = state.addMessageForSigning(message, randomHash);
    SigningRequest signingRequest = new SigningRequest(0, message, randomHash);
    assertThat(state.getPendingMessages()).containsExactly(signingRequest);
    Signature sign = oraclePrivateKey.sign(message);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.ADD_SIGNATURE);
              message.write(rpc);
              rpc.writeInt(0); // nonce
              sign.write(rpc);
            });
    assertThat(state.getSignature(0, message, randomHash)).isEqualTo(sign);
    assertThat(state.getPendingMessages()).isEmpty();
    assertThat(state.getCurrentMessageNonce()).isEqualTo(1);
  }

  @Test
  public void addSignatureInvalidSigner() {
    from(withdrawalContract);
    Hash message = Hash.create(s -> s.writeInt(123));
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.ADD_SIGNATURE);
                      message.write(rpc);
                      rpc.writeInt(0); // nonce
                      oraclePrivateKey.sign(message).write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only oracle members can add signatures");
  }

  @Test
  public void addSignatureInvalidMessage() {
    from(address2);
    Hash randomHash = Hash.create(s -> s.writeInt(987));
    Hash real = Hash.create(s -> s.writeInt(123));
    Hash fake = Hash.create(s -> s.writeInt(321));
    // doesn't matter that the signature is valid.
    Signature sign = oraclePrivateKey.sign(fake);
    state = state.addMessageForSigning(real, randomHash);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.ADD_SIGNATURE);
                      fake.write(rpc);
                      rpc.writeInt(0);
                      sign.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unrecognized message");
  }

  @Test
  void addSignatureOnMessageOutOfOrder() {
    from(address2);
    Hash message0 = Hash.create(s -> s.writeInt(123));
    Hash message1 = Hash.create(s -> s.writeInt(321));
    Hash randomHash0 = Hash.create(s -> s.writeInt(9837));
    Hash randomHash1 = Hash.create(s -> s.writeInt(9887));
    state = state.addMessageForSigning(message0, randomHash0);
    state = state.addMessageForSigning(message1, randomHash1);
    Signature signature = oraclePrivateKey.sign(message1);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.ADD_SIGNATURE);
                      message1.write(rpc);
                      rpc.writeInt(1);
                      signature.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unrecognized message");
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.ADD_SIGNATURE);
              message0.write(rpc);
              rpc.writeInt(0);
              oraclePrivateKey.sign(message0).write(rpc);
            });
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.ADD_SIGNATURE);
              message1.write(rpc);
              rpc.writeInt(1);
              signature.write(rpc);
            });
    assertThat(state.getPendingMessages()).isEmpty();
    assertThat(state.getSignedMessages()).hasSize(2);
  }

  @Test
  public void invalidSigner() {
    from(address0);
    Hash randomHash = Hash.create(s -> s.writeInt(9847));
    Hash message = Hash.create(s -> s.writeInt(123));
    KeyPair keyPair = new KeyPair(BigInteger.valueOf(123));
    Signature sign = keyPair.sign(message);
    state = state.addMessageForSigning(message, randomHash);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.ADD_SIGNATURE);
                      message.write(rpc);
                      rpc.writeInt(0);
                      sign.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Invalid signer");
  }

  @Test
  public void associateTokensToContract() {
    long amount = 250_000L;
    from(address0);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.ASSOCIATE_TOKENS_TO_CONTRACT);
              rpc.writeLong(amount);
            });

    assertThat(state.getTokensStakedBy(address0))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(0L);

    byte[] actualRpc = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] expectedRpc = AccountPluginRpc.associateTokens(amount, context.getContractAddress());
    assertThat(actualRpc).isEqualTo(expectedRpc);
    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            LargeOracleContract.Callbacks.associateCallback(address0, amount));
    assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  public void associatedTokensMustBePositiveAmount() {
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.ASSOCIATE_TOKENS_TO_CONTRACT);
                      rpc.writeLong(0);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Associated stakes must be positive");
  }

  @Nested
  @DisplayName("Setting expiration timestamp for association.")
  final class SetExpirationForAssociation {
    @Test
    @DisplayName("To a negative value, throws exception.")
    public void setNegativeExpirationTimestamp() {
      long initialExpirationTimestamp = 100L;
      state = state.stakeTokens(context.getFrom(), 100, initialExpirationTimestamp);
      assertThatThrownBy(() -> setExpiration(-1L))
          .isInstanceOf(RuntimeException.class)
          .hasMessage("Expiration timestamp must be non-negative");
    }

    @Test
    @DisplayName("To expire, succeeds.")
    public void setAssociationToExpireExpire() {
      long expirationTimestamp = 100L;
      long amount = 200L;
      state = state.stakeTokens(address0, amount, 400L);
      state =
          serialization.callback(
              context,
              getCallbackContext(true),
              state,
              LargeOracleContract.Callbacks.setExpirationForAssociation(
                  address0, expirationTimestamp));

      assertThat(state.getTokensStakedBy(address0))
          .extracting(StakedTokens::getExpirationTimestamp)
          .isEqualTo(expirationTimestamp);
    }

    @Test
    @DisplayName("To never expire, creates expected rpcs.")
    public void setExpirationToNeverExpireForAssociation() {
      Long expirationTimestamp = null;
      from(address0);
      state = setExpiration(expirationTimestamp);

      assertThat(state.getTokensStakedBy(address0))
          .extracting(StakedTokens::getExpirationTimestamp)
          .isNull();

      byte[] actualRpc = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
      byte[] expectedRpc =
          AccountPluginRpc.setExpirationForAssociation(
              context.getContractAddress(), expirationTimestamp);
      assertThat(actualRpc).isEqualTo(expectedRpc);

      byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
      byte[] expectedCallbackRpc =
          SafeDataOutputStream.serialize(
              LargeOracleContract.Callbacks.setExpirationForAssociation(
                  address0, expirationTimestamp));
      assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
    }

    @Test
    @DisplayName("To zero, creates expected rpcs.")
    public void setExpirationForAssociationToZero() {
      long initialExpirationTimestamp = 100L;
      state = state.stakeTokens(address0, 100, initialExpirationTimestamp);

      long expirationTimestamp = 0L;
      from(address0);
      state = setExpiration(expirationTimestamp);

      assertThat(state.getTokensStakedBy(address0))
          .extracting(StakedTokens::getExpirationTimestamp)
          .isEqualTo(initialExpirationTimestamp);

      byte[] actualRpc = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
      byte[] expectedRpc =
          AccountPluginRpc.setExpirationForAssociation(
              context.getContractAddress(), expirationTimestamp);
      assertThat(actualRpc).isEqualTo(expectedRpc);

      byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
      byte[] expectedCallbackRpc =
          SafeDataOutputStream.serialize(
              LargeOracleContract.Callbacks.setExpirationForAssociation(
                  address0, expirationTimestamp));
      assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
    }

    @Test
    @DisplayName("To a positive value, but with a failing callback, throws exception.")
    public void associateTokensWithExpirationCallbackFailsIfNotSuccessful() {
      long expirationTimestamp = 100L;
      assertThatThrownBy(
              () ->
                  serialization.callback(
                      context,
                      getCallbackContext(false),
                      state,
                      LargeOracleContract.Callbacks.setExpirationForAssociation(
                          address0, expirationTimestamp)))
          .isInstanceOf(RuntimeException.class)
          .hasMessage(
              String.format("Failed to set expiration to %d for association", expirationTimestamp));
    }
  }

  @Nested
  @DisplayName("Associating tokens to contract with an expiration timestamp.")
  final class AssociateTokensWithTimestamp {
    @Test
    @DisplayName("In the future, creates expected rpcs.")
    public void associateTokensToContractWithExpiration() {
      long amount = 250_000L;
      long expirationTimestamp = 100L;

      setBlockProductionTime(expirationTimestamp - 1);
      from(address0);
      state = associateWithExpiration(amount, expirationTimestamp);

      assertThat(state.getTokensStakedBy(address0))
          .extracting(StakedTokens::getAllocatableTokens)
          .isEqualTo(0L);

      byte[] actualRpc = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
      byte[] expectedRpc =
          AccountPluginRpc.associateTokensWithExpiration(
              amount, context.getContractAddress(), expirationTimestamp);
      assertThat(actualRpc).isEqualTo(expectedRpc);

      byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
      byte[] expectedCallbackRpc =
          SafeDataOutputStream.serialize(
              LargeOracleContract.Callbacks.associateWithExpirationCallback(
                  address0, amount, expirationTimestamp));
      assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
    }

    @Test
    @DisplayName("In the future, but with a non-positive amount of tokens, throws exception.")
    public void associatedTokensWithExpirationMustBePositiveAmount() {
      assertThatThrownBy(() -> associateWithExpiration(0, 1L))
          .isInstanceOf(RuntimeException.class)
          .hasMessage("Associated stakes must be positive");
    }

    @Test
    @DisplayName("In the past, throws exception.")
    public void associatedTokensWithExpirationInThePastFails() {
      long expirationTimestamp = 100L;
      setBlockProductionTime(expirationTimestamp + 1);
      assertThatThrownBy(() -> associateWithExpiration(100, expirationTimestamp))
          .isInstanceOf(RuntimeException.class)
          .hasMessage("Expiration timestamp must be set in the future");
    }

    @Test
    @DisplayName("At block production time, throws exception.")
    public void associatedTokensWithExpirationAsBlockProductionTimeFails() {
      long expirationTimestamp = 100L;
      setBlockProductionTime(expirationTimestamp);
      assertThatThrownBy(() -> associateWithExpiration(100, expirationTimestamp))
          .isInstanceOf(RuntimeException.class)
          .hasMessage("Expiration timestamp must be set in the future");
    }

    @Test
    @DisplayName("Of positive value, but with a failing callback, throws exception.")
    public void associateTokensWithExpirationCallbackFailsIfNotSuccessful() {
      long amount = 250_000L;
      long expirationTimestamp = 100L;
      assertThatThrownBy(
              () ->
                  serialization.callback(
                      context,
                      getCallbackContext(false),
                      state,
                      LargeOracleContract.Callbacks.associateWithExpirationCallback(
                          address0, amount, expirationTimestamp)))
          .isInstanceOf(RuntimeException.class)
          .hasMessage(
              String.format(
                  "Failed to associate %d tokens that expires at %d for %s",
                  amount, expirationTimestamp, address0));
    }

    @Test
    @DisplayName("Of positive value and successful callback, associates tokens at timestamp.")
    public void associateTokensWithExpirationCallback() {
      long amount = 250_000L;
      long expirationTimestamp = 100L;
      state =
          serialization.callback(
              context,
              getCallbackContext(true),
              state,
              LargeOracleContract.Callbacks.associateWithExpirationCallback(
                  address0, amount, expirationTimestamp));

      assertThat(state.getTokensStakedBy(address0))
          .extracting(StakedTokens::getAllocatableTokens)
          .isEqualTo(amount);
      assertThat(state.getTokensStakedBy(address0))
          .extracting(StakedTokens::getExpirationTimestamp)
          .isEqualTo(expirationTimestamp);
    }

    @Test
    @DisplayName("Can be disassociated.")
    public void disassociateTokensWithExpirationCallback() {
      long amount = 250_000L;
      long expirationTimestamp = 100L;
      state =
          serialization.callback(
              context,
              getCallbackContext(true),
              state,
              LargeOracleContract.Callbacks.disassociateWithExpirationCallback(
                  address0, amount, expirationTimestamp));

      assertThat(state.getTokensStakedBy(address0))
          .extracting(StakedTokens::getAllocatableTokens)
          .isEqualTo(0L);
    }
  }

  private LargeOracleContractState associateWithExpiration(long amount, long expirationTimestamp) {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(
              LargeOracleContract.Invocations.ASSOCIATE_TOKENS_TO_CONTRACT_WITH_EXPIRATION);
          rpc.writeLong(amount);
          rpc.writeLong(expirationTimestamp);
        });
  }

  private LargeOracleContractState setExpiration(Long expirationTimestamp) {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(LargeOracleContract.Invocations.SET_EXPIRATION_FOR_ASSOCIATION);
          rpc.writeOptional((value, stream) -> stream.writeLong(value), expirationTimestamp);
        });
  }

  @Nested
  @DisplayName("Disassociating expired tokens for an account.")
  final class DisassociatedExpiredToken {
    @Test
    @DisplayName("Creates expected rpcs.")
    public void disassociateExpiredTokenCreatesExpectedRpc() {
      long amount = 250_000L;
      long expirationTimestamp = 100L;
      state = state.stakeTokens(address0, amount, expirationTimestamp);

      setBlockProductionTime(expirationTimestamp + 1L);
      from(address1);
      state =
          serialization.invoke(
              context,
              state,
              rpc -> {
                rpc.writeByte(
                    LargeOracleContract.Invocations.DISASSOCIATE_EXPIRED_STAKES_FROM_CONTRACT);
                address0.write(rpc);
              });

      byte[] actualRpc = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
      byte[] expectedRpc =
          AccountPluginRpc.disassociateTokens(amount, context.getContractAddress());
      assertThat(actualRpc).isEqualTo(expectedRpc);

      byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
      byte[] expectedCallbackRpc =
          SafeDataOutputStream.serialize(
              LargeOracleContract.Callbacks.disassociateWithExpirationCallback(
                  address0, amount, expirationTimestamp));
      assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
    }

    @Test
    @DisplayName("When no tokens to disassociates, throws exception.")
    public void noFreeTokensAvailableToDisassociate() {
      long amount = 250_000L;
      long expirationTimestamp = 100L;
      state = state.stakeTokens(address0, amount, expirationTimestamp);

      setBlockProductionTime(expirationTimestamp + 1L);
      from(address1);
      state =
          serialization.invoke(
              context,
              state,
              rpc -> {
                rpc.writeByte(
                    LargeOracleContract.Invocations.DISASSOCIATE_EXPIRED_STAKES_FROM_CONTRACT);
                address0.write(rpc);
              });

      assertThat(state.getTokensStakedBy(address0).getFreeTokens()).isEqualTo(0);
      assertThatThrownBy(
              () ->
                  serialization.invoke(
                      context,
                      state,
                      rpc -> {
                        rpc.writeByte(
                            LargeOracleContract.Invocations
                                .DISASSOCIATE_EXPIRED_STAKES_FROM_CONTRACT);
                        address0.write(rpc);
                      }))
          .isInstanceOf(RuntimeException.class)
          .hasMessage("No tokens available to disassociate for account");
    }

    @Test
    @DisplayName("That has not expired, does nothing.")
    public void disassociateNonExpiredTokenCreatesExpectedRpc() {
      long amount = 250_000L;
      long expirationTimestamp = 100L;
      state = state.stakeTokens(address0, amount, expirationTimestamp);

      setBlockProductionTime(expirationTimestamp - 1);
      from(address1);
      state =
          serialization.invoke(
              context,
              state,
              rpc -> {
                rpc.writeByte(
                    LargeOracleContract.Invocations.DISASSOCIATE_EXPIRED_STAKES_FROM_CONTRACT);
                address0.write(rpc);
              });

      assertThat(context.getUpdateLocalAccountPluginStates().size()).isEqualTo(0);
    }

    @Test
    @DisplayName("Only deallocates expired tokens.")
    void disassociateAllExpiredTokensFromContract() {
      long amount = 250_000L;

      long expiration = 100L;
      state = state.stakeTokens(address0, amount, expiration);
      assertThat(state.getTokensStakedBy(address0))
          .extracting(StakedTokens::getExpirationTimestamp)
          .isEqualTo(expiration);
      assertThat(state.getTokensStakedBy(address0))
          .extracting(StakedTokens::getAllocatableTokens)
          .isEqualTo(amount);

      from(address2);
      setBlockProductionTime(expiration + 1);
      state =
          serialization.invoke(
              context,
              state,
              rpc -> {
                rpc.writeByte(
                    LargeOracleContract.Invocations.DISASSOCIATE_EXPIRED_STAKES_FROM_CONTRACT);
                address0.write(rpc);
              });

      assertThat(state.getTokensStakedBy(address0))
          .extracting(StakedTokens::getAllocatableTokens)
          .isEqualTo(0L);
    }
  }

  @Test
  @DisplayName(
      "When a disassociation callback with an expiration timestamp fails, both amount and"
          + " expirations are added back to state.")
  public void disassociateTokensWithExpirationCallbackStakesTokensAgainIfNotSuccessful() {
    long amount = 250_000L;
    long expirationTimestamp = 250L;
    state =
        serialization.callback(
            context,
            getCallbackContext(false),
            state,
            LargeOracleContract.Callbacks.disassociateWithExpirationCallback(
                address0, amount, expirationTimestamp));

    assertThat(state.getTokensStakedBy(address0))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(amount);
    assertThat(state.getTokensStakedBy(address0))
        .extracting(StakedTokens::getExpirationTimestamp)
        .isEqualTo(expirationTimestamp);
  }

  @Test
  public void disassociateFreeTokens() {
    long amount = 250_000L;
    state = state.stakeTokens(address0, amount, null);
    from(address0);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.DISASSOCIATE_TOKENS_FROM_CONTRACT);
              rpc.writeLong(amount);
            });

    assertThat(state.getTokensStakedBy(address0))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(0L);

    byte[] actualRpc = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] expectedRpc = AccountPluginRpc.disassociateTokens(amount, context.getContractAddress());
    assertThat(actualRpc).isEqualTo(expectedRpc);
    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            LargeOracleContract.Callbacks.disassociateCallback(address0, amount));
    assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  @DisplayName("Disassociating tokens that has an expiration, creates expected RPC.")
  public void disassociateTokensWithExpirationFromContractCreatesExpectedRpc() {
    long associationAmount = 250_000L;
    long expirationTimestamp = 100L;
    state = state.stakeTokens(address0, associationAmount, expirationTimestamp);
    from(address0);
    long disAssociationAmount = associationAmount - 100_000L;
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.DISASSOCIATE_TOKENS_FROM_CONTRACT);
              rpc.writeLong(disAssociationAmount);
            });

    byte[] actualRpc = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] expectedRpc =
        AccountPluginRpc.disassociateTokens(disAssociationAmount, context.getContractAddress());
    assertThat(actualRpc).isEqualTo(expectedRpc);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            LargeOracleContract.Callbacks.disassociateWithExpirationCallback(
                address0, disAssociationAmount, expirationTimestamp));
    assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  /**
   * Request a new small oracle where three nodes have staked enough even though one node has
   * reserved some tokens.
   */
  @Test
  public void requestNewSmallOracleWithEnoughTokensAvailableForAllocation() {
    long stakeAmount = 30L;
    state =
        state
            .stakeTokens(address0, stakeAmount, null)
            .stakeTokens(address1, stakeAmount, null)
            .stakeTokens(address2, stakeAmount, null);
    StakedTokens afterStake = state.getTokensStakedBy(address0);
    assertThat(afterStake.getAllocatableTokens()).isEqualTo(stakeAmount);

    long reserveAmount = 10L;
    from(address0);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.SET_RESERVED_TOKENS);
              rpc.writeLong(reserveAmount);
            });
    StakedTokens afterReserve = state.getTokensStakedBy(address0);
    assertThat(afterReserve.getAllocatableTokens()).isEqualTo(stakeAmount - reserveAmount);

    from(withdrawalContract);
    long requiredStake = stakeAmount - reserveAmount;
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requiredStake);
              rpc.writeByte(42); // callback
              rpc.writeDynamicBytes(Hash.create(s -> s.writeInt(0)).getBytes()); // additionalData
            });
    assertThat(context.getInteractions()).hasSize(1);
    assertThat(state.getPendingUpdateRequest()).isNull();

    assertThat(state.getTokensStakedBy(address0).getLockedToOracle(withdrawalContract))
        .isEqualTo(requiredStake);
    assertThat(state.getTokensStakedBy(address0).getAllocatableTokens()).isZero();

    for (BlockchainAddress oracleNode : List.of(address1, address2)) {
      StakedTokens stakedTokens = state.getTokensStakedBy(oracleNode);
      assertThat(stakedTokens.getLockedToOracle(withdrawalContract)).isEqualTo(requiredStake);
      assertThat(stakedTokens.getAllocatableTokens()).isEqualTo(stakeAmount - requiredStake);
    }
  }

  /**
   * Request a new small oracle where three nodes have staked tokens, but one of the nodes has
   * reserved enough to not be selected.
   */
  @Test
  void requestNewSmallOracleWithInsufficientTokensAvailableForAllocation() {
    long stakeAmount = 100;
    state =
        state
            .stakeTokens(address0, stakeAmount, null)
            .stakeTokens(address1, stakeAmount, null)
            .stakeTokens(address2, stakeAmount, null);

    from(address0);
    long reserveAmount = 50;
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.SET_RESERVED_TOKENS);
              rpc.writeLong(reserveAmount);
            });

    from(withdrawalContract);
    long requiredStake = stakeAmount - reserveAmount + 1;
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requiredStake);
              rpc.writeByte(42); // callback
              rpc.writeDynamicBytes(Hash.create(s -> s.writeInt(0)).getBytes()); // additionalData
            });

    assertThat(context.getInteractions()).hasSize(0);
    assertThat(state.getPendingUpdateRequest()).isNotNull();
    assertThat(state.getTokensStakedBy(address0).getAllocatableTokens())
        .isEqualTo(stakeAmount - reserveAmount);
    for (BlockchainAddress nodeAddress : List.of(address1, address2)) {
      assertThat(state.getTokensStakedBy(nodeAddress).getAllocatableTokens())
          .isEqualTo(stakeAmount);
    }
  }

  @Test
  public void setReservedTokensCannotBeNegative() {
    long amount = 50L;
    state = state.stakeTokens(address0, amount, null);
    from(address0);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.SET_RESERVED_TOKENS);
                      rpc.writeLong(-1L);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount of reserved tokens has to be non-negative: -1");
  }

  @Test
  public void setReservedTokensHigherThanFreeTokens() {
    long free = 50L;
    long reserved = 100L;
    state = state.stakeTokens(address0, free, null);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.SET_RESERVED_TOKENS);
              rpc.writeLong(reserved);
            });
    StakedTokens stakedBy = state.getTokensStakedBy(address0);
    assertThat(stakedBy.getAllocatableTokens()).isZero();
  }

  @Test
  public void setReservedTokensToZero() {
    long amount = 50L;
    state = state.stakeTokens(address0, amount, null);
    from(address0);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.SET_RESERVED_TOKENS);
              rpc.writeLong(0L);
            });
    assertThat(state.getTokensStakedBy(address0).getAllocatableTokens()).isEqualTo(amount);
  }

  @Test
  public void disassociateDecreasesBothCurrentAndReservedTokens() {
    long amount = 200L;
    state = state.stakeTokens(address0, amount, null);
    from(address0);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.SET_RESERVED_TOKENS);
              rpc.writeLong(amount);
            });

    StakedTokens before = state.getTokensStakedBy(address0);
    assertThat(before.getAllocatableTokens()).isZero();

    long disassociateAmount = amount / 2;
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.DISASSOCIATE_TOKENS_FROM_CONTRACT);
              rpc.writeLong(disassociateAmount);
            });

    // Ensure that disassociating tokens decreases both reserved and free tokens
    StakedTokens afterDisassociate = state.getTokensStakedBy(address0);
    assertThat(afterDisassociate.getAllocatableTokens()).isZero();
  }

  /** Disassociate some but not all of what is reserved should decrease reserved tokens. */
  @Test
  void disassociateSomeOfReserved() {
    final long stakeAmount = 3;
    final long reserveAmount = 2;
    final long disassociateAmount = 1;
    state =
        state.stakeTokens(address0, stakeAmount, null).setReservedTokens(address0, reserveAmount);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.DISASSOCIATE_TOKENS_FROM_CONTRACT);
              rpc.writeLong(disassociateAmount);
            });
    assertThat(state.getTokensStakedBy(address0).getAllocatableTokens()).isEqualTo(1);
    assertThat(state.getTokensStakedBy(address0).getFreeTokens()).isEqualTo(2);
  }

  @Test
  public void disassociatedTokensMustBePositiveAmount() {
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(
                          LargeOracleContract.Invocations.DISASSOCIATE_TOKENS_FROM_CONTRACT);
                      rpc.writeLong(0);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Cannot disassociate a negative amount");
  }

  @Test
  public void resultOfDisassociatingTokensCannotBeNegative() {
    final long stakeAmount = 100L;
    final long disassociateAmount = 101L;
    state = state.stakeTokens(address0, stakeAmount, null);
    from(address0);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(
                          LargeOracleContract.Invocations.DISASSOCIATE_TOKENS_FROM_CONTRACT);
                      rpc.writeLong(disassociateAmount);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Cannot disassociate %d tokens when only %d tokens are available to disassociate"
                .formatted(disassociateAmount, stakeAmount));
  }

  @Test
  public void associateTokensCallback() {
    long amount = 250_000L;
    state =
        serialization.callback(
            context,
            getCallbackContext(true),
            state,
            LargeOracleContract.Callbacks.associateCallback(address0, amount));
    assertThat(state.getTokensStakedBy(address0))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(amount);
  }

  @Test
  public void associateTokensCallbackFailsIfNotSuccessful() {
    long amount = 250_000L;
    assertThatThrownBy(
            () ->
                serialization.callback(
                    context,
                    getCallbackContext(false),
                    state,
                    LargeOracleContract.Callbacks.associateCallback(address0, amount)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(String.format("Failed to associate %d tokens for %s", amount, address0));
  }

  @Test
  public void disassociateTokensCallback() {
    long amount = 250_000L;
    state =
        serialization.callback(
            context,
            getCallbackContext(true),
            state,
            LargeOracleContract.Callbacks.disassociateCallback(address0, amount));
    assertThat(state.getTokensStakedBy(address0))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(0L);
  }

  @Test
  public void disassociateTokensCallbackStakesTokensAgainIfNotSuccessful() {
    long amount = 250_000L;
    state =
        serialization.callback(
            context,
            getCallbackContext(false),
            state,
            LargeOracleContract.Callbacks.disassociateCallback(address0, amount));
    assertThat(state.getTokensStakedBy(address0))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(amount);
  }

  @Test
  public void onUpgradeDispute() {
    from(address0);
    state = state.stakeTokens(challenger, disputeTokenCost, null);
    from(withdrawalContract);
    state = invokeCreateDisputePoll();

    from(depositContract);
    byte[] empty = new byte[0];
    int requiredStake = 10;
    int callback = 42;

    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requiredStake);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(empty);
            });
    LargeOracleContract contract = new LargeOracleContract();
    StateAccessor stateAccessor = StateAccessor.create(state);
    LargeOracleContractState updated = contract.upgrade(stateAccessor);
    StateSerializableEquality.assertStatesEqual(updated, state);
  }

  @Test
  public void onUpgradeSignature() {
    from(address1);
    Hash randomHash = Hash.create(s -> s.writeInt(987));
    Hash message = Hash.create(s -> s.writeInt(1243));
    state = state.addMessageForSigning(message, randomHash);
    SigningRequest hashAndNonce = new SigningRequest(0, message, randomHash);
    assertThat(state.getPendingMessages()).containsExactly(hashAndNonce);
    Signature sign = oraclePrivateKey.sign(message);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.ADD_SIGNATURE);
              message.write(rpc);
              rpc.writeInt(0); // nonce
              sign.write(rpc);
            });

    from(withdrawalContract);
    String byocTwinSymbol = "coin";

    state = state.addByocTwinToMpcDeal(byocTwinSymbol, Unsigned256.create(10), 5);

    StateAccessor stateAccessor = StateAccessor.create(state);
    LargeOracleContract contract = new LargeOracleContract();
    LargeOracleContractState updated = contract.upgrade(stateAccessor);

    assertThat(updated.getSignature(0, message, randomHash)).isEqualTo(sign);
    assertThat(updated.getPendingMessages()).isEmpty();
    assertThat(updated.getCurrentMessageNonce()).isEqualTo(1);
  }

  @Test
  public void onUpgradeByocTwinsTrade() {
    from(withdrawalContract);
    String byocTwinSymbol = "coin";

    state = state.addByocTwinToMpcDeal(byocTwinSymbol, Unsigned256.create(10), 5);
    state = state.stakeTokens(address0, 10, null);
    state = state.setMpcToUsdExchangeRate(new Fraction(10, 5));
    long convertedTokens = state.convertToMpcTokens(new Fraction(10, 10));
    StateAccessor stateAccessor = StateAccessor.create(state);
    LargeOracleContract contract = new LargeOracleContract();
    LargeOracleContractState updated = contract.upgrade(stateAccessor);

    long updatedConverted = updated.convertToMpcTokens(new Fraction(10, 10));
    assertThat(convertedTokens).isEqualTo(updatedConverted);

    assertThat(updated.getByocTwinToMpcTradeDeal(byocTwinSymbol).getByocTwins())
        .isEqualTo(Unsigned256.TEN);

    assertThat(updated.getByocTwinToMpcTradeDeal(byocTwinSymbol).getMpcTokens()).isEqualTo(5);
    assertThat(updated.getTokensStakedBy(address0).getAllocatableTokens()).isEqualTo(10);
  }

  @Test
  public void lockTokensToPriceOracle() {

    long amount = 5_000_0000L;
    from(priceOracleContract);
    state = state.stakeTokens(address1, amount, null);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.LOCK_TOKENS_TO_PRICE_ORACLE);
              rpc.writeLong(amount);
              address1.write(rpc);
            });

    assertThat(state.getTokensStakedBy(address1))
        .extracting(StakedTokens::getAllocatableTokens)
        .isEqualTo(0L);
  }

  @Test
  public void onlySmallOracleContractsCanCreateDispute() {
    long amount = 5_000_0000L;
    from(address0);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(LargeOracleContract.Invocations.LOCK_TOKENS_TO_PRICE_ORACLE);
                      rpc.writeLong(amount);
                      address1.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the price oracle contract can lock tokens.");
  }

  @Test
  public void invoke_unlockTokensFromPriceOracle() {
    state = state.stakeTokens(address0, 100, null);
    from(priceOracleContract);
    LargeOracleContractState lockedState =
        state.lockTokensToOracle(
            address0, priceOracleContract, 15, context.getBlockProductionTime());
    LargeOracleContractState unlockState =
        serialization.invoke(
            context,
            lockedState,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.UNLOCK_TOKENS_FROM_PRICE_ORACLE);
              rpc.writeLong(10L);
              address0.write(rpc);
            });
    assertThat(unlockState.getTokensStakedBy(address0).getAllocatableTokens()).isEqualTo(95);
    assertThat(unlockState.getTokensStakedBy(address0).getLockedToOracle(priceOracleContract))
        .isEqualTo(5);
  }

  @Test
  public void invoke_unlockTokensFromPriceOracleFullLockedAmount() {
    state = state.stakeTokens(address0, 100, null);
    from(priceOracleContract);
    LargeOracleContractState lockedState =
        state.lockTokensToOracle(
            address0, priceOracleContract, 15, context.getBlockProductionTime());
    LargeOracleContractState unlockState =
        serialization.invoke(
            context,
            lockedState,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.UNLOCK_TOKENS_FROM_PRICE_ORACLE);
              rpc.writeLong(15L);
              address0.write(rpc);
            });
    assertThat(unlockState.getTokensStakedBy(address0).getAllocatableTokens()).isEqualTo(100);
    assertThat(unlockState.getTokensStakedBy(address0).getLockedToOracle(priceOracleContract))
        .isEqualTo(0);
  }

  @Test
  public void invoke_unlockTokensFromPriceOracleNotAllowedContract() {
    from(address0);
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> {
                      rpc.writeByte(
                          LargeOracleContract.Invocations.UNLOCK_TOKENS_FROM_PRICE_ORACLE);
                      rpc.writeLong(10000);
                      address1.write(rpc);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the price oracle contract can unlock tokens.");
  }

  @Test
  public void invoke_unlockTokensTooLargeAmount() {
    state = state.stakeTokens(address0, 100, null);
    LargeOracleContractState lockedState =
        state.lockTokensToOracle(
            address0, priceOracleContract, 15, context.getBlockProductionTime());
    assertThatThrownBy(
            () -> lockedState.unlockTokensFromOracle(address0, priceOracleContract, 1000))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Not enough locked tokens to unlock requested amount");
  }

  @Test
  void randomizeListActuallyRandomizesList() {
    assertThat(state.getOracleMembers())
        .isNotEqualTo(LargeOracleContractState.randomizeList(state.getOracleMembers(), 42));
  }

  @Test
  void hashCodeOfArraysIsStable() {
    // Ensure that Arrays.hashCode() does not change when updating dependencies
    assertThat(Arrays.hashCode(hash.getBytes())).isEqualTo(hashCodeOfHash);
  }

  @Test
  void invoke_unlockOldPendingTokens() {
    long fourteenDaysAndOneMillisecond = Duration.ofDays(14).toMillis() + 1;
    long currentTime = context.getBlockProductionTime();
    long oldTime = currentTime - fourteenDaysAndOneMillisecond;
    BlockchainAddress oracle = oracleAddresses.get(0);
    state =
        state
            .stakeTokens(address0, 42, null)
            .lockTokensToOracle(address0, oracle, 42, context.getBlockProductionTime())
            .releaseTokensFromOracle(address0, oracle, oldTime);
    state =
        state
            .stakeTokens(address0, 10, null)
            .lockTokensToOracle(address0, oracle, 10, context.getBlockProductionTime())
            .releaseTokensFromOracle(address0, oracle, currentTime);
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.UNLOCK_OLD_PENDING_TOKENS);
              oracle.write(rpc);
            });
    // Second release is not old enough to be unlocked
    assertThat(state.getTokensStakedBy(address0).getPendingForOracle(oracle)).isEqualTo(10);
    // Check that invocation works for oracles with no pending tokens
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.UNLOCK_OLD_PENDING_TOKENS);
              oracleAddresses.get(1).write(rpc);
            });
  }

  @Test
  void unlockOldPendingTokensSelectsNewOracle() {
    from(withdrawalContract);
    int requireStake = 10;
    int callback = 100;
    // Set tokens to be pending release
    state =
        state
            .stakeTokens(address0, 10, null)
            .stakeTokens(address1, 10, null)
            .stakeTokens(address2, 10, null);
    state =
        state
            .lockTokensToOracle(address0, withdrawalContract, 10, context.getBlockProductionTime())
            .lockTokensToOracle(address1, withdrawalContract, 10, context.getBlockProductionTime())
            .lockTokensToOracle(address2, withdrawalContract, 10, context.getBlockProductionTime());
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.REQUEST_NEW_SMALL_ORACLE);
              rpc.writeLong(requireStake);
              rpc.writeByte(callback);
              rpc.writeDynamicBytes(new byte[0]);
            });
    // No oracle members currently have enough free tokens to change small oracle
    assertThat(state.getPendingUpdateRequest()).isNotNull();

    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.UNLOCK_OLD_PENDING_TOKENS);
              withdrawalContract.write(rpc);
            });
    // No pending tokens were old enough to be unlocked, so no new small oracle could be chosen
    assertThat(state.getPendingUpdateRequest()).isNotNull();

    // Skip enough time to unlock old pending tokens and check that oracle update request was
    // handled
    setBlockProductionTime(Duration.ofDays(15).toMillis());
    state =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.UNLOCK_OLD_PENDING_TOKENS);
              withdrawalContract.write(rpc);
            });
    assertThat(state.getPendingUpdateRequest()).isNull();
  }

  @Test
  void invoke_checkSignatureCount() {
    for (int i = 0; i < LargeOracleContract.NUMBER_OF_SIGNATURES_PER_COMMITTEE - 1; i++) {
      int valueToHash = i + 180;
      Hash message = Hash.create(s -> s.writeInt(valueToHash));
      state = state.addMessageForSigning(message, message);
    }
    assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    state,
                    rpc -> rpc.writeByte(LargeOracleContract.Invocations.CHECK_SIGNATURE_COUNT)))
        .hasMessage("Current committee has not run out of signatures");

    state = state.addMessageForSigning(hash, hash);

    LargeOracleContractState updatedState =
        serialization.invoke(
            context,
            state,
            rpc -> rpc.writeByte(LargeOracleContract.Invocations.CHECK_SIGNATURE_COUNT));
    assertThat(updatedState).isNotNull();

    ContractEventInteraction contractEventInteraction =
        (ContractEventInteraction) context.getInteractions().get(0);
    assertThat(contractEventInteraction.contract).isEqualTo(bpOrchestrationContract);

    byte[] actualRpc = SafeDataOutputStream.serialize(contractEventInteraction.rpc);
    byte[] expectedRpc = SafeDataOutputStream.serialize(BpOrchestrationRpc.triggerNewCommittee());
    assertThat(actualRpc).isEqualTo(expectedRpc);
  }

  /**
   * CHECK_ORACLE_MEMBER_STATUSES responds with true if all addresses of invocation are part of
   * current large oracle.
   */
  @Test
  void invoke_checkOracleMemberStatusReportsAllOracleMembers() {
    assertThat(checkOracleMemberStatusInvocation(oracleAddresses)).isEqualTo(true);

    List<BlockchainAddress> singleAddress = List.of(address1);
    assertThat(checkOracleMemberStatusInvocation(singleAddress)).isEqualTo(true);

    List<BlockchainAddress> partOfLargeOracle = List.of(address2, address0);
    assertThat(checkOracleMemberStatusInvocation(partOfLargeOracle)).isEqualTo(true);
  }

  /**
   * CHECK_ORACLE_MEMBER_STATUSES responds with false if any of addresses are not part of large
   * oracle.
   */
  @Test
  void invoke_checkOracleMemberStatusReportsAnyNonOracleMembers() {
    BlockchainAddress nonLargeOracleAddress1 =
        BlockchainAddress.fromString("000000000000000000000000000000000012340001");
    BlockchainAddress nonLargeOracleAddress2 =
        BlockchainAddress.fromString("000000000000000000000000000000000012340002");
    BlockchainAddress nonLargeOracleAddress3 =
        BlockchainAddress.fromString("000000000000000000000000000000000012340003");

    List<BlockchainAddress> noLargeOracleAddress =
        List.of(nonLargeOracleAddress1, nonLargeOracleAddress2, nonLargeOracleAddress3);
    assertThat(checkOracleMemberStatusInvocation(noLargeOracleAddress)).isEqualTo(false);

    List<BlockchainAddress> notAllLargeOracle = List.of(address2, nonLargeOracleAddress1, address0);
    assertThat(checkOracleMemberStatusInvocation(notAllLargeOracle)).isEqualTo(false);

    List<BlockchainAddress> singleNonLargeOracle = List.of(nonLargeOracleAddress3);
    assertThat(checkOracleMemberStatusInvocation(singleNonLargeOracle)).isEqualTo(false);
  }

  /**
   * CHECK_ORACLE_MEMBER_STATUSES expects blockchain addresses and will fail if non are supplied.
   */
  @Test
  void invoke_checkOracleMemberStatusNoSuppliedAddresses() {
    assertThatThrownBy(
        () ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(LargeOracleContract.Invocations.CHECK_ORACLE_MEMBER_STATUSES);
                  BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, List.of());
                }),
        "No blockchain addresses were supplied");
  }

  /**
   * Invokes a check on large oracle member status for the given addresses. This invocation should
   * never change the state.
   *
   * @param addressesToCheck addresses to check for large oracle status
   * @return true if all addresses are large oracle members
   */
  private boolean checkOracleMemberStatusInvocation(List<BlockchainAddress> addressesToCheck) {
    LargeOracleContractState stateBefore = state;
    LargeOracleContractState invokedState =
        serialization.invoke(
            context,
            state,
            rpc -> {
              rpc.writeByte(LargeOracleContract.Invocations.CHECK_ORACLE_MEMBER_STATUSES);
              BlockchainAddress.LIST_SERIALIZER.writeDynamic(rpc, addressesToCheck);
            });
    assertThat(invokedState).usingRecursiveComparison().isEqualTo(stateBefore);
    SafeDataInputStream checkResult = SafeDataInputStream.createFromBytes(context.getResult());
    return checkResult.readBoolean();
  }

  private static String getEncodedPublicKeys(List<BlockchainPublicKey> expectedOracle) {
    return expectedOracle.stream()
        .map(BlockchainPublicKey::getEcPointBytes)
        .map(a -> Arrays.copyOfRange(a, 1, a.length))
        .map(Hex::toHexString)
        .reduce("", (s0, s1) -> s0 + s1);
  }

  private static SafeDataInputStream emptyRpc() {
    return SafeDataInputStream.createFromBytes(new byte[] {});
  }

  private CallbackContext getCallbackContext(boolean success) {
    return CallbackContext.create(
        FixedList.create(
            List.of(CallbackContext.createResult(Hash.create(address0), success, emptyRpc()))));
  }

  @SuppressWarnings("unused")
  @Immutable
  private static final class OuterCoinTest implements StateSerializable {

    private final InnerCoinTest coins = new InnerCoinTest();
  }

  @SuppressWarnings("unused")
  @Immutable
  private static final class InnerCoinTest implements StateSerializable {

    private final FixedList<CoinTest> coins =
        FixedList.create(
            List.of(
                new CoinTest("coin", new Fraction(3, 1)),
                new CoinTest("dogecoin", new Fraction(10, 1)),
                new CoinTest(ETH, new Fraction(400L, 1_000_000_000__000L))));
  }

  @SuppressWarnings({"unused", "FieldCanBeLocal"})
  @Immutable
  private static final class CoinTest implements StateSerializable {

    private final String symbol;
    private final Fraction conversionRate;

    public CoinTest(String symbol, Fraction conversionRate) {
      this.symbol = symbol;
      this.conversionRate = conversionRate;
    }
  }
}
