package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import java.math.BigInteger;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class LargeOracleContractStateTest {

  private final BlockchainPublicKey oracle0 = new KeyPair(BigInteger.valueOf(1)).getPublic();
  private final BlockchainAddress address0 =
      BlockchainAddress.fromString("000000000000000000000000000000000000000000");
  private final BlockchainPublicKey oracle1 = new KeyPair(BigInteger.valueOf(2)).getPublic();
  private final BlockchainAddress address1 =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private final BlockchainPublicKey oracle2 = new KeyPair(BigInteger.valueOf(3)).getPublic();
  private final BlockchainAddress address2 =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");

  private final List<BlockchainPublicKey> oracles = List.of(oracle0, oracle1, oracle2);
  private final List<BlockchainAddress> oracleAddresses = List.of(address0, address1, address2);
  private final KeyPair oraclePrivateKey = new KeyPair(BigInteger.TWO);
  private final BlockchainPublicKey oraclePublicKey = oraclePrivateKey.getPublic();

  private final BlockchainAddress bpOrchestrationContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000042");

  private final BlockchainAddress withdrawContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000001");
  private final BlockchainAddress governanceUpdates =
      BlockchainAddress.fromString("040000000000000000000000000000000000000002");

  private LargeOracleContractState state;

  private final BlockchainAddress account =
      BlockchainAddress.fromString("000000000000000000000000000000000000000003");
  private static final long tokens = 500L;

  /** Setup. */
  @BeforeEach
  public void setUp() {
    state =
        LargeOracleContractState.initial(
                LargeOracleContract.createOracleMembers(oracleAddresses, oracles),
                oraclePublicKey,
                bpOrchestrationContract,
                governanceUpdates)
            .stakeTokens(account, tokens, null);
  }

  @Test
  public void addMessageForSigning() {
    Hash message = Hash.create(s -> s.writeInt(123));
    Hash randomHash = Hash.create(s -> s.writeInt(987));
    state = state.addMessageForSigning(message, randomHash);
    SigningRequest hashAndNonce = new SigningRequest(0, message, randomHash);
    assertThat(state.getPendingMessages()).containsExactly(hashAndNonce);
  }

  @Test
  public void addSignature() {
    Hash message = Hash.create(s -> s.writeInt(123));
    Hash randomHash = Hash.create(s -> s.writeInt(9873));
    Signature sign = oraclePrivateKey.sign(message);
    SigningRequest hashAndNonce = new SigningRequest(0, message, randomHash);
    state = state.addMessageForSigning(message, randomHash).addSignature(hashAndNonce, sign);
    assertThat(state.getPendingMessages()).isEmpty();
    assertThat(state.getSignature(0, message, randomHash)).isEqualTo(sign);
    // subsequent additions of a signature on the same message does nothing
    LargeOracleContractState largeOracleContractState = state.addSignature(hashAndNonce, sign);
    assertThat(state).isEqualTo(largeOracleContractState);
    assertThat(state.getCurrentMessageNonce()).isEqualTo(1);
  }

  @Test
  public void getBpOrchestrationContract() {
    assertThat(state.getBpOrchestrationContract()).isEqualTo(bpOrchestrationContract);
  }

  @Test
  public void replaceLargeOracle() {
    Hash message = Hash.create(s -> s.writeInt(123));
    Hash randomHash = Hash.create(s -> s.writeInt(987));
    Signature sign = oraclePrivateKey.sign(message);
    SigningRequest hashAndNonce = new SigningRequest(0, message, randomHash);
    state = state.addMessageForSigning(message, randomHash).addSignature(hashAndNonce, sign);
    state = state.addSignature(hashAndNonce, sign);
    List<BlockchainPublicKey> newKeys =
        List.of(
            new KeyPair(BigInteger.valueOf(5)).getPublic(),
            new KeyPair(BigInteger.valueOf(6)).getPublic(),
            new KeyPair(BigInteger.valueOf(7)).getPublic());
    List<BlockchainAddress> newAddresses =
        List.of(
            BlockchainAddress.fromString("000000000000000000000000000000000000000005"),
            BlockchainAddress.fromString("000000000000000000000000000000000000000006"),
            BlockchainAddress.fromString("000000000000000000000000000000000000000007"));
    BlockchainPublicKey newPublicKey = new KeyPair(BigInteger.TEN).getPublic();
    List<OracleMember> newOracleMembers =
        LargeOracleContract.createOracleMembers(newAddresses, newKeys);
    state = state.replaceLargeOracle(newOracleMembers, newPublicKey);
    assertThat(state.getOracleMembers()).isEqualTo(newOracleMembers);
    assertThat(state.getOracleKey()).isEqualTo(newPublicKey);
    // ensure that the initial nonce after the update points to the nonce of first message to be
    // signed by the new oracle.
    assertThat(state.getInitialMessageNonce()).isEqualTo(1);
    // oracle contains all messages ever signed (also those by the old oracle)
    assertThat(state.getSignedMessages()).containsExactly(hashAndNonce);
  }

  @Test
  public void gettingOldOracle() {
    state =
        state
            .stakeTokens(address0, 30, null)
            .stakeTokens(address1, 30, null)
            .stakeTokens(address2, 10, null);
    state =
        state
            .lockTokensToOracle(address0, withdrawContract, 10, 0L)
            .lockTokensToOracle(address2, withdrawContract, 10, 0L);

    assertThat(state.getMembersOfOldOracle(withdrawContract)).containsExactly(address0, address2);
  }

  @Test
  void initialNonceWithPendingMessages() {
    Hash message = Hash.create(s -> s.writeInt(1));
    Hash randomHash = Hash.create(s -> s.writeInt(987));
    state = state.addMessageForSigning(message, randomHash);
    Hash anotherMessage = Hash.create(s -> s.writeInt(2));
    Hash anotherRandomHash = Hash.create(s -> s.writeInt(9876));

    state = state.addMessageForSigning(anotherMessage, anotherRandomHash);
    assertThat(state.getInitialMessageNonce()).isEqualTo(0);
    assertThat(state.getCurrentMessageNonce()).isEqualTo(2);
    assertThat(state.numberOfSignaturesRequestedFromCurrentCommittee()).isEqualTo(2);

    state = state.addSignature(new SigningRequest(0, message, randomHash), null);
    assertThat(state.getInitialMessageNonce()).isEqualTo(0);
    assertThat(state.getCurrentMessageNonce()).isEqualTo(2);
    assertThat(state.numberOfSignaturesRequestedFromCurrentCommittee()).isEqualTo(2);

    state = state.replaceLargeOracle(List.of(), oraclePublicKey);
    assertThat(state.getInitialMessageNonce()).isEqualTo(1);
    assertThat(state.getCurrentMessageNonce()).isEqualTo(2);
    assertThat(state.numberOfSignaturesRequestedFromCurrentCommittee()).isEqualTo(1);
  }
}
