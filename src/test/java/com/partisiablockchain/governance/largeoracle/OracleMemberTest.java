package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import java.math.BigInteger;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/** Test. */
public final class OracleMemberTest {

  @Test
  public void testEquals() {
    BlockchainPublicKey key0 = new KeyPair(BigInteger.valueOf(15)).getPublic();
    BlockchainPublicKey key1 = new KeyPair(BigInteger.valueOf(12)).getPublic();

    EqualsVerifier.forClass(OracleMember.class)
        .withNonnullFields("key", "address")
        .withPrefabValues(BlockchainPublicKey.class, key0, key1)
        .verify();
  }
}
