package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/** A representation of a generic BYOC transfer dispute. */
@Immutable
public final class Dispute implements StateSerializable {
  private final BlockchainAddress challenger;
  private final int contractResultInvocation;
  private final int contractCounterClaimInvocation;
  private final FixedList<Integer> votes;
  private final FixedList<BlockchainAddress> counterClaimChallengers;

  @SuppressWarnings("unused")
  Dispute() {
    challenger = null;
    contractResultInvocation = 0;
    contractCounterClaimInvocation = 0;
    votes = null;
    counterClaimChallengers = null;
  }

  private Dispute(
      BlockchainAddress challenger,
      int contractResultInvocation,
      int contractCounterClaimInvocation,
      FixedList<Integer> votes,
      FixedList<BlockchainAddress> counterClaimChallengers) {
    this.challenger = challenger;
    this.contractResultInvocation = contractResultInvocation;
    this.contractCounterClaimInvocation = contractCounterClaimInvocation;
    this.votes = votes;
    this.counterClaimChallengers = counterClaimChallengers;
  }

  static Dispute createFromStateAccessor(StateAccessor accessor) {
    return new Dispute(
        accessor.get("challenger").blockchainAddressValue(),
        accessor.get("contractResultInvocation").intValue(),
        accessor.get("contractCounterClaimInvocation").intValue(),
        accessor.get("votes").typedFixedList(Integer.class),
        accessor.get("counterClaimChallengers").typedFixedList(BlockchainAddress.class));
  }

  /**
   * Create a new dispute of a BYOC transaction.
   *
   * @param challenger the account disputing the transaction
   * @param contractResultInvocation the invocation to call when a voting result has been reached
   * @param contractCounterClaimInvocation the invocation to call when a counter-claim is added
   * @param potentialVotesCount the number of block producers that can vote on the dispute
   * @return a new dispute
   */
  public static Dispute create(
      BlockchainAddress challenger,
      int contractResultInvocation,
      int contractCounterClaimInvocation,
      int potentialVotesCount) {
    FixedList<Integer> votes = FixedList.create(Collections.nCopies(potentialVotesCount, null));
    return new Dispute(
        challenger,
        contractResultInvocation,
        contractCounterClaimInvocation,
        votes,
        FixedList.create());
  }

  /**
   * Get the challenger.
   *
   * @return challenger
   */
  public BlockchainAddress getChallenger() {
    return challenger;
  }

  /**
   * Get the contract result invocation.
   *
   * @return contract result invocation
   */
  public int getContractResultInvocation() {
    return contractResultInvocation;
  }

  /**
   * Get the contract counterclaim invocation.
   *
   * @return contract counterclaim invocation
   */
  public int getContractCounterClaimInvocation() {
    return contractCounterClaimInvocation;
  }

  /**
   * Get the votes.
   *
   * @return votes
   */
  public List<Integer> getVotes() {
    return new ArrayList<>(Objects.requireNonNull(votes));
  }

  /**
   * Test if oracle member has already added counter-claim to this dispute.
   *
   * @param address the oracle member to test
   * @return true if member has added counter-claim, false otherwise
   */
  public boolean hasAddedCounterClaim(BlockchainAddress address) {
    return counterClaimChallengers.contains(address);
  }

  /**
   * Record that an oracle member has added counter-claim to dispute.
   *
   * @param address address of the oracle member
   * @return dispute updated with recorded counter-claim challenger
   */
  public Dispute recordCounterClaimChallenger(BlockchainAddress address) {
    return new Dispute(
        challenger,
        contractResultInvocation,
        contractCounterClaimInvocation,
        votes,
        counterClaimChallengers.addElement(address));
  }

  /**
   * Add vote to dispute.
   *
   * @param blockProducer the index of the block producer voting
   * @param vote the vote being cast
   * @return the dispute updated with the newly cast vote
   */
  public Dispute addVote(int blockProducer, int vote) {
    FixedList<Integer> updatedVotes = Objects.requireNonNull(votes).setElement(blockProducer, vote);
    return new Dispute(
        challenger,
        contractResultInvocation,
        contractCounterClaimInvocation,
        updatedVotes,
        counterClaimChallengers);
  }

  /**
   * Count the number of votes received. If 2/3 have cast votes, then we have enough for an honest
   * majority.
   *
   * @return true if enough votes have been cast, false otherwise
   */
  public boolean enoughVotesCast() {
    long castVotes = Objects.requireNonNull(votes).stream().filter(Objects::nonNull).count();
    return 3L * castVotes >= 2L * votes.size();
  }

  /**
   * Count the votes using Boyer-Moore majority vote algorithm. This algorithm assumes that there is
   * a candidate that holds the majority of votes. If the assumption is not true this algorithm is
   * not guaranteed to return a correct result.
   *
   * @return int representing the candidate with majority votes
   */
  public int countVotes() {
    int result = -1;
    int counter = 0;

    List<Integer> validVotes =
        Objects.requireNonNull(votes).stream()
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    for (int candidate : validVotes) {
      if (counter == 0) {
        result = candidate;
        counter++;
      } else if (result == candidate) {
        counter++;
      } else {
        counter--;
      }
    }
    return result;
  }
}
