package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/** Large oracle contract state. */
@Immutable
public final class LargeOracleContractState implements StateSerializable {

  private final FixedList<OracleMember> oracleMembers;

  private final BlockchainPublicKey oracleKey;

  private final BlockchainAddress bpOrchestrationContract;

  private final BlockchainAddress governanceUpdates;

  private final AvlTree<BlockchainAddress, StakedTokens> stakedTokens;

  private final AvlTree<OracleDisputeId, Dispute> activeDisputes;

  private final FixedList<SigningRequest> pendingMessages;
  private final int currentMessageNonce;
  private final int initialMessageNonce;
  private final AvlTree<SigningRequest, Signature> signedMessages;

  /** The number of USD cents an MPC token is worth. */
  private final Fraction mpcToUsdExchangeRate;

  private final AvlTree<String, ByocTwinToMpcTradeDeal> byocTwinsToMpcTrade;

  private final FixedList<OracleUpdateRequest> updateRequests;

  @SuppressWarnings("unused")
  LargeOracleContractState() {
    this.oracleMembers = null;
    this.oracleKey = null;
    this.bpOrchestrationContract = null;
    this.governanceUpdates = null;
    this.stakedTokens = null;
    this.pendingMessages = null;
    this.signedMessages = null;
    this.currentMessageNonce = 0;
    this.initialMessageNonce = 0;
    this.activeDisputes = null;
    this.mpcToUsdExchangeRate = null;
    this.byocTwinsToMpcTrade = null;
    this.updateRequests = null;
  }

  LargeOracleContractState(
      FixedList<OracleMember> oracleMembers,
      BlockchainPublicKey oracleKey,
      BlockchainAddress bpOrchestrationContract,
      BlockchainAddress governanceUpdates,
      AvlTree<BlockchainAddress, StakedTokens> stakedTokens,
      FixedList<SigningRequest> pendingMessages,
      Integer currentMessageNonce,
      Integer initialMessageNonce,
      AvlTree<SigningRequest, Signature> signedMessages,
      AvlTree<OracleDisputeId, Dispute> activeDisputes,
      Fraction mpcToUsdExchangeRate,
      AvlTree<String, ByocTwinToMpcTradeDeal> byocTwinsToMpcTrade,
      FixedList<OracleUpdateRequest> updateRequests) {
    this.oracleMembers = oracleMembers;
    this.oracleKey = oracleKey;
    this.bpOrchestrationContract = bpOrchestrationContract;
    this.governanceUpdates = governanceUpdates;
    this.stakedTokens = stakedTokens;
    this.pendingMessages = pendingMessages;
    this.currentMessageNonce = currentMessageNonce;
    this.initialMessageNonce = initialMessageNonce;
    this.signedMessages = signedMessages;
    this.activeDisputes = activeDisputes;
    this.mpcToUsdExchangeRate = mpcToUsdExchangeRate;
    this.byocTwinsToMpcTrade = byocTwinsToMpcTrade;
    this.updateRequests = updateRequests;
  }

  /**
   * Create an initial state.
   *
   * @param oracleMembers list of the member accounts public keys of the large oracle
   * @param oracleKey the public key of the large oracle
   * @param bpOrchestrationContract address of the block producer orchestration contract
   * @param governanceUpdates address of the governance update contract
   * @return initial state
   */
  public static LargeOracleContractState initial(
      List<OracleMember> oracleMembers,
      BlockchainPublicKey oracleKey,
      BlockchainAddress bpOrchestrationContract,
      BlockchainAddress governanceUpdates) {
    return new LargeOracleContractState(
        FixedList.create(oracleMembers),
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        AvlTree.create(),
        FixedList.create(),
        0,
        0,
        AvlTree.create(),
        AvlTree.create(),
        null,
        AvlTree.create(),
        FixedList.create());
  }

  /**
   * Get member of oracle.
   *
   * @param address address of member
   * @return member of oracle
   */
  public OracleMember getOracleMember(BlockchainAddress address) {
    int index = getOracleMemberIndex(address);
    return index != -1 ? Objects.requireNonNull(oracleMembers).get(index) : null;
  }

  /**
   * Get the index of an oracle member.
   *
   * @param address the address of the oracle member
   * @return the index of the oracle member or -1 if not found.
   */
  public int getOracleMemberIndex(BlockchainAddress address) {
    int index = 0;
    for (OracleMember member : Objects.requireNonNull(oracleMembers)) {
      if (member.address().equals(address)) {
        return index;
      }
      index++;
    }
    return -1;
  }

  /**
   * Get the oracle members.
   *
   * @return oracle members
   */
  public List<OracleMember> getOracleMembers() {
    return new ArrayList<>(Objects.requireNonNull(oracleMembers));
  }

  /**
   * Get the oracle key.
   *
   * @return oracle key
   */
  public BlockchainPublicKey getOracleKey() {
    return oracleKey;
  }

  /**
   * Get the block producer orchestration contract.
   *
   * @return block producer orchestration contract
   */
  public BlockchainAddress getBpOrchestrationContract() {
    return bpOrchestrationContract;
  }

  /**
   * Get the governance update.
   *
   * @return governance update
   */
  public BlockchainAddress getGovernanceUpdates() {
    return governanceUpdates;
  }

  /**
   * Get tokens staked by account.
   *
   * @param account account address
   * @return tokens staked by account
   */
  public StakedTokens getTokensStakedBy(BlockchainAddress account) {
    return Objects.requireNonNullElse(stakedTokens.getValue(account), StakedTokens.create());
  }

  /**
   * Find all address of oracles that currently have stakes locked to the address of a small oracle.
   *
   * @param oracle address of the small oracle to find members of
   * @return a list of blockchain addresses
   */
  public List<BlockchainAddress> getMembersOfOldOracle(BlockchainAddress oracle) {
    List<BlockchainAddress> oldLockedStaked = new ArrayList<>();
    for (BlockchainAddress account : stakedTokens.keySet()) {
      if (stakedTokens.getValue(account).getLockedToOracle(oracle) > 0L) {
        oldLockedStaked.add(account);
      }
    }
    return oldLockedStaked;
  }

  /**
   * Get active disputes.
   *
   * @return active disputes
   */
  public AvlTree<OracleDisputeId, Dispute> getActiveDisputes() {
    return activeDisputes;
  }

  /**
   * Get dispute.
   *
   * @param oracleDisputeId dispute id
   * @return dispute
   */
  public Dispute getDispute(OracleDisputeId oracleDisputeId) {
    return Objects.requireNonNull(activeDisputes).getValue(oracleDisputeId);
  }

  /**
   * Get pending messages.
   *
   * @return pending messages
   */
  public List<SigningRequest> getPendingMessages() {
    return new ArrayList<>(Objects.requireNonNull(pendingMessages));
  }

  /**
   * Get signature.
   *
   * @param nonce the nonce
   * @param message the message hash
   * @param transactionHash the random hash
   * @return signature
   */
  public Signature getSignature(int nonce, Hash message, Hash transactionHash) {
    return Objects.requireNonNull(signedMessages)
        .getValue(new SigningRequest(nonce, message, transactionHash));
  }

  /**
   * Get signed messages.
   *
   * @return list of signed messages
   */
  public List<SigningRequest> getSignedMessages() {
    return new ArrayList<>(Objects.requireNonNull(signedMessages).keySet());
  }

  /**
   * Get BYOC twin to MPC deal.
   *
   * @param symbol identifier
   * @return BYOC twin to MPC deal
   */
  public ByocTwinToMpcTradeDeal getByocTwinToMpcTradeDeal(String symbol) {
    return Objects.requireNonNull(byocTwinsToMpcTrade).getValue(symbol);
  }

  /**
   * Add or update a dispute.
   *
   * @param oracleDisputeId the id of the dispute
   * @param dispute the dispute to set or update
   * @return a state with an updated dispute
   */
  public LargeOracleContractState setDispute(OracleDisputeId oracleDisputeId, Dispute dispute) {
    AvlTree<OracleDisputeId, Dispute> updatedDisputes =
        Objects.requireNonNull(activeDisputes).set(oracleDisputeId, dispute);
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens,
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        updatedDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Removes a dispute.
   *
   * @param oracleDisputeId the id of the dispute to remove
   * @return a state with the dispute removed
   */
  public LargeOracleContractState clearDispute(OracleDisputeId oracleDisputeId) {
    AvlTree<OracleDisputeId, Dispute> updatedDisputes =
        Objects.requireNonNull(activeDisputes).remove(oracleDisputeId);

    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens,
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        updatedDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Associate tokens to this contract for account.
   *
   * @param account address of the account
   * @param amount amount to associate
   * @param expirationTimestamp timestamp when association expires
   * @return updated state
   */
  public LargeOracleContractState stakeTokens(
      BlockchainAddress account, long amount, Long expirationTimestamp) {
    StakedTokens accountTokens =
        getTokensStakedBy(account).associateTokens(amount, expirationTimestamp);
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens.set(account, accountTokens),
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Update expiration timestamp for association to this contract for account.
   *
   * @param account address of the account
   * @param expirationTimestamp timestamp when association expires
   * @return updated state
   */
  public LargeOracleContractState setExpirationForAssociation(
      BlockchainAddress account, Long expirationTimestamp) {
    StakedTokens accountTokens =
        getTokensStakedBy(account).setExpirationTimestamp(expirationTimestamp);
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens.set(account, accountTokens),
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Disassociate tokens from the contract for an account.
   *
   * @param account address of the account
   * @param amount amount to disassociate
   * @return updated state
   */
  public LargeOracleContractState disassociateTokens(BlockchainAddress account, long amount) {
    StakedTokens accountTokens = getTokensStakedBy(account).disassociateTokens(amount);
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens.set(account, accountTokens),
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Lock tokens for account towards doing work as an oracle.
   *
   * @param account the account to lock tokens for
   * @param oracle the oracle which the tokens are locked to
   * @param amount the amount of tokens to lock
   * @param blockProductionTime the block production time
   * @return a new state where <code>account</code> has <code>amount</code> of tokens locked to
   *     <code>oracle</code>
   */
  public LargeOracleContractState lockTokensToOracle(
      BlockchainAddress account, BlockchainAddress oracle, long amount, long blockProductionTime) {
    StakedTokens accountTokens =
        getTokensStakedBy(account).lockToOracle(oracle, amount, blockProductionTime);
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens.set(account, accountTokens),
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Unlock tokens for account towards doing work as an oracle.
   *
   * @param account the account to unlock tokens from
   * @param oracle the oracle which the tokens are unlocked from
   * @param amount the amount of tokens to unlock
   * @return a new state where the <code>account</code> has <code>amount</code> tokens unlocked from
   *     oracle work
   */
  public LargeOracleContractState unlockTokensFromOracle(
      BlockchainAddress account, BlockchainAddress oracle, long amount) {
    StakedTokens accountTokens = getTokensStakedBy(account).unlockFromOracle(oracle, amount);
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens.set(account, accountTokens),
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Free tokens locked for the account towards working as an oracle.
   *
   * @param account the account to free tokens for
   * @param oracle the oracle to free tokens from
   * @param timestamp the timestamp of when the tokens are freed
   * @return a new state where the account has tokens freed from oracle work
   */
  public LargeOracleContractState releaseTokensFromOracle(
      BlockchainAddress account, BlockchainAddress oracle, long timestamp) {
    StakedTokens accountTokens = getTokensStakedBy(account).releaseFromOracle(oracle, timestamp);
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens.set(account, accountTokens),
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Clear stakes pending final release 14 days older than the given block production timestamp for
   * this contract.
   *
   * @param oracleContract the contract holding pending stakes
   * @param blockProductionTime the block production time of the transaction
   * @return new state without pending stakes older than the timestamp for the contract
   */
  public LargeOracleContractState clearPendingOracleStakes(
      BlockchainAddress oracleContract, Long blockProductionTime) {
    long fourteenDays = blockProductionTime - Duration.ofDays(14).toMillis();
    AvlTree<BlockchainAddress, StakedTokens> updatedStakedTokens = stakedTokens;
    for (BlockchainAddress account : updatedStakedTokens.keySet()) {
      StakedTokens accountTokens =
          updatedStakedTokens
              .getValue(account)
              .clearPendingForOracleOlderThan(oracleContract, fourteenDays);
      updatedStakedTokens = updatedStakedTokens.set(account, accountTokens);
    }
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        updatedStakedTokens,
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Lock some of the account's staked tokens for a dispute.
   *
   * @param account the account to lock tokens for
   * @param disputeId the dispute to lock tokens to
   * @param amount the amount of tokens to lock
   * @param blockProductionTime the block production time
   * @return a state where the account has some locked tokens to a dispute
   */
  public LargeOracleContractState lockTokensToDispute(
      BlockchainAddress account, OracleDisputeId disputeId, long amount, long blockProductionTime) {
    StakedTokens accountTokens =
        getTokensStakedBy(account).lockToDispute(disputeId, amount, blockProductionTime);
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens.set(account, accountTokens),
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Unlock all tokens locked to dispute.
   *
   * @param account the account to unlock tokens for
   * @param disputeId the dispute to free tokens from
   * @return a state where the account no longer has tokens locked to dispute
   */
  public LargeOracleContractState releaseTokensFromDispute(
      BlockchainAddress account, OracleDisputeId disputeId) {
    StakedTokens accountTokens = getTokensStakedBy(account).releaseFromDispute(disputeId);
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens.set(account, accountTokens),
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Burn all the tokens the account has locked on a dispute.
   *
   * @param account the account to burn tokens for
   * @param disputeId the dispute the tokens are locked to
   * @return a state where the locked tokens no longer exists
   */
  public LargeOracleContractState burnTokensLockedToDispute(
      BlockchainAddress account, OracleDisputeId disputeId) {
    StakedTokens accountTokens = getTokensStakedBy(account).burnFromDispute(disputeId);
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens.set(account, accountTokens),
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Get the current message nonce.
   *
   * @return current message nonce
   */
  public Integer getCurrentMessageNonce() {
    return currentMessageNonce;
  }

  /**
   * Get the initial message nonce.
   *
   * @return initial message nonce
   */
  public Integer getInitialMessageNonce() {
    return initialMessageNonce;
  }

  /**
   * Burn tokens that are locked to do oracle work.
   *
   * @param account the account with staked tokens
   * @param oracleContract the oracle contract to burn tokens from
   * @param amount the amount of tokens to burn
   * @return a state where the tokens no longer exists
   */
  public LargeOracleContractState burnOracleTokens(
      BlockchainAddress account, BlockchainAddress oracleContract, long amount) {
    StakedTokens accountTokens = getTokensStakedBy(account).burnFromOracle(oracleContract, amount);
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens.set(account, accountTokens),
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Update the amount of reserved tokens for an account.
   *
   * @param account address of the account
   * @param newReserved the new amount of reserved tokens
   * @return updated state
   */
  public LargeOracleContractState setReservedTokens(BlockchainAddress account, long newReserved) {
    StakedTokens updated = getTokensStakedBy(account).setReserved(newReserved);
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens.set(account, updated),
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Add a new message that should be signed by the large oracle.
   *
   * @param message the message that is to be signed
   * @param transactionHash the random hash for the message to be signed
   * @return the updated state
   */
  public LargeOracleContractState addMessageForSigning(Hash message, Hash transactionHash) {
    int nonce = currentMessageNonce;
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens,
        Objects.requireNonNull(pendingMessages)
            .addElement(new SigningRequest(nonce, message, transactionHash)),
        nonce + 1,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Replace the large oracle and its key with a new one.
   *
   * @param newOracle the new oracle
   * @param newPublicKey the key of the new oracle
   * @return state with a new oracle.
   */
  public LargeOracleContractState replaceLargeOracle(
      List<OracleMember> newOracle, BlockchainPublicKey newPublicKey) {
    int nextInitialMessageNonce = currentMessageNonce - pendingMessages.size();
    return new LargeOracleContractState(
        FixedList.create(newOracle),
        newPublicKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens,
        pendingMessages,
        currentMessageNonce,
        nextInitialMessageNonce,
        signedMessages,
        resetDisputesForNewOracle(newOracle.size()),
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  private AvlTree<OracleDisputeId, Dispute> resetDisputesForNewOracle(int newOracleSize) {
    AvlTree<OracleDisputeId, Dispute> oldDisputes = this.getActiveDisputes();

    HashMap<OracleDisputeId, Dispute> newDisputes = new HashMap<>();
    for (OracleDisputeId id : oldDisputes.keySet()) {
      Dispute oldDispute = oldDisputes.getValue(id);
      newDisputes.put(
          id,
          Dispute.create(
              oldDispute.getChallenger(),
              oldDispute.getContractResultInvocation(),
              oldDispute.getContractCounterClaimInvocation(),
              newOracleSize));
    }
    return AvlTree.create(newDisputes);
  }

  /**
   * Add a finished signature.
   *
   * @param message the message that was signed
   * @param signature the signature
   * @return the updated state.
   */
  public LargeOracleContractState addSignature(SigningRequest message, Signature signature) {
    if (Objects.requireNonNull(signedMessages).containsKey(message)) {
      // do nothing in case we already have this particular signature.
      return this;
    } else {
      return new LargeOracleContractState(
          oracleMembers,
          oracleKey,
          bpOrchestrationContract,
          governanceUpdates,
          stakedTokens,
          Objects.requireNonNull(pendingMessages).removeElement(message),
          currentMessageNonce,
          initialMessageNonce,
          Objects.requireNonNull(signedMessages).set(message, signature),
          activeDisputes,
          mpcToUsdExchangeRate,
          byocTwinsToMpcTrade,
          updateRequests);
    }
  }

  /**
   * Find a collection of randomized nodes of a given size with a stake of at least some size.
   *
   * @param requiredStake the minimum stake needed to be considered
   * @param oracleSize the size of the resultant oracle
   * @param seed used for shuffling of the nodes
   * @param blockProductionTime the block production time
   * @return a randomized list of oracles, or an empty list if not enough members is found
   */
  public List<OracleMember> findOracleNodes(
      long requiredStake, int oracleSize, long seed, long blockProductionTime) {
    return findOracleNodes(
        requiredStake, oracleSize, randomizeList(getOracleMembers(), seed), blockProductionTime);
  }

  private List<OracleMember> findOracleNodes(
      long requiredStake,
      int oracleSize,
      List<OracleMember> oracleMembers,
      long blockProductionTime) {
    List<OracleMember> oracle = new ArrayList<>();
    for (OracleMember candidate : oracleMembers) {
      StakedTokens stakedTokens = getTokensStakedBy(candidate.address());
      long allocatableTokens = stakedTokens.getAllocatableTokens();

      boolean tokenHasExpired = stakedTokens.hasExpired(blockProductionTime);
      boolean enoughAllocated = allocatableTokens >= requiredStake;
      if (enoughAllocated && !tokenHasExpired) {
        oracle.add(candidate);
      }
      if (oracle.size() == oracleSize) {
        return Collections.unmodifiableList(oracle);
      }
    }
    return Collections.emptyList();
  }

  static <T> List<T> randomizeList(List<T> elements, long seed) {
    List<T> randomizedElements = new ArrayList<>(elements);
    Collections.shuffle(randomizedElements, new Random(seed));
    return randomizedElements;
  }

  /**
   * Get the first pending request to update a small oracle, if any exists.
   *
   * @return the first pending update requests, null otherwise.
   */
  public OracleUpdateRequest getPendingUpdateRequest() {
    if (updateRequests.size() > 0) {
      return updateRequests.get(0);
    } else {
      return null;
    }
  }

  /**
   * Set a new exchange rate.
   *
   * @param exchangeRate the exchange rate represented as a fraction
   * @return a new state with an updated exchange rate
   */
  public LargeOracleContractState setMpcToUsdExchangeRate(Fraction exchangeRate) {
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens,
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        exchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  /**
   * Converts from USD to MPC tokens. Rounds up to closest integer.
   *
   * @param usdAmount the amount of USD to convert
   * @return the value of the USD amount in MPC tokens
   */
  public long convertToMpcTokens(Fraction usdAmount) {
    Fraction exchangeRate = Objects.requireNonNull(mpcToUsdExchangeRate);

    Unsigned256 numerator =
        Unsigned256.create(usdAmount.getNumerator())
            .multiply(Unsigned256.create(exchangeRate.getDenominator()));
    Unsigned256 denominator =
        Unsigned256.create(usdAmount.getDenominator())
            .multiply(Unsigned256.create(exchangeRate.getNumerator()));

    Unsigned256 ceilingDivision =
        numerator.add(denominator).subtract(Unsigned256.ONE).divide(denominator);
    return ceilingDivision.longValueExact();
  }

  /**
   * Adds a new trade deal. If a trade deal already exists, then adds to that.
   *
   * @param symbol the coin to trade
   * @param byocAmount the BYOC twin amount that can be sold
   * @param mpcTokens the amount of MPC tokens that can be bought
   * @return a new state with updated trade deal
   */
  public LargeOracleContractState addByocTwinToMpcDeal(
      String symbol, Unsigned256 byocAmount, long mpcTokens) {
    ByocTwinToMpcTradeDeal existingDeal =
        Objects.requireNonNull(byocTwinsToMpcTrade).getValue(symbol);
    ByocTwinToMpcTradeDeal newDeal;
    if (existingDeal == null) {
      newDeal = new ByocTwinToMpcTradeDeal(byocAmount, mpcTokens);
    } else {
      newDeal =
          new ByocTwinToMpcTradeDeal(
              byocAmount.add(existingDeal.getByocTwins()), mpcTokens + existingDeal.getMpcTokens());
    }
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens,
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade.set(symbol, newDeal),
        updateRequests);
  }

  /**
   * Deducts from existing trade deal.
   *
   * @param symbol the coin to trade
   * @param byocAmount the BYOC twin amount that has been sold
   * @param mpcTokens the amount of MPC tokens that has been bought
   * @return a new state with updated trade deal
   */
  public LargeOracleContractState deductByocTwinToMpcDeal(
      String symbol, Unsigned256 byocAmount, long mpcTokens) {

    ByocTwinToMpcTradeDeal existingDeal =
        Objects.requireNonNull(byocTwinsToMpcTrade).getValue(symbol);
    ByocTwinToMpcTradeDeal newDeal =
        new ByocTwinToMpcTradeDeal(
            existingDeal.getByocTwins().subtract(byocAmount),
            existingDeal.getMpcTokens() - mpcTokens);

    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens,
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade.set(symbol, newDeal),
        updateRequests);
  }

  /**
   * Add a new oracle update requests to be fulfilled at a later time.
   *
   * @param request request to add
   * @return new state with a pending update request
   */
  public LargeOracleContractState addPendingOracleUpdateRequest(OracleUpdateRequest request) {
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens,
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests.addElement(request));
  }

  /**
   * Remove the first of the pending oracle update requests.
   *
   * @return a new state with the first oracle update request removed
   */
  public LargeOracleContractState removePendingOracleUpdateRequest() {
    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens,
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests.removeElementAtIndex(0));
  }

  SigningRequest getSigningRequest(int nonce, Hash message) {
    for (SigningRequest request : pendingMessages) {
      if (request.getNonce() == nonce && message.equals(request.getMessageHash())) {
        return request;
      }
    }
    return null;
  }

  int numberOfSignaturesRequestedFromCurrentCommittee() {
    return currentMessageNonce - initialMessageNonce;
  }
}
