package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/** Represents a trade deal from BYOC twins to MPC tokens. */
@Immutable
public final class ByocTwinToMpcTradeDeal implements StateSerializable {

  private final Unsigned256 byocTwins;
  private final long mpcTokens;

  /** Serializable constructor. */
  @SuppressWarnings("unused")
  public ByocTwinToMpcTradeDeal() {
    this.byocTwins = null;
    this.mpcTokens = 0;
  }

  /**
   * Create BYOC twin to MPC trade deal.
   *
   * @param byocTwins BYOC twins
   * @param mpcTokens MPC tokens
   */
  public ByocTwinToMpcTradeDeal(Unsigned256 byocTwins, long mpcTokens) {
    this.byocTwins = byocTwins;
    this.mpcTokens = mpcTokens;
  }

  static ByocTwinToMpcTradeDeal createFromStateAccessor(StateAccessor accessor) {
    return new ByocTwinToMpcTradeDeal(
        accessor.get("byocTwins").cast(Unsigned256.class), accessor.get("mpcTokens").longValue());
  }

  /**
   * Get the BYOC twins.
   *
   * @return BYOC twins
   */
  public Unsigned256 getByocTwins() {
    return byocTwins;
  }

  /**
   * Get the mpc tokens.
   *
   * @return mpc tokens
   */
  public long getMpcTokens() {
    return mpcTokens;
  }
}
