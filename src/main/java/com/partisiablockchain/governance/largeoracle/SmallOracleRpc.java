package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.secata.stream.DataStreamSerializable;
import java.util.List;

final class SmallOracleRpc {

  @SuppressWarnings("unused")
  private SmallOracleRpc() {}

  static DataStreamSerializable addCounterClaim(int invocationByte, byte[] passThrough) {
    return stream -> {
      stream.writeByte(invocationByte);
      stream.write(passThrough);
    };
  }

  static DataStreamSerializable sendPollResult(
      int invocationByte, long oracleId, long disputeId, int pollResult) {
    return stream -> {
      stream.writeByte(invocationByte);
      stream.writeLong(oracleId);
      stream.writeLong(disputeId);
      stream.writeInt(pollResult);
    };
  }

  static DataStreamSerializable updateRequest(
      int callback, List<BlockchainAddress> addresses, List<BlockchainPublicKey> keys) {
    return stream -> {
      stream.writeByte(callback);
      BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, addresses);
      BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(stream, keys);
    };
  }
}
