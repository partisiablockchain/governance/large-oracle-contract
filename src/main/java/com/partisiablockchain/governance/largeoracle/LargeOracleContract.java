package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.FormatMethod;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** LargeOracleContract. */
@AutoSysContract(LargeOracleContractState.class)
public final class LargeOracleContract {

  private static final Logger logger = LoggerFactory.getLogger(LargeOracleContract.class);

  static final int INSUFFICIENT_TOKENS = -2;
  static final int NO_FRAUD = -1;

  /** The number of gas per USD cents. */
  static final long GAS_TO_USD = 1_000;

  static final int NUMBER_OF_SIGNATURES_PER_COMMITTEE = 9;

  /**
   * Initialize the large oracle contract.
   *
   * @param blockchainPublicKeys blockchain public keys of the initial oracle nodes
   * @param blockchainAddresses blockchain addresses of the initial oracle nodes
   * @param oracleKey oracle public key
   * @param bpOrchestrationContract orchestration contract that created the oracle
   * @param governanceUpdates governance update contract
   * @return initial state
   */
  @Init
  public LargeOracleContractState create(
      List<BlockchainPublicKey> blockchainPublicKeys,
      List<BlockchainAddress> blockchainAddresses,
      BlockchainPublicKey oracleKey,
      BlockchainAddress bpOrchestrationContract,
      BlockchainAddress governanceUpdates) {

    return LargeOracleContractState.initial(
        OracleMember.createOracleMembers(blockchainAddresses, blockchainPublicKeys),
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates);
  }

  /**
   * Migrate an existing contract to this version.
   *
   * @param oldState accessor for the old state
   * @return the migrated state
   */
  @Upgrade
  public LargeOracleContractState upgrade(StateAccessor oldState) {
    FixedList<OracleMember> oracleMembers =
        FixedList.create(
            oldState.get("oracleMembers").getListElements().stream()
                .map(OracleMember::createFromStateAccessor)
                .toList());
    BlockchainPublicKey oracleKey = oldState.get("oracleKey").blockchainPublicKeyValue();
    BlockchainAddress bpOrchestrationContract =
        oldState.get("bpOrchestrationContract").blockchainAddressValue();
    BlockchainAddress governanceUpdates =
        oldState.get("governanceUpdates").blockchainAddressValue();
    AvlTree<BlockchainAddress, StakedTokens> stakedTokens =
        createStakeTree(oldState.get("stakedTokens"));
    FixedList<SigningRequest> pendingMessages =
        FixedList.create(
            oldState.get("pendingMessages").getListElements().stream()
                .map(SigningRequest::createFromStateAccessor)
                .toList());
    int currentMessageNonce = oldState.get("currentMessageNonce").intValue();
    int initialMessageNonce = oldState.get("initialMessageNonce").intValue();
    AvlTree<SigningRequest, Signature> signedMessages =
        createSignatureTree(oldState.get("signedMessages"));
    AvlTree<OracleDisputeId, Dispute> activeDisputes =
        createActiveDisputeTree(oldState.get("activeDisputes"));
    Fraction mpcToUsdExchangeRate =
        Fraction.createFromStateAccessor(oldState.get("mpcToUsdExchangeRate"));
    AvlTree<String, ByocTwinToMpcTradeDeal> byocTwinsToMpcTrade =
        createMpcTradeDealTree(oldState.get("byocTwinsToMpcTrade"));
    FixedList<OracleUpdateRequest> updateRequests =
        FixedList.create(
            oldState.get("updateRequests").getListElements().stream()
                .map(OracleUpdateRequest::createFromStateAccessor));

    return new LargeOracleContractState(
        oracleMembers,
        oracleKey,
        bpOrchestrationContract,
        governanceUpdates,
        stakedTokens,
        pendingMessages,
        currentMessageNonce,
        initialMessageNonce,
        signedMessages,
        activeDisputes,
        mpcToUsdExchangeRate,
        byocTwinsToMpcTrade,
        updateRequests);
  }

  private static AvlTree<String, ByocTwinToMpcTradeDeal> createMpcTradeDealTree(
      StateAccessor accessor) {
    AvlTree<String, ByocTwinToMpcTradeDeal> tradeDealAvlTree = AvlTree.create();
    for (StateAccessorAvlLeafNode entry : accessor.getTreeLeaves()) {
      ByocTwinToMpcTradeDeal tradeDeal =
          ByocTwinToMpcTradeDeal.createFromStateAccessor(entry.getValue());
      String symbol = entry.getKey().stringValue();
      tradeDealAvlTree = tradeDealAvlTree.set(symbol, tradeDeal);
    }
    return tradeDealAvlTree;
  }

  private static AvlTree<OracleDisputeId, Dispute> createActiveDisputeTree(StateAccessor accessor) {
    AvlTree<OracleDisputeId, Dispute> oracleDisputeIdDisputeAvlTree = AvlTree.create();
    for (StateAccessorAvlLeafNode entry : accessor.getTreeLeaves()) {
      OracleDisputeId oracleDisputeId = OracleDisputeId.createFromStateAccessor(entry.getKey());
      Dispute dispute = Dispute.createFromStateAccessor(entry.getValue());
      oracleDisputeIdDisputeAvlTree = oracleDisputeIdDisputeAvlTree.set(oracleDisputeId, dispute);
    }
    return oracleDisputeIdDisputeAvlTree;
  }

  private static AvlTree<SigningRequest, Signature> createSignatureTree(StateAccessor accessor) {
    AvlTree<SigningRequest, Signature> hashAndNonceSignatureAvlTree = AvlTree.create();
    for (StateAccessorAvlLeafNode entry : accessor.getTreeLeaves()) {
      SigningRequest hashAndNonce = SigningRequest.createFromStateAccessor(entry.getKey());
      Signature signature = entry.getValue().signatureValue();
      hashAndNonceSignatureAvlTree = hashAndNonceSignatureAvlTree.set(hashAndNonce, signature);
    }
    return hashAndNonceSignatureAvlTree;
  }

  private static AvlTree<BlockchainAddress, StakedTokens> createStakeTree(StateAccessor accessor) {
    AvlTree<BlockchainAddress, StakedTokens> stakedTokensMap = AvlTree.create();
    for (StateAccessorAvlLeafNode entry : accessor.getTreeLeaves()) {
      BlockchainAddress address = entry.getKey().blockchainAddressValue();
      StakedTokens stakedTokens = StakedTokens.createFromStateAccessor(entry.getValue());
      stakedTokensMap = stakedTokensMap.set(address, stakedTokens);
    }
    return stakedTokensMap;
  }

  static long convertByocTwinsToMpcTokens(
      LargeOracleContractState state,
      SysContractContext context,
      String byocTwinSymbol,
      Unsigned256 byocAmount) {
    Unsigned256 valueInGas = convertByocToGas(context, byocTwinSymbol, byocAmount);
    Fraction valueInUsd = gasToUsd(valueInGas);
    return state.convertToMpcTokens(valueInUsd);
  }

  static Unsigned256 convertByocToGas(
      SysContractContext context, String byocTwinSymbol, Unsigned256 byocAmount) {
    StateSerializable globalAccountPluginState = context.getGlobalAccountPluginState();
    StateAccessor stateAccessor = StateAccessor.create(globalAccountPluginState);
    List<StateAccessor> coins = stateAccessor.get("coins").get("coins").getListElements();
    Fraction byocToGas =
        coins.stream()
            .filter(x -> byocTwinSymbol.equals(x.get("symbol").stringValue()))
            .map(x -> x.get("conversionRate"))
            .map(
                fraction ->
                    new Fraction(
                        fraction.get("numerator").longValue(),
                        fraction.get("denominator").longValue()))
            .findFirst()
            .orElseThrow(
                () ->
                    new IllegalArgumentException(
                        "The symbol '" + byocTwinSymbol + "' was not found"));

    Unsigned256 valueInGas =
        byocAmount
            .multiply(Unsigned256.create(byocToGas.getNumerator()))
            .divide(Unsigned256.create(byocToGas.getDenominator()));
    return valueInGas;
  }

  private static Fraction gasToUsd(Unsigned256 valueInGas) {
    return new Fraction(valueInGas.longValueExact(), GAS_TO_USD);
  }

  /**
   * Check if contract is allowed to act as small oracle.
   *
   * @param from address to check
   * @return true if contract is small oracle, false if large
   */
  public static boolean isAllowedOracleContract(BlockchainAddress from) {
    return from.getType() == BlockchainAddress.Type.CONTRACT_GOV;
  }

  private static LargeOracleContractState createDisputePoll(
      BlockchainAddress disputeChallenger,
      int smallOraclesResultInvocation,
      int smallOraclesCounterClaimInvocation,
      LargeOracleContractState state,
      long oracleId,
      long disputeId,
      BlockchainAddress from,
      long disputeTokenCost,
      long blockProductionTime) {
    Dispute dispute =
        Dispute.create(
            disputeChallenger,
            smallOraclesResultInvocation,
            smallOraclesCounterClaimInvocation,
            state.getOracleMembers().size());

    logger.info(
        "{} initiated a dispute poll for {}/{} on {}",
        disputeChallenger,
        oracleId,
        disputeId,
        from);
    OracleDisputeId oracleDisputeId = new OracleDisputeId(from, oracleId, disputeId);
    return state
        .setDispute(oracleDisputeId, dispute)
        .lockTokensToDispute(
            disputeChallenger, oracleDisputeId, disputeTokenCost, blockProductionTime);
  }

  /**
   * Creates a poll for a dispute in a small oracle contract to be voted on by the large oracle
   * members. Can only be called by a small oracle contract.
   *
   * @param context execution context
   * @param state current state
   * @param disputeChallenger the dispute challenger
   * @param disputeTokenCost token cost of the dispute
   * @param oracleId id of the oracle that this dispute relates to
   * @param disputeId id for the dispute
   * @param smallOraclesResultInvocation invocation to call when a voting result has been reached
   * @param smallOraclesCounterClaimInvocation invocation to call for adding counter-claims to the
   *     dispute
   * @return updated state with a dispute poll created
   */
  @Action(Invocations.CREATE_DISPUTE_POLL)
  public LargeOracleContractState createDisputePoll(
      SysContractContext context,
      LargeOracleContractState state,
      BlockchainAddress disputeChallenger,
      long disputeTokenCost,
      long oracleId,
      long disputeId,
      @RpcType(signed = false, size = 1) int smallOraclesResultInvocation,
      @RpcType(signed = false, size = 1) int smallOraclesCounterClaimInvocation) {
    BlockchainAddress from = context.getFrom();
    ensure(
        isAllowedOracleContract(from),
        "Disputes can only be created by one of the small oracle contracts");
    long allocatableTokens = state.getTokensStakedBy(disputeChallenger).getAllocatableTokens();
    boolean hasExpired =
        state.getTokensStakedBy(disputeChallenger).hasExpired(context.getBlockProductionTime());
    if (allocatableTokens < disputeTokenCost || hasExpired) {
      logger.warn(
          "{} has insufficient staked tokens to initiate a dispute poll", disputeChallenger);
      context
          .getInvocationCreator()
          .invoke(from)
          .withPayload(
              SmallOracleRpc.sendPollResult(
                  smallOraclesResultInvocation, oracleId, disputeId, INSUFFICIENT_TOKENS))
          .sendFromContract();
      return state;
    }

    return createDisputePoll(
        disputeChallenger,
        smallOraclesResultInvocation,
        smallOraclesCounterClaimInvocation,
        state,
        oracleId,
        disputeId,
        from,
        disputeTokenCost,
        context.getBlockProductionTime());
  }

  /**
   * Adds a counter claim to an ongoing dispute. Only members of the large oracle can add counter
   * claims to disputes.
   *
   * @param context execution context
   * @param state current state
   * @param smallOracle oracle contract where the dispute is happening
   * @param oracleId id of the oracle that this dispute relates to
   * @param disputeId id of the dispute
   * @param passThrough additional data for the oracle contracts
   * @return updated state with dispute counter claim added
   */
  @Action(Invocations.ADD_DISPUTE_COUNTER_CLAIM)
  public LargeOracleContractState addDisputeCounterClaim(
      SysContractContext context,
      LargeOracleContractState state,
      BlockchainAddress smallOracle,
      long oracleId,
      long disputeId,
      byte[] passThrough) {
    BlockchainAddress sender = context.getFrom();
    ensure(
        state.getOracleMember(sender) != null,
        "Only members of the large oracle can add counter-claims to disputes");
    OracleDisputeId oracleDisputeId = new OracleDisputeId(smallOracle, oracleId, disputeId);
    Dispute activeDispute = state.getDispute(oracleDisputeId);
    ensure(activeDispute != null, "No dispute with this id in progress");
    ensure(
        !activeDispute.hasAddedCounterClaim(sender),
        "Only one counter-claim per oracle member allowed");

    context
        .getInvocationCreator()
        .invoke(smallOracle)
        .withPayload(
            SmallOracleRpc.addCounterClaim(
                activeDispute.getContractCounterClaimInvocation(), passThrough))
        .sendFromContract();

    logger.info("{} added counter-claim to dispute {} on {}", sender, disputeId, smallOracle);
    return state.setDispute(oracleDisputeId, activeDispute.recordCounterClaimChallenger(sender));
  }

  /**
   * Vote on a dispute and send the result back to the small oracle if enough oracle members have
   * voted. Only members of the large oracle can vote on disputes.
   *
   * @param context execution context
   * @param state current state
   * @param smallOracle oracle contract where the dispute is happening
   * @param oracleId id of the oracle that this dispute relates to
   * @param disputeId id of the dispute
   * @param vote the index of the claim to vote on
   * @return updated state with the vote added to the dispute
   */
  @Action(Invocations.VOTE_ON_DISPUTE)
  public LargeOracleContractState voteOnDispute(
      SysContractContext context,
      LargeOracleContractState state,
      BlockchainAddress smallOracle,
      long oracleId,
      long disputeId,
      int vote) {
    BlockchainAddress voter = context.getFrom();
    ensure(
        state.getOracleMember(voter) != null,
        "Only members of the large oracle can vote on disputes");

    OracleDisputeId oracleDisputeId = new OracleDisputeId(smallOracle, oracleId, disputeId);
    Dispute activeDispute = state.getDispute(oracleDisputeId);
    ensure(activeDispute != null, "No dispute with this id to vote on");

    int bpIndex = state.getOracleMemberIndex(voter);
    Dispute dispute = activeDispute.addVote(bpIndex, vote);
    if (dispute.enoughVotesCast()) {
      logger.info("Sufficient amount of votes cast. Counting result and closing dispute");
      int votingResult = dispute.countVotes();

      SystemEventCreator eventManager = context.getInvocationCreator();
      eventManager
          .invoke(smallOracle)
          .withPayload(
              SmallOracleRpc.sendPollResult(
                  dispute.getContractResultInvocation(), oracleId, disputeId, votingResult))
          .sendFromContract();

      BlockchainAddress challenger = dispute.getChallenger();
      if (votingResult == NO_FRAUD) {
        long amount = state.getTokensStakedBy(challenger).getLockedToDispute(oracleDisputeId);
        if (amount != 0) {
          burnStakedTokens(eventManager, context.getContractAddress(), challenger, amount);
        }
        return state
            .burnTokensLockedToDispute(challenger, oracleDisputeId)
            .clearDispute(oracleDisputeId);
      } else {
        return state
            .releaseTokensFromDispute(challenger, oracleDisputeId)
            .clearDispute(oracleDisputeId);
      }
    } else {
      return state.setDispute(oracleDisputeId, dispute);
    }
  }

  /**
   * Burns staked tokens for malicious oracle nodes specified by the given addresses. Can only be
   * called by small oracle contracts.
   *
   * @param context execution context
   * @param state current state
   * @param oracles oracle nodes to have their staked tokens burned
   * @param amount amount of burned staked tokens
   * @return updated state with staked tokens burned for the given oracle nodes
   */
  @Action(Invocations.BURN_STAKED_TOKENS)
  public LargeOracleContractState burnStakedTokens(
      SysContractContext context,
      LargeOracleContractState state,
      List<BlockchainAddress> oracles,
      long amount) {
    BlockchainAddress from = context.getFrom();
    ensure(
        isAllowedOracleContract(from),
        "Tokens can only be burned by one of the small oracle contracts");

    SystemEventCreator eventCreator = context.getInvocationCreator();
    BlockchainAddress largeOracleContract = context.getContractAddress();
    LargeOracleContractState updatedState = state;
    for (BlockchainAddress oracle : oracles) {
      burnStakedTokens(eventCreator, largeOracleContract, oracle, amount);
      updatedState = updatedState.burnOracleTokens(oracle, from, amount);
    }
    return updatedState;
  }

  private static void burnStakedTokens(
      SystemEventCreator eventManager,
      BlockchainAddress contractToBurnFrom,
      BlockchainAddress accountToBurnFrom,
      long amount) {
    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            accountToBurnFrom, AccountPluginRpc.burnStakedTokens(amount, contractToBurnFrom)));
  }

  /**
   * Replaces the large oracle with new oracle nodes selected on the BP Orchestration Contract. Can
   * only be called by the BP Orchestration Contract.
   *
   * @param context execution context
   * @param state current state
   * @param newKeys blockchain public keys of the new oracle nodes
   * @param newAddresses blockchain addresses for the new oracle nodes
   * @param newPublicKey public key for the new large oracle
   * @return updated state with replaced large oracle members
   */
  @Action(Invocations.REPLACE_LARGE_ORACLE)
  public LargeOracleContractState replaceLargeOracle(
      SysContractContext context,
      LargeOracleContractState state,
      List<BlockchainPublicKey> newKeys,
      List<BlockchainAddress> newAddresses,
      BlockchainPublicKey newPublicKey) {
    ensure(
        context.getFrom().equals(state.getBpOrchestrationContract()),
        "Only orchestration contract can replace the large oracle");

    // It is assumed that the BP orchestration contract doesn't provide lists of different
    // lengths here.
    List<OracleMember> newOracle = createOracleMembers(newAddresses, newKeys);

    // By the time we receive this invocation, the new large oracle have already been verified
    // by the BP orchestration contract, so no need to check it here.
    return state.replaceLargeOracle(newOracle, newPublicKey);
  }

  /**
   * Requests a new small oracle. Can only be called by small oracle contracts.
   *
   * @param context execution context
   * @param state current state
   * @param requiredStake stakes required by the contract
   * @param callback callback identifier for contract
   * @param additionalData additional data provided by the contract
   * @return updated state with a pending small oracle update request
   */
  @Action(Invocations.REQUEST_NEW_SMALL_ORACLE)
  public LargeOracleContractState requestNewSmallOracle(
      SysContractContext context,
      LargeOracleContractState state,
      long requiredStake,
      @RpcType(signed = false, size = 1) int callback,
      byte[] additionalData) {
    BlockchainAddress from = context.getFrom();
    ensure(isAllowedOracleContract(from), "Only a small oracle can request a small oracle change");

    LargeByteArray data;
    if (additionalData.length > 0) {
      data = new LargeByteArray(additionalData);
    } else {
      data = null;
    }
    OracleUpdateRequest request = new OracleUpdateRequest(from, callback, requiredStake, data);
    state = state.addPendingOracleUpdateRequest(request);

    return tryFulfillingUpdateRequests(context, state);
  }

  /**
   * Requests a signature from the large oracle on a message. Only callable from the governance
   * update contract.
   *
   * @param context execution context
   * @param state current state
   * @param message message to be signed
   * @return updated state with a pending signature request
   */
  @Action(Invocations.REQUEST_SIGNATURE)
  public LargeOracleContractState requestSignature(
      SysContractContext context, LargeOracleContractState state, Hash message) {
    BlockchainAddress from = context.getFrom();
    ensure(
        from.equals(state.getGovernanceUpdates()),
        "Only governance update can request a signature");

    state = state.addMessageForSigning(message, context.getCurrentTransactionHash());

    return state;
  }

  /**
   * Adds a signature to a pending small oracle update request. Can only be called by large oracle
   * members.
   *
   * @param context execution context
   * @param state current state
   * @param message message to be signed
   * @param nonce index of the message
   * @param signature signature for the message
   * @return updated state with the added signature
   */
  @Action(Invocations.ADD_SIGNATURE)
  public LargeOracleContractState addSignature(
      SysContractContext context,
      LargeOracleContractState state,
      Hash message,
      int nonce,
      Signature signature) {
    ensure(
        state.getOracleMember(context.getFrom()) != null, "Only oracle members can add signatures");

    SigningRequest signingRequest = state.getSigningRequest(nonce, message);
    // ensure signatures are published in the order they were requested
    ensure(state.getPendingMessages().get(0).equals(signingRequest), "Unrecognized message");

    BlockchainPublicKey address = signature.recoverPublicKey(message);
    ensure(address.equals(state.getOracleKey()), "Invalid signer");

    return state.addSignature(signingRequest, signature);
  }

  /**
   * Changes the MPC/USD exchange rate. Can only be called by the voting contract.
   *
   * @param context execution context
   * @param state current state
   * @param numerator numerator of the fraction representing the exchange rate
   * @param denominator denominator of the fraction representing the exchange rate
   * @return update state with the changed exchange rate
   */
  @Action(Invocations.CHANGE_EXCHANGE_RATE)
  public LargeOracleContractState changeExchangeRate(
      SysContractContext context, LargeOracleContractState state, int numerator, int denominator) {
    BlockchainAddress from = context.getFrom();
    ensure(
        from.equals(state.getGovernanceUpdates()),
        "Exchange rate can only be updated by governance updates");
    Fraction exchangeRate = new Fraction(numerator, denominator);
    return state.setMpcToUsdExchangeRate(exchangeRate);
  }

  private LargeOracleContractState burnTokens(
      SysContractContext context,
      LargeOracleContractState state,
      List<BlockchainAddress> oracles,
      long totalTokensToConfiscate) {
    long equalDistribution = totalTokensToConfiscate / oracles.size();
    long rest = totalTokensToConfiscate % oracles.size();
    SystemEventCreator eventManager = context.getInvocationCreator();
    for (int i = 0; i < oracles.size(); i++) {
      BlockchainAddress oracle = oracles.get(i);
      long toBurn = equalDistribution + (i < rest ? 1 : 0);
      burnStakedTokens(eventManager, context.getContractAddress(), oracle, toBurn);
      state = state.burnOracleTokens(oracle, context.getFrom(), toBurn);
    }
    return state;
  }

  /**
   * Recalibrates BYOC twins by adding/creating a BYOC-twin-trade to let users buy MPC tokens for
   * the given amount of BYOC twins. Burns the amount of MPC tokens to sell from the oracles. Only
   * oracle contracts can call recalibrate BYOC twins.
   *
   * @param context execution context
   * @param state current state
   * @param symbol symbol of the BYOC twin
   * @param byocTwinAmount amount of the BYOC twin
   * @param oracles list of small oracles calibrating the BYOC twin
   * @return updated state with recalibrated BYOC twins
   */
  @Action(Invocations.RECALIBRATE_BYOC_TWINS)
  public LargeOracleContractState recalibrateByocTwins(
      SysContractContext context,
      LargeOracleContractState state,
      String symbol,
      Unsigned256 byocTwinAmount,
      List<BlockchainAddress> oracles) {
    BlockchainAddress from = context.getFrom();
    ensure(isAllowedOracleContract(from), "Only a small oracle can recalibrate the BYOC twins");
    long totalTokensToBurn = convertByocTwinsToMpcTokens(state, context, symbol, byocTwinAmount);
    state = burnTokens(context, state, oracles, totalTokensToBurn);
    return state.addByocTwinToMpcDeal(symbol, byocTwinAmount, totalTokensToBurn);
  }

  /**
   * Lets any caller sell their BYOC for MPC tokens. BYOC twin amount is the amount of BYOC they are
   * willing to sell for the expected MPC amount. If there exists a BYOC-twin-trade for the given
   * BYOC twin symbol then the trade happens.
   *
   * @param context execution context
   * @param state current state
   * @param symbol symbol of the BYOC twin
   * @param byocTwinAmountToSell amount of BYOC to sell
   * @param expectedMpcTokensToBuy amount of MPC tokens to buy
   * @return updated state with the given BYOC amount sold for MPC if the expected amount of MPC was
   *     satisfied
   */
  @Action(Invocations.SELL_BYOC_TWINS)
  public LargeOracleContractState sellByocTwins(
      SysContractContext context,
      LargeOracleContractState state,
      String symbol,
      Unsigned256 byocTwinAmountToSell,
      long expectedMpcTokensToBuy) {
    ByocTwinToMpcTradeDeal byocTwinToMpcTradeDeal = state.getByocTwinToMpcTradeDeal(symbol);
    ensure(byocTwinToMpcTradeDeal != null, "No MPC tokens for sale for this BYOC twin");

    Unsigned256 byocTwinsTotal = byocTwinToMpcTradeDeal.getByocTwins();
    long mpcTotal = byocTwinToMpcTradeDeal.getMpcTokens();
    ensure(byocTwinAmountToSell.compareTo(Unsigned256.ZERO) > 0, "Amount to sell must be positive");

    byocTwinAmountToSell = min(byocTwinAmountToSell, byocTwinsTotal);
    long mpcTokensToBuy =
        byocTwinAmountToSell
            .multiply(Unsigned256.create(mpcTotal))
            .divide(byocTwinsTotal)
            .longValueExact();
    ensure(mpcTokensToBuy > 0, "Not enough BYOC twins to buy an MPC token");

    ensure(
        mpcTokensToBuy >= expectedMpcTokensToBuy,
        "The trade will give less than the expected amount of MPC tokens");

    Unsigned256 byocTwinAmountToWithdraw =
        Unsigned256.create(mpcTokensToBuy)
            .multiply(MathUtil.ceilDiv(byocTwinsTotal, Unsigned256.create(mpcTotal)));

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    BlockchainAddress seller = context.getFrom();
    eventManager.registerCallbackWithCostFromRemaining(
        Callbacks.sellByocTwinsCallback(seller, symbol, byocTwinAmountToWithdraw, mpcTokensToBuy));

    LocalPluginStateUpdate accountUpdate =
        LocalPluginStateUpdate.create(
            seller, AccountPluginRpc.deductCoinBalance(symbol, byocTwinAmountToWithdraw));
    eventManager.updateLocalAccountPluginState(accountUpdate);

    return state.deductByocTwinToMpcDeal(symbol, byocTwinAmountToWithdraw, mpcTokensToBuy);
  }

  @SuppressWarnings({"ComparatorResultComparison", "CompareToZero"})
  Unsigned256 min(Unsigned256 left, Unsigned256 right) {
    return left.compareTo(right) == -1 ? left : right;
  }

  /**
   * Associates staked tokens towards the large oracle contract for potentially being selected as a
   * small oracle.
   *
   * @param context execution context
   * @param state current state
   * @param amount amount associated to contract
   * @return updated state with tokens associated to contract
   */
  @Action(Invocations.ASSOCIATE_TOKENS_TO_CONTRACT)
  public LargeOracleContractState associateTokensToContract(
      SysContractContext context, LargeOracleContractState state, long amount) {
    BlockchainAddress account = context.getFrom();

    ensure(amount > 0, "Associated stakes must be positive");

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.registerCallbackWithCostFromRemaining(
        Callbacks.associateCallback(account, amount));

    LocalPluginStateUpdate accountUpdate =
        LocalPluginStateUpdate.create(
            account, AccountPluginRpc.associateTokens(amount, context.getContractAddress()));
    eventManager.updateLocalAccountPluginState(accountUpdate);

    return state;
  }

  /**
   * Associates staked tokens towards the large oracle contract with an expiration for potentially
   * being selected as a small oracle, before the staked tokens expire.
   *
   * @param context execution context
   * @param state current state
   * @param amount amount associated to contract
   * @param expirationTimestamp timestamp when the association is no longer valid
   * @return updated state with tokens associated to contract with an expiration
   */
  @Action(Invocations.ASSOCIATE_TOKENS_TO_CONTRACT_WITH_EXPIRATION)
  public LargeOracleContractState associateTokensToContractWithExpiration(
      SysContractContext context,
      LargeOracleContractState state,
      long amount,
      long expirationTimestamp) {
    BlockchainAddress account = context.getFrom();

    ensure(amount > 0, "Associated stakes must be positive");
    ensure(
        expirationTimestamp > context.getBlockProductionTime(),
        "Expiration timestamp must be set in the future");

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.registerCallbackWithCostFromRemaining(
        Callbacks.associateWithExpirationCallback(account, amount, expirationTimestamp));

    LocalPluginStateUpdate accountUpdate =
        LocalPluginStateUpdate.create(
            account,
            AccountPluginRpc.associateTokensWithExpiration(
                amount, context.getContractAddress(), expirationTimestamp));
    eventManager.updateLocalAccountPluginState(accountUpdate);

    return state;
  }

  /**
   * Set new expiration for association. When the expiration has been reached, the association is no
   * longer valid to be selected as a new small oracle.
   *
   * @param context execution context
   * @param state current state
   * @param expirationTimestamp timestamp when the association is no longer valid
   * @return updated state with new expiration for association
   */
  @Action(Invocations.SET_EXPIRATION_FOR_ASSOCIATION)
  public LargeOracleContractState setExpirationForAssociation(
      SysContractContext context,
      LargeOracleContractState state,
      @RpcType(nullable = true) Long expirationTimestamp) {
    BlockchainAddress account = context.getFrom();

    ensure(
        expirationTimestamp == null || expirationTimestamp >= 0,
        "Expiration timestamp must be non-negative");

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.registerCallbackWithCostFromRemaining(
        Callbacks.setExpirationForAssociation(account, expirationTimestamp));

    LocalPluginStateUpdate accountUpdate =
        LocalPluginStateUpdate.create(
            account,
            AccountPluginRpc.setExpirationForAssociation(
                context.getContractAddress(), expirationTimestamp));
    eventManager.updateLocalAccountPluginState(accountUpdate);

    return state;
  }

  /**
   * Disassociates all expired free tokens from the large oracle contract for an account. Can be
   * invoked by all accounts.
   *
   * @param context execution context
   * @param state current state
   * @param account the account to disassociate tokens for
   * @return updated state with all expired free token disassociated from the large oracle contract
   *     for an account
   */
  @Action(Invocations.DISASSOCIATE_EXPIRED_STAKES_FROM_CONTRACT)
  public LargeOracleContractState disassociateAllExpiredStakesFromContract(
      SysContractContext context, LargeOracleContractState state, BlockchainAddress account) {

    long blockProductionTime = context.getBlockProductionTime();
    StakedTokens tokens = state.getTokensStakedBy(account);
    long amount = tokens.getFreeTokens();
    ensure(amount > 0, "No tokens available to disassociate for account");

    if (tokens.hasExpired(blockProductionTime)) {
      Long expirationTimestamp = tokens.getExpirationTimestamp();
      SystemEventManager eventManager = context.getRemoteCallsCreator();

      eventManager.registerCallbackWithCostFromRemaining(
          Callbacks.disassociateWithExpirationCallback(account, amount, expirationTimestamp));
      LocalPluginStateUpdate accountUpdate =
          LocalPluginStateUpdate.create(
              account, AccountPluginRpc.disassociateTokens(amount, context.getContractAddress()));
      eventManager.updateLocalAccountPluginState(accountUpdate);

      state = state.disassociateTokens(account, amount);
    }

    return state;
  }

  /**
   * Disassociates staked tokens from the large oracle contract.
   *
   * @param context execution context
   * @param state current state
   * @param amount amount disassociated from contract
   * @return updated state with tokens disassociated to contract
   */
  @Action(Invocations.DISASSOCIATE_TOKENS_FROM_CONTRACT)
  public LargeOracleContractState disassociateTokensFromContract(
      SysContractContext context, LargeOracleContractState state, long amount) {
    BlockchainAddress account = context.getFrom();

    ensure(amount > 0, "Cannot disassociate a negative amount");
    long availableForDisassociation = state.getTokensStakedBy(account).getFreeTokens();
    ensure(
        availableForDisassociation >= amount,
        "Cannot disassociate %d tokens when only %d tokens are available to disassociate",
        amount,
        availableForDisassociation);

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    Long expirationTimestamp = state.getTokensStakedBy(account).getExpirationTimestamp();
    if (expirationTimestamp == null) {
      eventManager.registerCallbackWithCostFromRemaining(
          Callbacks.disassociateCallback(account, amount));
    } else {
      eventManager.registerCallbackWithCostFromRemaining(
          Callbacks.disassociateWithExpirationCallback(account, amount, expirationTimestamp));
    }

    LocalPluginStateUpdate accountUpdate =
        LocalPluginStateUpdate.create(
            account, AccountPluginRpc.disassociateTokens(amount, context.getContractAddress()));
    eventManager.updateLocalAccountPluginState(accountUpdate);

    return state.disassociateTokens(account, amount);
  }

  /**
   * Locks staked tokens towards the price oracle contract. Can only be called by the price oracle
   * contract.
   *
   * @param context execution context
   * @param state current state
   * @param tokensToLock amount of tokens locked
   * @param priceOracleNodeAddress account that gets their tokens locked
   * @return updated state with tokens locked to price oracle.
   */
  @Action(Invocations.LOCK_TOKENS_TO_PRICE_ORACLE)
  public LargeOracleContractState lockTokensToPriceOracle(
      SysContractContext context,
      LargeOracleContractState state,
      long tokensToLock,
      BlockchainAddress priceOracleNodeAddress) {
    final BlockchainAddress priceOracleContractAddress = context.getFrom();
    ensure(
        isAllowedOracleContract(priceOracleContractAddress),
        "Only the price oracle contract can lock tokens.");
    return state.lockTokensToOracle(
        priceOracleNodeAddress,
        priceOracleContractAddress,
        tokensToLock,
        context.getBlockProductionTime());
  }

  /**
   * Unlocks all tokens from the price oracle contract for an account.
   *
   * @param context execution context
   * @param state current state
   * @param tokensToUnlock amount of tokens unlocked
   * @param priceOracleNodeAddress account that gets their tokens unlocked
   * @return updated state with tokens unlocked from price oracle
   */
  @Action(Invocations.UNLOCK_TOKENS_FROM_PRICE_ORACLE)
  public LargeOracleContractState unlockTokensFromPriceOracle(
      SysContractContext context,
      LargeOracleContractState state,
      long tokensToUnlock,
      BlockchainAddress priceOracleNodeAddress) {
    final BlockchainAddress priceOracleContractAddress = context.getFrom();
    ensure(
        isAllowedOracleContract(priceOracleContractAddress),
        "Only the price oracle contract can unlock tokens.");
    return state.unlockTokensFromOracle(
        priceOracleNodeAddress, priceOracleContractAddress, tokensToUnlock);
  }

  /**
   * Creates a poll for a dispute in a small oracle contract to be voted on by the large oracle
   * members. Throws an error if an insufficient tokens have been staked. Can only be called by
   * small oracle contracts.
   *
   * @param context execution context
   * @param state current state
   * @param disputeChallenger the dispute challenger
   * @param disputeTokenCost token cost of dispute
   * @param oracleId id of the oracle that this dispute relates to
   * @param disputeId id for the dispute
   * @param smallOraclesResultInvocation invocation to call when a voting result has been reached
   * @param smallOraclesCounterClaimInvocation invocation to call for adding counter-claims to the
   *     dispute
   * @return updated state with a dispute poll created
   */
  @Action(Invocations.CREATE_DISPUTE_POLL_WITH_ERROR_THROW)
  public LargeOracleContractState createDisputePollWithErrorThrow(
      SysContractContext context,
      LargeOracleContractState state,
      BlockchainAddress disputeChallenger,
      long disputeTokenCost,
      long oracleId,
      long disputeId,
      @RpcType(signed = false, size = 1) int smallOraclesResultInvocation,
      @RpcType(signed = false, size = 1) int smallOraclesCounterClaimInvocation) {
    BlockchainAddress from = context.getFrom();
    ensure(
        isAllowedOracleContract(from),
        "Disputes can only be created by one of the small oracle contracts");
    long allocatableTokens = state.getTokensStakedBy(disputeChallenger).getAllocatableTokens();

    ensure(
        allocatableTokens >= disputeTokenCost,
        "{} has insufficient staked tokens to initiate a dispute poll");
    boolean hasExpired =
        state.getTokensStakedBy(disputeChallenger).hasExpired(context.getBlockProductionTime());
    ensure(
        !hasExpired, "Tokens associated to contract have expired, and cannot be locked to oracle");

    return createDisputePoll(
        disputeChallenger,
        smallOraclesResultInvocation,
        smallOraclesCounterClaimInvocation,
        state,
        oracleId,
        disputeId,
        from,
        disputeTokenCost,
        context.getBlockProductionTime());
  }

  /**
   * Unlocks all tokens that have been pending release from a small oracle for more than 14 days,
   * and checks if a new small oracle should be selected.
   *
   * @param context execution context
   * @param state current state
   * @param oracleContract the address of the oracle
   * @return updated state with the pending tokens freed from the oracle and potentially a new small
   *     oracle.
   */
  @Action(Invocations.UNLOCK_OLD_PENDING_TOKENS)
  public LargeOracleContractState unlockOldPendingTokens(
      SysContractContext context,
      LargeOracleContractState state,
      BlockchainAddress oracleContract) {
    state = state.clearPendingOracleStakes(oracleContract, context.getBlockProductionTime());
    return tryFulfillingUpdateRequests(context, state);
  }

  /**
   * Checks the amount of signed messages. If more than {@link
   * LargeOracleContract#NUMBER_OF_SIGNATURES_PER_COMMITTEE} signatures has been requested, trigger
   * invocation TRIGGER_NEW_COMMITEE on bp orchestration contract.
   *
   * @param context execution context
   * @param state current state
   * @return unchanged state
   */
  @Action(Invocations.CHECK_SIGNATURE_COUNT)
  public LargeOracleContractState checkSignatureCount(
      SysContractContext context, LargeOracleContractState state) {
    int signedMessageCount = state.numberOfSignaturesRequestedFromCurrentCommittee();
    ensure(
        signedMessageCount >= NUMBER_OF_SIGNATURES_PER_COMMITTEE,
        "Current committee has not run out of signatures");
    context
        .getInvocationCreator()
        .invoke(state.getBpOrchestrationContract())
        .withPayload(BpOrchestrationRpc.triggerNewCommittee())
        .sendFromContract();
    return state;
  }

  /**
   * Check if the given Blockchain addresses are all part of the current large oracle. Will return
   * false if any of the given blockchain addresses is found to not be part of the current large
   * oracle.
   *
   * @param context execution context
   * @param state current state
   * @param oracleMembers blockchain addresses to check
   * @return unchanged state
   */
  @Action(Invocations.CHECK_ORACLE_MEMBER_STATUSES)
  public LargeOracleContractState checkOracleMemberStatus(
      SysContractContext context,
      LargeOracleContractState state,
      List<BlockchainAddress> oracleMembers) {
    ensure(!oracleMembers.isEmpty(), "No blockchain addresses were supplied");
    Set<BlockchainAddress> largeOracleMemberAddresses =
        state.getOracleMembers().stream().map(OracleMember::address).collect(Collectors.toSet());
    context.setResult(s -> s.writeBoolean(largeOracleMemberAddresses.containsAll(oracleMembers)));
    return state;
  }

  /**
   * Set the amount of reserved tokens.
   *
   * @param context context
   * @param state state
   * @param newReserved amount of reserved tokens
   * @return updated state
   */
  @Action(Invocations.SET_RESERVED_TOKENS)
  public LargeOracleContractState setReservedTokens(
      SysContractContext context, LargeOracleContractState state, long newReserved) {
    ensure(newReserved >= 0, "Amount of reserved tokens has to be non-negative: %d", newReserved);
    BlockchainAddress account = context.getFrom();
    return state.setReservedTokens(account, newReserved);
  }

  static void writeKeys(SafeDataOutputStream output, List<BlockchainPublicKey> keys) {
    for (BlockchainPublicKey key : keys) {
      output.write(encodeUncompressed(key));
    }
  }

  private static byte[] encodeUncompressed(BlockchainPublicKey pubKey) {
    byte[] uncompressedKey = pubKey.getEcPointBytes();
    return Arrays.copyOfRange(uncompressedKey, 1, uncompressedKey.length);
  }

  /**
   * Callback for associating tokens to the large oracle contract.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param account account to associate for
   * @param amount amount of tokens associated
   * @return updated state
   */
  @Callback(Callbacks.ASSOCIATE_TOKENS_CALLBACK)
  public LargeOracleContractState associateTokenCallback(
      SysContractContext context,
      LargeOracleContractState state,
      CallbackContext callbackContext,
      BlockchainAddress account,
      long amount) {
    ensure(callbackContext.isSuccess(), "Failed to associate %d tokens for %s", amount, account);

    LargeOracleContractState updatedState = state.stakeTokens(account, amount, null);

    return tryFulfillingUpdateRequests(context, updatedState);
  }

  /**
   * Callback for associating tokens with expiration to the large oracle contract.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param account account to associate for
   * @param amount amount of tokens associated
   * @param expirationTimestamp when the association expires
   * @return updated state
   */
  @Callback(Callbacks.ASSOCIATE_TOKENS_WITH_EXPIRATION_CALLBACK)
  public LargeOracleContractState associateTokenWithExpirationCallback(
      SysContractContext context,
      LargeOracleContractState state,
      CallbackContext callbackContext,
      BlockchainAddress account,
      long amount,
      long expirationTimestamp) {
    ensure(
        callbackContext.isSuccess(),
        "Failed to associate %d tokens that expires at %d for %s",
        amount,
        expirationTimestamp,
        account);

    LargeOracleContractState updatedState = state.stakeTokens(account, amount, expirationTimestamp);

    return tryFulfillingUpdateRequests(context, updatedState);
  }

  /**
   * Callback for setting the expiration for an association to the large oracle contract.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param account account to associate for
   * @param expirationTimestamp when the association expires
   * @return updated state
   */
  @Callback(Callbacks.SET_EXPIRATION_FOR_ASSOCIATION_CALLBACK)
  public LargeOracleContractState setExpirationForAssociationCallback(
      SysContractContext context,
      LargeOracleContractState state,
      CallbackContext callbackContext,
      BlockchainAddress account,
      @RpcType(nullable = true) Long expirationTimestamp) {
    ensure(
        callbackContext.isSuccess(),
        "Failed to set expiration to %d for association",
        expirationTimestamp);

    LargeOracleContractState updatedState =
        state.setExpirationForAssociation(account, expirationTimestamp);

    return tryFulfillingUpdateRequests(context, updatedState);
  }

  private static LargeOracleContractState tryFulfillingUpdateRequests(
      SysContractContext context, LargeOracleContractState initialState) {

    LargeOracleContractState state = initialState;
    while (state.getPendingUpdateRequest() != null) {
      OracleUpdateRequest request = state.getPendingUpdateRequest();

      final BlockchainAddress oracleContract = request.getContractAddress();
      final long requiredStake = request.getRequiredStake();
      final int callback = request.getCallback();
      final byte[] additionalData = request.getAdditionalData();

      long blockProductionTime = context.getBlockProductionTime();
      state = releasePreviousOracle(state, oracleContract, blockProductionTime);

      // pick three nodes that have enough stake to act as oracles.
      int oracleSize = 3;
      List<OracleMember> members =
          state.findOracleNodes(
              requiredStake,
              oracleSize,
              getStableHashCode(context.getCurrentTransactionHash()),
              blockProductionTime);
      if (members.isEmpty()) {
        // Unable to fulfill request
        return state;
      }

      state = state.removePendingOracleUpdateRequest();

      List<BlockchainAddress> addresses =
          members.stream().map(OracleMember::address).collect(Collectors.toList());
      List<BlockchainPublicKey> keys =
          members.stream().map(OracleMember::key).collect(Collectors.toList());
      for (BlockchainAddress account : addresses) {
        state =
            state.lockTokensToOracle(account, oracleContract, requiredStake, blockProductionTime);
      }
      context
          .getInvocationCreator()
          .invoke(oracleContract)
          .withPayload(SmallOracleRpc.updateRequest(callback, addresses, keys))
          .sendFromContract();
      // If there is no additional data a signature is not needed.
      if (additionalData != null) {
        Hash message =
            Hash.create(
                s -> {
                  s.write(additionalData);
                  writeKeys(s, keys);
                });
        state = state.addMessageForSigning(message, context.getCurrentTransactionHash());
      }
    }

    return state;
  }

  private static LargeOracleContractState releasePreviousOracle(
      LargeOracleContractState state, BlockchainAddress oracleContract, long blockProductionTime) {
    List<BlockchainAddress> oldOracleMembers = state.getMembersOfOldOracle(oracleContract);
    for (BlockchainAddress oldOracleMember : oldOracleMembers) {
      state = state.releaseTokensFromOracle(oldOracleMember, oracleContract, blockProductionTime);
    }
    return state.clearPendingOracleStakes(oracleContract, blockProductionTime);
  }

  /**
   * Callback for disassociating tokens from the large oracle contract.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param account account to disassociate for
   * @param amount amount of tokens disassociated
   * @return updated state
   */
  @Callback(Callbacks.DISASSOCIATE_TOKENS_CALLBACK)
  public LargeOracleContractState disassociateTokenCallback(
      SysContractContext context,
      LargeOracleContractState state,
      CallbackContext callbackContext,
      BlockchainAddress account,
      long amount) {
    if (!callbackContext.isSuccess()) {
      // Account plugin failed, re-associate the tokens
      return state.stakeTokens(account, amount, null);
    } else {
      return state;
    }
  }

  /**
   * Callback for disassociating tokens with expiration from the large oracle contract.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param account account to disassociate for
   * @param amount amount of tokens disassociated
   * @param expirationTimestamp timestamp when the association expires
   * @return updated state
   */
  @Callback(Callbacks.DISASSOCIATE_TOKENS_WITH_EXPIRATION_CALLBACK)
  public LargeOracleContractState disassociateTokenWithExpirationCallback(
      SysContractContext context,
      LargeOracleContractState state,
      CallbackContext callbackContext,
      BlockchainAddress account,
      long amount,
      long expirationTimestamp) {
    if (!callbackContext.isSuccess()) {
      // Account plugin failed, re-associate the tokens
      return state.stakeTokens(account, amount, expirationTimestamp);
    } else {
      return state;
    }
  }

  /**
   * Callback for selling BYOC twins.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param seller seller of the BYOC twins
   * @param symbol coin symbol
   * @param byocTwinsAmountToSell amount of in BYOC
   * @param mpcTokensToBuy amount in MPC tokens
   * @return updated state
   */
  @Callback(Callbacks.SELL_BYOC_TWINS_CALLBACK)
  public LargeOracleContractState sellByocTwinsCallback(
      SysContractContext context,
      LargeOracleContractState state,
      CallbackContext callbackContext,
      BlockchainAddress seller,
      String symbol,
      Unsigned256 byocTwinsAmountToSell,
      long mpcTokensToBuy) {

    if (!callbackContext.isSuccess()) {
      logger.info("Unable to withdraw {} {} from {}", byocTwinsAmountToSell, symbol, seller);
      return state.addByocTwinToMpcDeal(symbol, byocTwinsAmountToSell, mpcTokensToBuy);
    }

    SystemEventCreator eventManager = context.getInvocationCreator();
    LocalPluginStateUpdate accountUpdate =
        LocalPluginStateUpdate.create(seller, AccountPluginRpc.addTokenBalance(mpcTokensToBuy));
    eventManager.updateLocalAccountPluginState(accountUpdate);

    return state;
  }

  /** Invocation shortnames. */
  static final class Invocations {

    static final int CREATE_DISPUTE_POLL = 0;
    static final int ADD_DISPUTE_COUNTER_CLAIM = 1;
    static final int VOTE_ON_DISPUTE = 2;
    static final int BURN_STAKED_TOKENS = 3;
    static final int REPLACE_LARGE_ORACLE = 4;
    static final int REQUEST_NEW_SMALL_ORACLE = 5;
    static final int ADD_SIGNATURE = 6;
    static final int CHANGE_EXCHANGE_RATE = 7;
    static final int RECALIBRATE_BYOC_TWINS = 8;
    static final int SELL_BYOC_TWINS = 9;
    static final int ASSOCIATE_TOKENS_TO_CONTRACT = 10;
    static final int DISASSOCIATE_TOKENS_FROM_CONTRACT = 11;
    static final int LOCK_TOKENS_TO_PRICE_ORACLE = 12;
    static final int UNLOCK_TOKENS_FROM_PRICE_ORACLE = 13;
    static final int CREATE_DISPUTE_POLL_WITH_ERROR_THROW = 14;
    static final int UNLOCK_OLD_PENDING_TOKENS = 15;
    static final int SET_RESERVED_TOKENS = 16;
    static final int REQUEST_SIGNATURE = 17;
    static final int CHECK_SIGNATURE_COUNT = 18;
    static final int CHECK_ORACLE_MEMBER_STATUSES = 19;
    static final int ASSOCIATE_TOKENS_TO_CONTRACT_WITH_EXPIRATION = 20;
    static final int DISASSOCIATE_EXPIRED_STAKES_FROM_CONTRACT = 21;
    static final int SET_EXPIRATION_FOR_ASSOCIATION = 22;

    private Invocations() {}
  }

  /** Callback shortnames. */
  static final class Callbacks {

    static final int ASSOCIATE_TOKENS_CALLBACK = 0;
    static final int DISASSOCIATE_TOKENS_CALLBACK = 1;
    static final int SELL_BYOC_TWINS_CALLBACK = 2;
    static final int ASSOCIATE_TOKENS_WITH_EXPIRATION_CALLBACK = 3;
    static final int DISASSOCIATE_TOKENS_WITH_EXPIRATION_CALLBACK = 4;
    static final int SET_EXPIRATION_FOR_ASSOCIATION_CALLBACK = 5;

    private Callbacks() {}

    static DataStreamSerializable associateCallback(BlockchainAddress account, long amount) {
      return callback -> {
        callback.writeByte(LargeOracleContract.Callbacks.ASSOCIATE_TOKENS_CALLBACK);
        account.write(callback);
        callback.writeLong(amount);
      };
    }

    static DataStreamSerializable associateWithExpirationCallback(
        BlockchainAddress account, long amount, long expirationTimestamp) {
      return callback -> {
        callback.writeByte(LargeOracleContract.Callbacks.ASSOCIATE_TOKENS_WITH_EXPIRATION_CALLBACK);
        account.write(callback);
        callback.writeLong(amount);
        callback.writeLong(expirationTimestamp);
      };
    }

    static DataStreamSerializable setExpirationForAssociation(
        BlockchainAddress account, Long expirationTimestamp) {
      return callback -> {
        callback.writeByte(LargeOracleContract.Callbacks.SET_EXPIRATION_FOR_ASSOCIATION_CALLBACK);
        account.write(callback);
        callback.writeOptional((value, rpc) -> rpc.writeLong(value), expirationTimestamp);
      };
    }

    static DataStreamSerializable disassociateCallback(BlockchainAddress account, long amount) {
      return callback -> {
        callback.writeByte(LargeOracleContract.Callbacks.DISASSOCIATE_TOKENS_CALLBACK);
        account.write(callback);
        callback.writeLong(amount);
      };
    }

    static DataStreamSerializable disassociateWithExpirationCallback(
        BlockchainAddress account, long amount, long expirationTimestamp) {
      return callback -> {
        callback.writeByte(
            LargeOracleContract.Callbacks.DISASSOCIATE_TOKENS_WITH_EXPIRATION_CALLBACK);
        account.write(callback);
        callback.writeLong(amount);
        callback.writeLong(expirationTimestamp);
      };
    }

    static DataStreamSerializable sellByocTwinsCallback(
        BlockchainAddress seller, String symbol, Unsigned256 withdrawAmount, long mpcTokensToBuy) {
      return callback -> {
        callback.writeByte(LargeOracleContract.Callbacks.SELL_BYOC_TWINS_CALLBACK);
        seller.write(callback);
        callback.writeString(symbol);
        withdrawAmount.write(callback);
        callback.writeLong(mpcTokensToBuy);
      };
    }
  }

  private static int getStableHashCode(Hash currentTransactionHash) {
    return Arrays.hashCode(currentTransactionHash.getBytes());
  }

  @FormatMethod
  private static void ensure(boolean predicate, String messageFormat, Object... args) {
    if (!predicate) {
      String errorMessage = String.format(messageFormat, args);
      throw new RuntimeException(errorMessage);
    }
  }

  static List<OracleMember> createOracleMembers(
      List<BlockchainAddress> addresses, List<BlockchainPublicKey> keys) {
    List<OracleMember> members = new ArrayList<>();
    for (int i = 0; i < addresses.size(); i++) {
      members.add(new OracleMember(addresses.get(i), keys.get(i)));
    }
    return members;
  }
}
