package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.Objects;

/** Id to determine a dispute. */
@Immutable
public final class OracleDisputeId implements StateSerializable, Comparable<OracleDisputeId> {
  private final BlockchainAddress smallOracle;
  private final long oracleId;
  private final long disputeId;

  @SuppressWarnings("unused")
  OracleDisputeId() {
    this.smallOracle = null;
    this.oracleId = 0;
    this.disputeId = 0;
  }

  /**
   * Create oracle dispute id.
   *
   * @param smallOracle small oracle address
   * @param oracleId id of the oracle that this dispute relates to
   * @param disputeId dispute id
   */
  public OracleDisputeId(BlockchainAddress smallOracle, long oracleId, long disputeId) {
    this.smallOracle = smallOracle;
    this.oracleId = oracleId;
    this.disputeId = disputeId;
  }

  static OracleDisputeId createFromStateAccessor(StateAccessor accessor) {
    return new OracleDisputeId(
        accessor.get("smallOracle").blockchainAddressValue(),
        accessor.get("oracleId").longValue(),
        accessor.get("disputeId").longValue());
  }

  /**
   * Get the small oracle address.
   *
   * @return small oracle address
   */
  public BlockchainAddress getSmallOracle() {
    return smallOracle;
  }

  /**
   * Get the dispute id.
   *
   * @return dispute id
   */
  public long getDisputeId() {
    return disputeId;
  }

  /**
   * Get the oracle nonce.
   *
   * @return oracle nonce
   */
  public long getOracleId() {
    return oracleId;
  }

  @Override
  public int compareTo(OracleDisputeId o) {
    int addressCompare = Objects.requireNonNull(smallOracle).compareTo(o.getSmallOracle());
    if (addressCompare != 0) {
      return addressCompare;
    }
    int oracleCompare = Long.compare(oracleId, o.getOracleId());
    if (oracleCompare != 0) {
      return oracleCompare;
    }
    return Long.compare(disputeId, o.getDisputeId());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof OracleDisputeId that)) {
      return false;
    }
    return oracleId == that.oracleId
        && disputeId == that.disputeId
        && Objects.equals(smallOracle, that.smallOracle);
  }

  @Override
  public int hashCode() {
    return Objects.hash(smallOracle, oracleId, disputeId);
  }
}
