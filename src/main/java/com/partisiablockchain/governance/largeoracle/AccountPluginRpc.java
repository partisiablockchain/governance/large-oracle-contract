package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataOutputStream;

final class AccountPluginRpc {

  static final int DEDUCT_COIN_BALANCE = 1;
  static final int ADD_MPC_TOKEN_BALANCE = 2;
  static final byte ASSOCIATE_TOKENS_TO_CONTRACT = 33;
  static final byte DISASSOCIATE_TOKENS_FROM_CONTRACT = 34;
  static final byte BURN_MPC_TOKENS = 10;
  static final byte ASSOCIATE_TO_CONTRACT_WITH_EXPIRATION = 78;
  static final byte SET_EXPIRATION_FOR_ASSOCIATION = 80;

  @SuppressWarnings("unused")
  private AccountPluginRpc() {}

  static byte[] deductCoinBalance(String symbol, Unsigned256 amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(DEDUCT_COIN_BALANCE);
          stream.writeString(symbol);
          amount.write(stream);
        });
  }

  static byte[] addTokenBalance(long amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ADD_MPC_TOKEN_BALANCE);
          stream.writeLong(amount);
        });
  }

  static byte[] associateTokens(long amount, BlockchainAddress contractAddress) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ASSOCIATE_TOKENS_TO_CONTRACT);
          stream.writeLong(amount);
          contractAddress.write(stream);
        });
  }

  static byte[] associateTokensWithExpiration(
      long amount, BlockchainAddress contractAddress, long expirationTimestamp) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ASSOCIATE_TO_CONTRACT_WITH_EXPIRATION);
          stream.writeLong(amount);
          contractAddress.write(stream);
          stream.writeLong(expirationTimestamp);
        });
  }

  static byte[] setExpirationForAssociation(
      BlockchainAddress contractAddress, Long expirationTimestamp) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(SET_EXPIRATION_FOR_ASSOCIATION);
          contractAddress.write(stream);
          stream.writeOptional((value, rpc) -> rpc.writeLong(value), expirationTimestamp);
        });
  }

  static byte[] disassociateTokens(long amount, BlockchainAddress contractAddress) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(DISASSOCIATE_TOKENS_FROM_CONTRACT);
          stream.writeLong(amount);
          contractAddress.write(stream);
        });
  }

  static byte[] burnStakedTokens(long amount, BlockchainAddress contractAddress) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(BURN_MPC_TOKENS);
          stream.writeLong(amount);
          contractAddress.write(stream);
        });
  }
}
