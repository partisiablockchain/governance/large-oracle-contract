package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/** Representation of a request to update an oracle. */
@Immutable
public final class OracleUpdateRequest implements StateSerializable {

  private final BlockchainAddress contractAddress;
  private final int callback;
  private final long requiredStake;
  private final LargeByteArray additionalData;

  @SuppressWarnings("unused")
  OracleUpdateRequest() {
    this.contractAddress = null;
    this.callback = 0;
    this.requiredStake = 0;
    this.additionalData = null;
  }

  /**
   * Default constructor for an OracleUpdateRequest.
   *
   * @param contractAddress the small oracle contract which has sent the request
   * @param callback the callback identifier for contract
   * @param requiredStake the stakes required by the contract
   * @param additionalData additional data provided by the contract
   */
  public OracleUpdateRequest(
      BlockchainAddress contractAddress,
      int callback,
      long requiredStake,
      LargeByteArray additionalData) {
    this.contractAddress = contractAddress;
    this.callback = callback;
    this.requiredStake = requiredStake;
    this.additionalData = additionalData;
  }

  static OracleUpdateRequest createFromStateAccessor(StateAccessor accessor) {
    return new OracleUpdateRequest(
        accessor.get("contractAddress").blockchainAddressValue(),
        accessor.get("callback").intValue(),
        accessor.get("requiredStake").longValue(),
        accessor.get("additionalData").typedValue(LargeByteArray.class));
  }

  /**
   * Get the address of the small oracle contract which has sent this request.
   *
   * @return {@link BlockchainAddress} of the small oracle contract
   */
  public BlockchainAddress getContractAddress() {
    return contractAddress;
  }

  /**
   * Get the callback identifier of the small oracle contract.
   *
   * @return a callback identifier
   */
  public int getCallback() {
    return callback;
  }

  /**
   * Get the stakes required to be an oracle member for the small oracle.
   *
   * @return the amount of tokens that must be staked
   */
  public long getRequiredStake() {
    return requiredStake;
  }

  /**
   * Get the optional additional data provided by the small oracle contract. May be null.
   *
   * @return the additional data or null
   */
  public byte[] getAdditionalData() {
    if (additionalData != null) {
      return additionalData.getData();
    } else {
      return null;
    }
  }
}
