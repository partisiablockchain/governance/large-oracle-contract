package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.Objects;

/** Container for a message hash, a nonce and the transaction hash. */
@Immutable
public final class SigningRequest implements StateSerializable, Comparable<SigningRequest> {

  private final Integer nonce;
  private final Hash messageHash;
  private final Hash transactionHash;

  /** Serializable constructor. */
  @SuppressWarnings("unused")
  public SigningRequest() {
    this.nonce = null;
    this.messageHash = null;
    this.transactionHash = null;
  }

  /**
   * Create hash and nonce.
   *
   * @param nonce the nonce
   * @param messageHash the message hash
   * @param transactionHash the random hash
   */
  public SigningRequest(Integer nonce, Hash messageHash, Hash transactionHash) {
    this.nonce = nonce;
    this.messageHash = messageHash;
    this.transactionHash = transactionHash;
  }

  static SigningRequest createFromStateAccessor(StateAccessor accessor) {
    return new SigningRequest(
        accessor.get("nonce").intValue(),
        accessor.get("messageHash").hashValue(),
        accessor.get("transactionHash").hashValue());
  }

  /**
   * Get the nonce.
   *
   * @return nonce
   */
  public Integer getNonce() {
    return Objects.requireNonNull(nonce);
  }

  /**
   * Get the hash.
   *
   * @return hash
   */
  public Hash getMessageHash() {
    return Objects.requireNonNull(messageHash);
  }

  /**
   * Get the random hash.
   *
   * @return hash
   */
  public Hash getTransactionHash() {
    return transactionHash;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SigningRequest that = (SigningRequest) o;
    return Objects.equals(nonce, that.nonce)
        && Objects.equals(messageHash, that.messageHash)
        && Objects.equals(transactionHash, that.transactionHash);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nonce, messageHash, transactionHash);
  }

  @Override
  public int compareTo(SigningRequest other) {
    int nonceCompare = other.getNonce().compareTo(getNonce());
    if (nonceCompare == 0) {
      int hashCompare = other.getMessageHash().compareTo(getMessageHash());
      if (hashCompare == 0) {
        if (getTransactionHash() == null && other.getTransactionHash() != null) {
          return -1;
        }
        if (getTransactionHash() == null) {
          return 0;
        }
        return other.getTransactionHash().compareTo(getTransactionHash());
      }
      return hashCompare;
    }
    return nonceCompare;
  }
}
