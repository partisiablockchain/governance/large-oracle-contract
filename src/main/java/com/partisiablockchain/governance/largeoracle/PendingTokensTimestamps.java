package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;

/**
 * This record keeps track of the releases of locked tokens from a small oracle through a mapping
 * from the timestamp of when the tokens were released (i.e. set in pending) to the amount of tokens
 * released.
 *
 * @param timestamps the map of timestamps to amount of tokens
 */
@Immutable
public record PendingTokensTimestamps(AvlTree<Long, Long> timestamps) implements StateSerializable {
  static PendingTokensTimestamps createFromStateAccessor(StateAccessor pendingTokensTimestamps) {
    AvlTree<Long, Long> timestamps = AvlTree.create();
    for (StateAccessorAvlLeafNode pendingTokens : pendingTokensTimestamps.getTreeLeaves()) {
      timestamps =
          timestamps.set(pendingTokens.getKey().longValue(), pendingTokens.getValue().longValue());
    }
    return new PendingTokensTimestamps(timestamps);
  }
}
