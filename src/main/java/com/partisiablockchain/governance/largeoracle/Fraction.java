package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/** Class representing a fraction. */
@Immutable
public final class Fraction implements StateSerializable {
  private final long numerator;
  private final long denominator;

  static Fraction createFromStateAccessor(StateAccessor accessor) {
    if (accessor.isNull()) {
      return null;
    } else {
      return new Fraction(
          accessor.get("numerator").longValue(), accessor.get("denominator").longValue());
    }
  }

  /** Serializable constructor. */
  @SuppressWarnings("unused")
  public Fraction() {
    this.numerator = 0;
    this.denominator = 0;
  }

  /**
   * Create fraction.
   *
   * @param numerator the numerator
   * @param denominator the denominator
   */
  public Fraction(long numerator, long denominator) {
    this.numerator = numerator;
    this.denominator = denominator;
  }

  /**
   * Get the numerator.
   *
   * @return numerator
   */
  public long getNumerator() {
    return numerator;
  }

  /**
   * Get the denominator.
   *
   * @return denominator
   */
  public long getDenominator() {
    return denominator;
  }
}
