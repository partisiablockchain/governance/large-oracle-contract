package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import java.util.Objects;

/** MPC tokens staked and associated to this contract by an account. */
@Immutable
public final class StakedTokens implements StateSerializable {

  private final long freeTokens;
  private final long reservedTokens;
  private final AvlTree<BlockchainAddress, Long> lockedToOracle;
  private final AvlTree<BlockchainAddress, PendingTokensTimestamps> pendingTokens;
  private final AvlTree<OracleDisputeId, Long> lockedToDispute;
  private final Long expirationTimestamp;

  static StakedTokens createFromStateAccessor(StateAccessor accessor) {
    final AvlTree<BlockchainAddress, Long> lockedOracleMap =
        accessor.get("lockedToOracle").typedAvlTree(BlockchainAddress.class, Long.class);
    AvlTree<BlockchainAddress, PendingTokensTimestamps> pendingTokensMap = AvlTree.create();
    for (StateAccessorAvlLeafNode oracleTree : accessor.get("pendingTokens").getTreeLeaves()) {
      PendingTokensTimestamps pendingTokensTimestamps =
          PendingTokensTimestamps.createFromStateAccessor(oracleTree.getValue().get("timestamps"));
      pendingTokensMap =
          pendingTokensMap.set(
              oracleTree.getKey().blockchainAddressValue(), pendingTokensTimestamps);
    }
    AvlTree<OracleDisputeId, Long> lockedToDisputeMap = AvlTree.create();
    for (StateAccessorAvlLeafNode entry : accessor.get("lockedToDispute").getTreeLeaves()) {
      OracleDisputeId oracleDisputeId = OracleDisputeId.createFromStateAccessor(entry.getKey());
      lockedToDisputeMap = lockedToDisputeMap.set(oracleDisputeId, entry.getValue().longValue());
    }
    long freeTokens = accessor.get("freeTokens").longValue();
    long reservedTokens = accessor.get("reservedTokens").longValue();

    Long expirationTimestamp = null;
    if (accessor.hasField("expirationTimestamp")) {
      expirationTimestamp = accessor.get("expirationTimestamp").cast(Long.class);
    }

    return new StakedTokens(
        freeTokens,
        reservedTokens,
        lockedOracleMap,
        pendingTokensMap,
        lockedToDisputeMap,
        expirationTimestamp);
  }

  @SuppressWarnings("unused")
  StakedTokens() {
    freeTokens = 0L;
    reservedTokens = 0L;
    lockedToOracle = null;
    pendingTokens = null;
    lockedToDispute = null;
    expirationTimestamp = null;
  }

  StakedTokens(
      long freeTokens,
      long reservedTokens,
      AvlTree<BlockchainAddress, Long> lockedToOracle,
      AvlTree<BlockchainAddress, PendingTokensTimestamps> pendingTokens,
      AvlTree<OracleDisputeId, Long> lockedToDispute,
      Long expirationTimestamp) {
    this.freeTokens = freeTokens;
    this.reservedTokens = reservedTokens;
    this.lockedToOracle = lockedToOracle;
    this.pendingTokens = pendingTokens;
    this.lockedToDispute = lockedToDispute;
    this.expirationTimestamp = expirationTimestamp;
  }

  /**
   * Create empty staked tokens.
   *
   * @return staked tokens
   */
  public static StakedTokens create() {
    return new StakedTokens(0L, 0L, AvlTree.create(), AvlTree.create(), AvlTree.create(), null);
  }

  /**
   * Get tokens available for allocating to new oracle work.
   *
   * @return tokens available for allocation
   */
  public long getAllocatableTokens() {
    return Math.max(0, freeTokens - reservedTokens);
  }

  /**
   * Checks if the staked tokens have expired for a contract for a given timestamp.
   *
   * @param timestamp the timestamp to check against
   * @return if the staked tokens have expired
   */
  public boolean hasExpired(long timestamp) {
    if (this.expirationTimestamp == null) {
      return false;
    }

    return timestamp > this.expirationTimestamp;
  }

  /**
   * Get free tokens.
   *
   * @return free tokens
   */
  public long getFreeTokens() {
    return freeTokens;
  }

  /**
   * Get tokens locked to oracle.
   *
   * @param oracleAddress address of oracle
   * @return tokens locked to oracle
   */
  public long getLockedToOracle(BlockchainAddress oracleAddress) {
    return Objects.requireNonNullElse(lockedToOracle.getValue(oracleAddress), 0L);
  }

  /**
   * Get tokens in pending state.
   *
   * @param oracleAddress address of oracle
   * @return tokens in pending state
   */
  public long getPendingForOracle(BlockchainAddress oracleAddress) {
    long pendingTokensForOracle = 0L;
    AvlTree<Long, Long> pendingTokensTimestampsForOracle = getTimestamps(oracleAddress);
    for (Long amount : pendingTokensTimestampsForOracle.values()) {
      pendingTokensForOracle += amount;
    }
    return pendingTokensForOracle;
  }

  /**
   * Clear tokens from a pending state that have been pending for longer than the supplied
   * timestamp, adding them to the free tokens.
   *
   * @param oracle address of the oracle the tokens have been locked to
   * @param timestamp the timestamp to compare the pending tokens against
   * @return new state with new free tokens
   */
  public StakedTokens clearPendingForOracleOlderThan(BlockchainAddress oracle, Long timestamp) {
    AvlTree<Long, Long> oracleTimestamps = getTimestamps(oracle);
    long clearedPendingTokens = 0L;
    for (Long pendingTimestamp : oracleTimestamps.keySet()) {
      if (timestamp > pendingTimestamp) {
        clearedPendingTokens += oracleTimestamps.getValue(pendingTimestamp);
        oracleTimestamps = oracleTimestamps.remove(pendingTimestamp);
      }
    }
    return new StakedTokens(
        freeTokens + clearedPendingTokens,
        reservedTokens,
        lockedToOracle,
        updatePendingTokensTimestamps(oracle, oracleTimestamps),
        lockedToDispute,
        expirationTimestamp);
  }

  private AvlTree<BlockchainAddress, PendingTokensTimestamps> updatePendingTokensTimestamps(
      BlockchainAddress oracle, AvlTree<Long, Long> oracleTimestamps) {
    AvlTree<BlockchainAddress, PendingTokensTimestamps> set;
    if (oracleTimestamps.size() == 0) {
      set = pendingTokens.remove(oracle);
    } else {
      set = pendingTokens.set(oracle, new PendingTokensTimestamps(oracleTimestamps));
    }
    return set;
  }

  /**
   * Get tokens locked to a dispute.
   *
   * @param disputeId id of dispute
   * @return tokens locked to a dispute
   */
  public long getLockedToDispute(OracleDisputeId disputeId) {
    return Objects.requireNonNullElse(lockedToDispute.getValue(disputeId), 0L);
  }

  /**
   * Associate tokens to increase the amount of free tokens available to oracle work.
   *
   * @param amount amount to associate
   * @param expirationTimestamp timestamp when the association expires
   * @return updated {@link StakedTokens} object
   */
  public StakedTokens associateTokens(long amount, Long expirationTimestamp) {
    return new StakedTokens(
        freeTokens + amount,
        reservedTokens,
        lockedToOracle,
        pendingTokens,
        lockedToDispute,
        expirationTimestamp);
  }

  /**
   * Disassociate tokens to decrease the number of tokens available for oracle work. If the
   * disassociated tokens are reserved the number of reserved tokens is decreased.
   *
   * @param amount amount to disassociate
   * @return updated {@link StakedTokens} object
   */
  public StakedTokens disassociateTokens(long amount) {
    ensure(freeTokens >= amount, "Not enough free tokens to disassociate");
    return new StakedTokens(
        freeTokens - amount,
        Math.max(0, reservedTokens - amount),
        lockedToOracle,
        pendingTokens,
        lockedToDispute,
        expirationTimestamp);
  }

  /**
   * Update the amount of reserved tokens. The amount can be greater than the current amount of free
   * tokens in order to reserve tokens that is currently in use.
   *
   * @param newReserved updated amount of reserved tokens
   * @return updated {@link StakedTokens} object
   */
  public StakedTokens setReserved(long newReserved) {
    return new StakedTokens(
        freeTokens,
        newReserved,
        lockedToOracle,
        pendingTokens,
        lockedToDispute,
        expirationTimestamp);
  }

  /**
   * Update the expiration timestamp.
   *
   * @param newExpirationTimestamp updated expiration for association
   * @return updated {@link StakedTokens} object
   */
  public StakedTokens setExpirationTimestamp(Long newExpirationTimestamp) {
    return new StakedTokens(
        freeTokens,
        reservedTokens,
        lockedToOracle,
        pendingTokens,
        lockedToDispute,
        newExpirationTimestamp);
  }

  /**
   * Lock tokens to an oracle.
   *
   * @param oracle the oracle to lock tokens to
   * @param amount the amount of tokens to lock
   * @param blockProductionTime the block production time
   * @return new instance with tokens locked to oracle
   */
  public StakedTokens lockToOracle(
      BlockchainAddress oracle, long amount, long blockProductionTime) {
    ensure(getAllocatableTokens() >= amount, "Not enough allocatable tokens to lock to oracle");
    ensure(
        !hasExpired(blockProductionTime),
        "Tokens associated to contract have expired, and cannot be locked to oracle");
    long currentAmount = Objects.requireNonNullElse(lockedToOracle.getValue(oracle), 0L);
    return new StakedTokens(
        freeTokens - amount,
        reservedTokens,
        lockedToOracle.set(oracle, currentAmount + amount),
        pendingTokens,
        lockedToDispute,
        expirationTimestamp);
  }

  /**
   * Unlock tokens from an oracle immediately.
   *
   * @param oracle the oracle to unlock tokens from
   * @param amount the amount of tokens to unlock
   * @return new instance with tokens unlocked from oracle
   */
  public StakedTokens unlockFromOracle(BlockchainAddress oracle, long amount) {
    long locked = Objects.requireNonNullElse(lockedToOracle.getValue(oracle), 0L);
    long updatedLockedAmount = locked - amount;
    ensure(updatedLockedAmount >= 0, "Not enough locked tokens to unlock requested amount");
    AvlTree<BlockchainAddress, Long> updatedLockedToOracle = lockedToOracle;
    if (updatedLockedAmount == 0) {
      updatedLockedToOracle = updatedLockedToOracle.remove(oracle);
    } else {
      updatedLockedToOracle = updatedLockedToOracle.set(oracle, updatedLockedAmount);
    }
    return new StakedTokens(
        freeTokens + amount,
        reservedTokens,
        updatedLockedToOracle,
        pendingTokens,
        lockedToDispute,
        expirationTimestamp);
  }

  /**
   * Release tokens from oracle lock, setting them in a pending state.
   *
   * @param oracle address of the oracle to release tokens from
   * @param timestamp the timestamp of when the tokens are freed
   * @return new instance with released tokens
   */
  public StakedTokens releaseFromOracle(BlockchainAddress oracle, long timestamp) {
    long locked = Objects.requireNonNullElse(lockedToOracle.getValue(oracle), 0L);
    AvlTree<Long, Long> pendingTimestampsForOracle = getTimestamps(oracle);
    pendingTimestampsForOracle = pendingTimestampsForOracle.set(timestamp, locked);
    return new StakedTokens(
        freeTokens,
        reservedTokens,
        lockedToOracle.remove(oracle),
        pendingTokens.set(oracle, new PendingTokensTimestamps(pendingTimestampsForOracle)),
        lockedToDispute,
        expirationTimestamp);
  }

  /**
   * Burn tokens from the oracle. If not enough tokens are locked to the oracle, they will be burned
   * from pending and freed tokens.
   *
   * @param oracle the oracle to burn tokens from
   * @param totalAmountToBurn the total amount of tokens to burn
   * @return new instance with burned tokens
   */
  public StakedTokens burnFromOracle(BlockchainAddress oracle, long totalAmountToBurn) {
    long remainingAmount = totalAmountToBurn;

    long pending = getPendingForOracle(oracle);
    long burnFromPending = Math.min(remainingAmount, pending);
    remainingAmount -= burnFromPending;
    AvlTree<Long, Long> updatedTimestamps = getTimestamps(oracle);
    updatedTimestamps = burnFromPending(burnFromPending, updatedTimestamps);

    long locked = Objects.requireNonNullElse(lockedToOracle.getValue(oracle), 0L);
    long burnFromLocked = Math.min(remainingAmount, locked);
    remainingAmount -= burnFromLocked;

    long burnFromFree = Math.min(remainingAmount, getFreeTokens());
    remainingAmount -= burnFromFree;

    ensure(remainingAmount == 0L, "Insufficient amount of staked tokens to burn");
    return new StakedTokens(
        freeTokens - burnFromFree,
        reservedTokens,
        lockedToOracle.set(oracle, locked - burnFromLocked),
        updatePendingTokensTimestamps(oracle, updatedTimestamps),
        lockedToDispute,
        expirationTimestamp);
  }

  private AvlTree<Long, Long> burnFromPending(
      long burnFromPending, AvlTree<Long, Long> timestamps) {
    long remainingToBurn = burnFromPending;
    for (Long timestamp : timestamps.keySet()) {
      long staked = timestamps.getValue(timestamp);
      if (remainingToBurn >= staked) {
        timestamps = timestamps.remove(timestamp);
        remainingToBurn -= staked;
      } else {
        timestamps = timestamps.set(timestamp, staked - remainingToBurn);
        return timestamps;
      }
    }
    return timestamps;
  }

  /**
   * Lock tokens to a dispute.
   *
   * @param disputeId id of the dispute to lock tokens to
   * @param amount the amount of tokens to lock
   * @param blockProductionTime the block production time
   * @return new instance with tokens locked to a dispute
   */
  public StakedTokens lockToDispute(
      OracleDisputeId disputeId, long amount, long blockProductionTime) {
    ensure(getAllocatableTokens() >= amount, "Not enough allocatable tokens to lock to dispute");
    ensure(
        !hasExpired(blockProductionTime),
        "Tokens associated to contract have expired, and cannot be locked to dispute");
    return new StakedTokens(
        freeTokens - amount,
        reservedTokens,
        lockedToOracle,
        pendingTokens,
        lockedToDispute.set(disputeId, amount),
        expirationTimestamp);
  }

  /**
   * Release tokens from dispute.
   *
   * @param disputeId id of the dispute to release tokens from
   * @return new instance with released tokens
   */
  public StakedTokens releaseFromDispute(OracleDisputeId disputeId) {
    long amount = Objects.requireNonNullElse(lockedToDispute.getValue(disputeId), 0L);
    return new StakedTokens(
        freeTokens + amount,
        reservedTokens,
        lockedToOracle,
        pendingTokens,
        lockedToDispute.remove(disputeId),
        expirationTimestamp);
  }

  /**
   * Burn tokens locked to a dispute.
   *
   * @param disputeId id of the dispute to burn tokens form
   * @return new instance with tokens burned
   */
  public StakedTokens burnFromDispute(OracleDisputeId disputeId) {
    return new StakedTokens(
        freeTokens,
        reservedTokens,
        lockedToOracle,
        pendingTokens,
        lockedToDispute.remove(disputeId),
        expirationTimestamp);
  }

  private void ensure(boolean predicate, String errorMessage) {
    if (!predicate) {
      throw new IllegalStateException(errorMessage);
    }
  }

  AvlTree<Long, Long> getTimestamps(BlockchainAddress oracle) {
    PendingTokensTimestamps value = pendingTokens.getValue(oracle);
    if (value == null) {
      return AvlTree.create();
    } else {
      return value.timestamps();
    }
  }

  /**
   * Gets the expiration timestamp for the staked tokens.
   *
   * @return the expiration timestamp
   */
  public Long getExpirationTimestamp() {
    return expirationTimestamp;
  }
}
