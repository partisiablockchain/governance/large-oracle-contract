package com.partisiablockchain.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.ArrayList;
import java.util.List;

/** A member of a large oracle. */
@Immutable
public record OracleMember(BlockchainAddress address, BlockchainPublicKey key)
    implements StateSerializable {

  static OracleMember createFromStateAccessor(StateAccessor accessor) {
    return new OracleMember(
        accessor.get("address").blockchainAddressValue(),
        accessor.get("key").blockchainPublicKeyValue());
  }

  static List<OracleMember> createOracleMembers(
      List<BlockchainAddress> addresses, List<BlockchainPublicKey> keys) {
    List<OracleMember> oracleMembers = new ArrayList<>();
    for (int i = 0; i < addresses.size(); i++) {
      OracleMember oracleMember = new OracleMember(addresses.get(i), keys.get(i));
      oracleMembers.add(oracleMember);
    }

    return oracleMembers;
  }
}
